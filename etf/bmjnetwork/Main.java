package etf.bmjnetwork;

import etf.bmjnetwork.graph.Graph;
import etf.bmjnetwork.graph.DirectedGraphFast;
import etf.bmjnetwork.graph.IOGraph;

import java.util.Random;

@SuppressWarnings("ALL")
public class Main {

    public static void main(String[] args) {
        try {

            // Object of graph is created.
            DirectedGraphFast<Integer> g = new DirectedGraphFast<>();

            // edges are added.
            // Since the graph is bidirectional,
            // so boolean bidirectional is passed as true.
            Random random = new Random();
            for (int i = 0; i < 1000; i++) {
                g.addEdge(random.nextInt(100), random.nextInt(100), "bla", Math.random() * 1000);
            }


            // print the graph.
            System.out.println("Graph:\n"
                    + g);

            // gives the no of vertices in the graph.
            System.out.println(g.getNodeCount());
            System.out.println(g.getEdgesCount());
            System.out.println(g.hasEdge(3, 4));
            System.out.println(g.hasNode(5));

            g.removeEdge(1, 2);
            // print the graph.
            System.out.println("Graph:\n"
                    + g);

            g.removeNode(3);
            // print the graph.

            System.out.println("********************");
            System.out.println("Graph:\n"
                    + g);
            System.out.println(g.getAverageDegree(true));
            System.out.println(g.getAverageWeightedDegree("weight", true));
            System.out.println(g.getDensity());
            System.out.println("********************");
            IOGraph.writeGMLFile("last1.gml", g);
            System.out.println("********************");
            IOGraph.writeGEXFFile("last1.gexf", g, "weight");
            System.out.println("********************");
            Graph<String> graph = IOGraph.readGMLFile("BasicALL.gml", true);
            System.out.println(graph.getAverageDegree(true));
            System.out.println(graph.getAverageWeightedDegree("weight", true));
            System.out.println(graph.getDensity());
            System.out.println("Graph:\n"
                    + graph);
            System.out.println(graph.getDiameter());
            System.out.println(graph.getRadius());
            System.out.println(graph.getAveragePathLength());
            System.out.println(graph.getEccentricityMap(true));
            System.out.println(graph.getHubsMap(true, 50, 1.0e-8));
            System.out.println(graph.getAuthorityMap(true, 50, 1.0e-8));
            System.out.println(graph.getStronglyConnectedComponents());
            System.out.println(graph.getWeaklyConnectedComponents());
            System.out.println(graph.getClosenessCentrality(true));
            System.out.println(graph.getEigenvectorCentrality(true, 10, "weight"));
            System.out.println(graph.getCommunityClassesSyncLabelPropagation(true));
            System.out.println("********************");
            IOGraph.writeGMLFile("last2.gml", graph);
            System.out.println("********************");
            IOGraph.writeGEXFFile("last2.gexf", graph, "weight");
            System.out.println("********************");
            graph = IOGraph.readGMLFile("Basic2018.gml", true);
            System.out.println(graph.getAverageDegree(true));
            System.out.println(graph.getAverageWeightedDegree("weight", true));
            System.out.println(graph.getDensity());
            System.out.println("Graph:\n"
                    + graph);
            System.out.println(graph.getDiameter());
            System.out.println(graph.getRadius());
            System.out.println(graph.getAveragePathLength());
            System.out.println(graph.getEccentricityMap(true));
            System.out.println(graph.getHubsMap(true, 50, 1.0e-8));
            System.out.println(graph.getAuthorityMap(true, 50, 1.0e-8));
            System.out.println(graph.getStronglyConnectedComponents());
            System.out.println(graph.getWeaklyConnectedComponents());
            System.out.println(graph.getClosenessCentrality(true));
            System.out.println(graph.getEigenvectorCentrality(true, 1, "weight"));
            System.out.println(graph.getCommunityClassesSyncLabelPropagation(true));
            System.out.println(graph.getBetweennessCentrality(false));
            System.out.println("********************");
            IOGraph.writeGMLFile("last3.gml", graph);
            System.out.println("********************");
            IOGraph.writeGEXFFile("last3.gexf", graph, "weight");
            System.out.println("********************");
            IOGraph.writeCSVFileNode("node.csv", graph);
            IOGraph.writeCSVFileEdge("edge.csv", graph);
            graph = new DirectedGraphFast<>();
            IOGraph.readCSVFileNode("node.csv", graph);
            IOGraph.readCSVFileEdge("edge.csv", graph);
            System.out.println(graph.getAverageDegree(true));
            System.out.println(graph.getAverageWeightedDegree("weight", true));
            System.out.println(graph.getDensity());
            System.out.println("Graph:\n"
                    + graph);
            System.out.println(graph.getDiameter());
            System.out.println(graph.getRadius());
            System.out.println(graph.getAveragePathLength());
            System.out.println(graph.getEccentricityMap(true));
            System.out.println(graph.getHubsMap(true, 50, 1.0e-8));
            System.out.println(graph.getAuthorityMap(true, 50, 1.0e-8));
            System.out.println(graph.getStronglyConnectedComponents());
            System.out.println(graph.getWeaklyConnectedComponents());
            System.out.println(graph.getClosenessCentrality(true));
            System.out.println(graph.getEigenvectorCentrality(true, 1, null));
            System.out.println(graph.getCommunityClassesSyncLabelPropagation(true));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
