package etf.bmjnetwork;

import etf.bmjnetwork.graph.Graph;
import etf.bmjnetwork.graph.IOGraph;
import etf.bmjnetwork.graph.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SpeedMain {
    private static final int NUM_OF_ITERATIONS = 40;
    private static final long MB = 1024L * 1024L;
    private static final long MILI_SECONDS = 1000000;

    public static void main(String[] args) {
        testCheckIfHaveEdgeFast("Basic2018.gml");
        testCheckIfHaveEdgeSlow("Basic2018.gml");
        testCheckIfHaveEdgeFast("BigGraph.gml");
        testCheckIfHaveEdgeSlow("BigGraph.gml");

        testReadGMLFileFast("Basic2018.gml");
        testReadGMLFileSlow("Basic2018.gml");
        testReadGMLFileFast("BigGraph.gml");
        testReadGMLFileSlow("BigGraph.gml");

        testWriteGEXFileFast("Basic2018.gml");
        testWriteGEXFileSlow("Basic2018.gml");
        testWriteGEXFileSlow("BigGraph.gml");
        testWriteGEXFileFast("BigGraph.gml");

        testWriteGMLFileFast("Basic2018.gml");
        testWriteGMLFileSlow("Basic2018.gml");
        testWriteGMLFileFast("BigGraph.gml");
        testWriteGMLFileSlow("BigGraph.gml");

        testRemoveNodeSlow("Basic2018.gml");
        testRemoveNodeSlow("BigGraph.gml");
        testRemoveNodeFast("Basic2018.gml");
        testRemoveNodeFast("BigGraph.gml");

        testRemoveEdgeSlow2("Basic2018.gml");
        testRemoveEdgeSlow2("BigGraph.gml");
        testRemoveEdgeFast2("Basic2018.gml");
        testRemoveEdgeFast2("BigGraph.gml");

        testAddNodeSlow("Basic2018.gml");
        testAddNodeSlow("BigGraph.gml");
        testAddNodeFast("Basic2018.gml");
        testAddNodeFast("BigGraph.gml");

        testAddNodesFromSlow("Basic2018.gml");
        testAddNodesFromSlow("BigGraph.gml");
        testAddNodesFromFast("Basic2018.gml");
        testAddNodesFromFast("BigGraph.gml");

        testAddAttributeToNodeSlow("Basic2018.gml");
        testAddAttributeToNodeSlow("BigGraph.gml");
        testAddAttributeToNodeFast("Basic2018.gml");
        testAddAttributeToNodeFast("BigGraph.gml");

        testAddEdgeSlow("Basic2018.gml");
        testAddEdgeSlow("BigGraph.gml");
        testAddEdgeFast("Basic2018.gml");
        testAddEdgeFast("BigGraph.gml");

        testAddEdgesFromSlow("Basic2018.gml");
        testAddEdgesFromSlow("BigGraph.gml");
        testAddEdgesFromFast("Basic2018.gml");
        testAddEdgesFromFast("BigGraph.gml");

        testAddAttributeToEdgeSlow2("Basic2018.gml");
        testAddAttributeToEdgeSlow2("BigGraph.gml");
        testAddAttributeToEdgeFast2("Basic2018.gml");
        testAddAttributeToEdgeFast2("BigGraph.gml");

        testGetNodeCountSlow("Basic2018.gml");
        testGetNodeCountSlow("BigGraph.gml");
        testGetNodeCountFast("Basic2018.gml");
        testGetNodeCountFast("BigGraph.gml");

        testGetEdgesCountSlow("Basic2018.gml");
        testGetEdgesCountSlow("BigGraph.gml");
        testGetEdgesCountFast("Basic2018.gml");
        testGetEdgesCountFast("BigGraph.gml");

        testGetAllNodesSlow("Basic2018.gml");
        testGetAllNodesSlow("BigGraph.gml");
        testGetAllNodesFast("Basic2018.gml");
        testGetAllNodesFast("BigGraph.gml");

        testGetAllNodesInfoSlow("Basic2018.gml");
        testGetAllNodesInfoSlow("BigGraph.gml");
        testGetAllNodesInfoFast("Basic2018.gml");
        testGetAllNodesInfoFast("BigGraph.gml");

        testGetNodeSlow("Basic2018.gml");
        testGetNodeSlow("BigGraph.gml");
        testGetNodeFast("Basic2018.gml");
        testGetNodeFast("BigGraph.gml");

        testGetEdgeSlow("Basic2018.gml");
        testGetEdgeSlow("BigGraph.gml");
        testGetEdgeFast("Basic2018.gml");
        testGetEdgeFast("BigGraph.gml");

        testGetAllEdgesInfoSlow("Basic2018.gml");
        testGetAllEdgesInfoSlow("BigGraph.gml");
        testGetAllEdgesInfoFast("Basic2018.gml");
        testGetAllEdgesInfoFast("BigGraph.gml");

        testHasNodeSlow("Basic2018.gml");
        testHasNodeSlow("BigGraph.gml");
        testHasNodeFast("Basic2018.gml");
        testHasNodeFast("BigGraph.gml");

        testHasEdgeSlow("Basic2018.gml");
        testHasEdgeSlow("BigGraph.gml");
        testHasEdgeFast("Basic2018.gml");
        testHasEdgeFast("BigGraph.gml");

        testToStringSlow2("Basic2018.gml");
        testToStringSlow2("BigGraph.gml");
        testToStringFast2("Basic2018.gml");
        testToStringFast2("BigGraph.gml");

        testGetAllNodeAttributesSlow("Basic2018.gml");
        testGetAllNodeAttributesSlow("BigGraph.gml");
        testGetAllNodeAttributesFast("Basic2018.gml");
        testGetAllNodeAttributesFast("BigGraph.gml");

        testGetAllEdgeAttributesSlow("Basic2018.gml");
        testGetAllEdgeAttributesSlow("BigGraph.gml");
        testGetAllEdgeAttributesFast("Basic2018.gml");
        testGetAllEdgeAttributesFast("BigGraph.gml");

        testGetAverageDegreeSlow("Basic2018.gml");
        testGetAverageDegreeSlow("BigGraph.gml");
        testGetAverageDegreeFast("Basic2018.gml");
        testGetAverageDegreeFast("BigGraph.gml");

        testGenerateInDegreeAttributesSlow("Basic2018.gml");
        testGenerateInDegreeAttributesSlow("BigGraph.gml");
        testGenerateInDegreeAttributesFast("Basic2018.gml");
        testGenerateInDegreeAttributesFast("BigGraph.gml");

        testRemoveAllGeneratedAttributesSlow("Basic2018.gml");
        testRemoveAllGeneratedAttributesSlow("BigGraph.gml");
        testRemoveAllGeneratedAttributesFast("Basic2018.gml");
        testRemoveAllGeneratedAttributesFast("BigGraph.gml");

        testGetAverageWeightedDegreeSlow("Basic2018.gml");
        testGetAverageWeightedDegreeSlow("BigGraph.gml");
        testGetAverageWeightedDegreeFast("Basic2018.gml");
        testGetAverageWeightedDegreeFast("BigGraph.gml");

        testGenerateWeightedInSlow("Basic2018.gml");
        testGenerateWeightedInSlow("BigGraph.gml");
        testGenerateWeightedInFast("Basic2018.gml");
        testGenerateWeightedInFast("BigGraph.gml");

        testGetDensitySlow("Basic2018.gml");
        testGetDensitySlow("BigGraph.gml");
        testGetDensityFast("Basic2018.gml");
        testGetDensityFast("BigGraph.gml");

        testGetLinksPerNodeSlow("Basic2018.gml");
        testGetLinksPerNodeSlow("BigGraph.gml");
        testGetLinksPerNodeFast("Basic2018.gml");
        testGetLinksPerNodeFast("BigGraph.gml");

        testChangeNameOfNodeAttributeSlow2("Basic2018.gml");
        testChangeNameOfNodeAttributeSlow2("BigGraph.gml");
        testChangeNameOfNodeAttributeFast2("Basic2018.gml");
        testChangeNameOfNodeAttributeFast2("BigGraph.gml");

        testChangeNameOfEdgeAttributeSlow2("Basic2018.gml");
        testChangeNameOfEdgeAttributeSlow2("BigGraph.gml");
        testChangeNameOfEdgeAttributeFast2("Basic2018.gml");
        testChangeNameOfEdgeAttributeFast2("BigGraph.gml");

        testGetDiameterSlow("Basic2018.gml");
        testGetDiameterSlow("BigGraph.gml");
        testGetDiameterFast("Basic2018.gml");
        testGetDiameterFast("BigGraph.gml");

        testGetAveragePathLengthSlow("Basic2018.gml");
        testGetAveragePathLengthSlow("BigGraph.gml");
        testGetAveragePathLengthFast("Basic2018.gml");
        testGetAveragePathLengthFast("BigGraph.gml");

        testGetRadiusSlow("Basic2018.gml");
        testGetRadiusSlow("BigGraph.gml");
        testGetRadiusFast("Basic2018.gml");
        testGetRadiusFast("BigGraph.gml");

        testGetEccentricityMapSlow("Basic2018.gml");
        testGetEccentricityMapSlow("BigGraph.gml");
        testGetEccentricityMapFast("Basic2018.gml");
        testGetEccentricityMapFast("BigGraph.gml");

        testGetInNeighboursSlow("Basic2018.gml");
        testGetInNeighboursSlow("BigGraph.gml");
        testGetInNeighboursFast("Basic2018.gml");
        testGetInNeighboursFast("BigGraph.gml");

        testGetHubsMapSlow("Basic2018.gml");
        testGetHubsMapSlow("BigGraph.gml");
        testGetHubsMapFast("Basic2018.gml");
        testGetHubsMapFast("BigGraph.gml");

        testGetAuthorityMapSlow("Basic2018.gml");
        testGetAuthorityMapSlow("BigGraph.gml");
        testGetAuthorityMapFast("Basic2018.gml");
        testGetAuthorityMapFast("BigGraph.gml");

        testGetStronglyConnectedComponentsSlow("Basic2018.gml");
        testGetStronglyConnectedComponentsSlow("BigGraph.gml");
        testGetStronglyConnectedComponentsFast("Basic2018.gml");
        testGetStronglyConnectedComponentsFast("BigGraph.gml");

        testGetWeaklyConnectedComponentsSlow("Basic2018.gml");
        testGetWeaklyConnectedComponentsSlow("BigGraph.gml");
        testGetWeaklyConnectedComponentsFast("Basic2018.gml");
        testGetWeaklyConnectedComponentsFast("BigGraph.gml");

        testGetEigenvectorCentralitySlow("Basic2018.gml");
        testGetEigenvectorCentralitySlow("BigGraph.gml");
        testGetEigenvectorCentralityFast("Basic2018.gml");
        testGetEigenvectorCentralityFast("BigGraph.gml");

        testCalculateLowestNumberOfHopsSlow("Basic2018.gml");
        testCalculateLowestNumberOfHopsSlow("BigGraph.gml");
        testCalculateLowestNumberOfHopsFast("Basic2018.gml");
        testCalculateLowestNumberOfHopsFast("BigGraph.gml");

        testCalculateLowestNumberOfHopsPathSlow("Basic2018.gml");
        testCalculateLowestNumberOfHopsPathFast("Basic2018.gml");

        testGetBetweennessCentralitySlow("Basic2018.gml");
        testGetBetweennessCentralityFast("Basic2018.gml");

        testGetClosenessCentralitySlow("Basic2018.gml");
        testGetClosenessCentralityFast("Basic2018.gml");

        testGetCommunityClassesAsyncLabelPropagationSlow("Basic2018.gml");
        testGetCommunityClassesAsyncLabelPropagationFast("Basic2018.gml");

        testGetCommunityClassesSyncLabelPropagationSlow("Basic2018.gml");
        testGetCommunityClassesSyncLabelPropagationFast("Basic2018.gml");

        testGetClosenessCentralityFast("BigGraph.gml");
        testGetCommunityClassesSyncLabelPropagationFast("BigGraph.gml");

        testGetCommunityClassesSyncLabelPropagationSlow("BigGraph.gml");
        testGetClosenessCentralitySlow("BigGraph.gml");

        testCalculateLowestNumberOfHopsPathFast("BigGraph.gml");
        testGetBetweennessCentralityFast("BigGraph.gml");
        testGetCommunityClassesAsyncLabelPropagationFast("BigGraph.gml");

        testGetBetweennessCentralitySlow("BigGraph.gml");
        testCalculateLowestNumberOfHopsPathSlow("BigGraph.gml");
        testGetCommunityClassesAsyncLabelPropagationSlow("BigGraph.gml");
        testAddEdgeSlow("Basic2018.gml");
    }

    private static void testReadGMLFileFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            IOGraph.readGMLFile(nameOfFile, true);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testReadGMLFileFast");
        System.out.println("Time: ms " + total / NUM_OF_ITERATIONS / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB / NUM_OF_ITERATIONS);
    }

    private static void testReadGMLFileSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            IOGraph.readGMLFile(nameOfFile, false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testReadGMLFileSlow");
        System.out.println("Time: ms " + total / NUM_OF_ITERATIONS / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB / NUM_OF_ITERATIONS);
    }

    private static void testReadGEXFFileSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            IOGraph.readGEXFFile(nameOfFile, false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testReadGEXFFileSlow");
        System.out.println("Time: ms " + total / NUM_OF_ITERATIONS / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB / NUM_OF_ITERATIONS);
    }

    private static void testReadGEXFFileFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            IOGraph.readGMLFile(nameOfFile, true);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testReadGEXFFileFast");
        System.out.println("Time: ms " + total / NUM_OF_ITERATIONS / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB / NUM_OF_ITERATIONS);
    }

    private static void testWriteGMLFileFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            IOGraph.writeGMLFile("test", stringGraph);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testWriteGMLFileFast");
        System.out.println("Time: ms " + total / NUM_OF_ITERATIONS / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB / NUM_OF_ITERATIONS);
    }

    private static void testWriteGMLFileSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            IOGraph.writeGMLFile("test", stringGraph);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testWriteGMLFileSlow");
        System.out.println("Time: ms " + total / NUM_OF_ITERATIONS / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB / NUM_OF_ITERATIONS);
    }

    private static void testWriteGEXFileFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            IOGraph.writeGEXFFile("test", stringGraph, "");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testWriteGEXFileFast");
        System.out.println("Time: ms " + total / NUM_OF_ITERATIONS / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB / NUM_OF_ITERATIONS);
    }

    private static void testWriteGEXFileSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            IOGraph.writeGEXFFile("test", stringGraph, "");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testWriteGEXFileSlow");
        System.out.println("Time: ms " + total / NUM_OF_ITERATIONS / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB / NUM_OF_ITERATIONS);
    }

    private static void testCheckIfHaveEdgeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String node2 = stringGraph.getAllNodes().get(400);
            runtime.gc();
            long time = System.nanoTime();
            node.checkIfHaveEdge(node2);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testCheckIfHaveEdgeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testCheckIfHaveEdgeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String node2 = stringGraph.getAllNodes().get(400);
            runtime.gc();
            long time = System.nanoTime();
            node.checkIfHaveEdge(node2);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testCheckIfHaveEdgeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNeighbourEdgeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String node2 = stringGraph.getAllNodes().get(400);
            runtime.gc();
            long time = System.nanoTime();
            node.getNeighbourEdge(node2);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNeighbourEdgeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNeighbourEdgeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String node2 = stringGraph.getAllNodes().get(400);
            runtime.gc();
            long time = System.nanoTime();
            node.getNeighbourEdge(node2);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNeighbourEdgeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testRemoveEdgeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String node2 = stringGraph.getAllNodes().get(400);
            runtime.gc();
            long time = System.nanoTime();
            node.removeEdge(node2);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testRemoveEdgeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testRemoveEdgeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String node2 = stringGraph.getAllNodes().get(400);
            runtime.gc();
            long time = System.nanoTime();
            node.removeEdge(node2);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testRemoveEdgeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddAttributeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.addAttribute("test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddAttributeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddAttributeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.addAttribute("test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddAttributeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddAttributeToEdgeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String node2 = stringGraph.getAllNodes().get(400);
            stringGraph.addEdge(node.getMyNode(), node2);
            node = stringGraph.getNode(node.getMyNode());
            runtime.gc();
            long time = System.nanoTime();
            node.addAttributeToEdge(node2, "test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddAttributeToEdgeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddAttributeToEdgeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String node2 = stringGraph.getAllNodes().get(400);
            stringGraph.addEdge(node.getMyNode(), node2);
            node = stringGraph.getNode(node.getMyNode());
            runtime.gc();
            long time = System.nanoTime();
            node.addAttributeToEdge(node2, "test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddAttributeToEdgeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testSetAttributesToEdgeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String node2 = stringGraph.getAllNodes().get(400);
            ConcurrentHashMap<String, Object> temp = new ConcurrentHashMap<>();
            temp.put("test", "test");
            runtime.gc();
            long time = System.nanoTime();
            node.setAttributesToEdge(node2, temp);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testSetAttributesToEdgeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testSetAttributesToEdgeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String node2 = stringGraph.getAllNodes().get(400);
            ConcurrentHashMap<String, Object> temp = new ConcurrentHashMap<>();
            temp.put("test", "test");
            runtime.gc();
            long time = System.nanoTime();
            node.setAttributesToEdge(node2, temp);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testSetAttributesToEdgeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNeighboursEdgeInfoSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.getNeighboursEdgeInfo();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNeighboursEdgeInfoSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNeighboursEdgeInfoFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.getNeighboursEdgeInfo();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNeighboursEdgeInfoFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNeighboursAttributesSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.getNeighboursAttributes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNeighboursAttributesSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNeighboursAttributesFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.getNeighboursAttributes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNeighboursAttributesFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNeighboursInternalSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.getNeighboursInternal();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNeighboursInternalSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNeighboursInternalFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.getNeighboursInternal();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNeighboursInternalFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNeighboursSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.getNeighbours();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNeighboursSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNeighboursFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.getNeighbours();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNeighboursFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAttributesSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.getAttributes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAttributesSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAttributesFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.getAttributes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAttributesFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetMyNodeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.getMyNode();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetMyNodeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetMyNodeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.getMyNode();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetMyNodeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testToStringSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.toString();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testToStringSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testToStringFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.toString();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testToStringFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testRemoveGeneratedAttributesSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.removeGeneratedAttributes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testRemoveGeneratedAttributesSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testRemoveGeneratedAttributesFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.removeGeneratedAttributes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testRemoveGeneratedAttributesFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testChangeNameOfNodeAttributeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.changeNameOfNodeAttribute("test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testChangeNameOfNodeAttributeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testChangeNameOfNodeAttributeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.changeNameOfNodeAttribute("test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testChangeNameOfNodeAttributeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testChangeNameOfEdgeAttributeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.changeNameOfEdgeAttribute("test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testChangeNameOfEdgeAttributeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testChangeNameOfEdgeAttributeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            node.changeNameOfEdgeAttribute("test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testChangeNameOfEdgeAttributeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNodeAttributeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            node.addAttribute("test", "test");
            runtime.gc();
            long time = System.nanoTime();
            node.getNodeAttribute("test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNodeAttributeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNodeAttributeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            node.addAttribute("test", "test");
            runtime.gc();
            long time = System.nanoTime();
            node.getNodeAttribute("test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNodeAttributeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetEdgeAttributeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String s = stringGraph.getAllNodes().get(400);
            stringGraph.addEdge(node.getMyNode(), s);
            stringGraph.addAttributeToEdge(node.getMyNode(), s, "test", "test");
            node = stringGraph.getNode(node.getMyNode());
            runtime.gc();
            long time = System.nanoTime();
            node.getEdgeAttribute(s, "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetEdgeAttributeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetEdgeAttributeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String s = stringGraph.getAllNodes().get(400);
            stringGraph.addEdge(node.getMyNode(), s);
            stringGraph.addAttributeToEdge(node.getMyNode(), s, "test", "test");
            node = stringGraph.getNode(node.getMyNode());
            runtime.gc();
            long time = System.nanoTime();
            node.getEdgeAttribute(s, "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetEdgeAttributeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testRemoveNodeAttributeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            node.addAttribute("test", "test");
            runtime.gc();
            long time = System.nanoTime();
            node.removeNodeAttribute("test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testRemoveNodeAttributeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testRemoveNodeAttributeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            node.addAttribute("test", "test");
            runtime.gc();
            long time = System.nanoTime();
            node.removeNodeAttribute("test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testRemoveNodeAttributeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testRemoveEdgeAttributeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String s = stringGraph.getAllNodes().get(400);
            stringGraph.addEdge(node.getMyNode(), s);
            stringGraph.addAttributeToEdge(node.getMyNode(), s, "test", "test");
            node = stringGraph.getNode(node.getMyNode());
            runtime.gc();
            long time = System.nanoTime();
            node.removeEdgeAttribute(s, "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testRemoveEdgeAttributeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testRemoveEdgeAttributeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String s = stringGraph.getAllNodes().get(400);
            stringGraph.addEdge(node.getMyNode(), s);
            stringGraph.addAttributeToEdge(node.getMyNode(), s, "test", "test");
            node = stringGraph.getNode(node.getMyNode());
            runtime.gc();
            long time = System.nanoTime();
            node.removeEdgeAttribute(s, "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testRemoveEdgeAttributeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testSetAttributesSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            HashMap<String, Object> temp = new HashMap<>();
            temp.put("test", "test");
            runtime.gc();
            long time = System.nanoTime();
            node.setAttributes(temp);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testSetAttributesSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testSetAttributesFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            HashMap<String, Object> temp = new HashMap<>();
            temp.put("test", "test");
            runtime.gc();
            long time = System.nanoTime();
            node.setAttributes(temp);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testSetAttributesFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testRemoveNodeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.removeNode(node.getMyNode());
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testRemoveNodeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testRemoveNodeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.removeNode(node.getMyNode());
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testRemoveNodeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testRemoveEdgeSlow2(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            Node<String> node2 = stringGraph.getAllNodesInfo().get(300);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.removeEdge(node.getMyNode(), node2.getMyNode());
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testRemoveEdgeSlow2");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testRemoveEdgeFast2(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            Node<String> node2 = stringGraph.getAllNodesInfo().get(300);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.removeEdge(node.getMyNode(), node2.getMyNode());
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testRemoveEdgeFast2");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddNodeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.addNode("qwea" + Math.random());
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddNodeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddNodeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.addNode("qwea" + Math.random());
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddNodeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddNodesFromSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            List<Integer> range = IntStream.rangeClosed(-1000, 0).boxed().collect(Collectors.toList());
            List<String> temp = new ArrayList<>();
            range.forEach(e -> {
                temp.add(e + " " + Math.random());
            });
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.addNodesFrom(temp);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddNodesFromSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddNodesFromFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            List<Integer> range = IntStream.rangeClosed(-1000, 0).boxed().collect(Collectors.toList());
            List<String> temp = new ArrayList<>();
            range.forEach(e -> {
                temp.add(e + " " + Math.random());
            });
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.addNodesFrom(temp);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddNodesFromFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddAttributeToNodeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.addAttributeToNode(node.getMyNode(), "test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddAttributeToNodeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddAttributeToNodeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.addAttributeToNode(node.getMyNode(), "test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddAttributeToNodeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddEdgeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.addEdge(node.getMyNode(), "test" + Math.random());
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddEdgeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddEdgeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.addEdge(node.getMyNode(), "test" + Math.random());
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddEdgeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddEdgesFromSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            List<Integer> range = IntStream.rangeClosed(-1000, 0).boxed().collect(Collectors.toList());
            List<String> temp = new ArrayList<>();
            List<String> temp2 = new ArrayList<>();
            range.forEach(e -> {
                temp.add(e + " " + Math.random());
                temp2.add(Math.random() + " " + e);
            });
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.addEdgesFrom(temp, temp2);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddEdgesFromSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddEdgesFromFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            List<Integer> range = IntStream.rangeClosed(-1000, 0).boxed().collect(Collectors.toList());
            List<String> temp = new ArrayList<>();
            List<String> temp2 = new ArrayList<>();
            range.forEach(e -> {
                temp.add(e + " " + Math.random());
                temp2.add(Math.random() + " " + e);
            });
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.addEdgesFrom(temp, temp2);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddEdgesFromFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddAttributeToEdgeSlow2(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            stringGraph.addEdge(node.getMyNode(), "test");
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.addAttributeToEdge(node.getMyNode(), "test", "test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddAttributeToEdgeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testAddAttributeToEdgeFast2(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            stringGraph.addEdge(node.getMyNode(), "test");
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.addAttributeToEdge(node.getMyNode(), "test", "test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testAddAttributeToEdgeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNodeCountSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getNodeCount();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNodeCountSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNodeCountFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getNodeCount();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNodeCountFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetEdgesCountSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getEdgesCount();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetEdgesCountSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetEdgesCountFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getEdgesCount();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetEdgesCountFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAllNodesSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAllNodes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAllNodesSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAllNodesFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAllNodes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAllNodesFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAllNodesInfoSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAllNodesInfo();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAllNodesInfoSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAllNodesInfoFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAllNodesInfo();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAllNodesInfoFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNodeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(50);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getNode(node.getMyNode());
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNodeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetNodeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(50);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getNode(node.getMyNode());
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetNodeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetEdgeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String s = stringGraph.getAllNodes().get(100);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getEdge(node.getMyNode(), s);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetEdgeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetEdgeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            String s = stringGraph.getAllNodes().get(100);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getEdge(node.getMyNode(), s);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetEdgeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAllEdgesInfoSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAllEdgesInfo();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAllEdgesInfoSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAllEdgesInfoFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAllEdgesInfo();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAllEdgesInfoFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testHasNodeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.hasNode(node.getMyNode());
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testHasNodeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testHasNodeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.hasNode(node.getMyNode());
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testHasNodeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testHasEdgeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            Node<String> node2 = stringGraph.getAllNodesInfo().get(100);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.hasEdge(node.getMyNode(), node2.getMyNode());
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testHasEdgeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testHasEdgeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            Node<String> node2 = stringGraph.getAllNodesInfo().get(100);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.hasEdge(node.getMyNode(), node2.getMyNode());
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testHasEdgeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testToStringSlow2(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.toString();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testToStringSlow2");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testToStringFast2(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.toString();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testToStringFast2");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAllNodeAttributesSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAllNodeAttributes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAllNodeAttributesSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAllNodeAttributesFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAllNodeAttributes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAllNodeAttributesFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAllEdgeAttributesSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAllEdgeAttributes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAllEdgeAttributesSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAllEdgeAttributesFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAllEdgeAttributes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAllEdgeAttributesFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAverageDegreeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAverageDegree(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAverageDegreeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAverageDegreeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            Node<String> node = stringGraph.getAllNodesInfo().get(0);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAverageDegree(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAverageDegreeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGenerateInDegreeAttributesSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            stringGraph.removeALlGeneratedAttributes();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.generateInDegreeAttributes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGenerateInDegreeAttributesSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGenerateInDegreeAttributesFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            stringGraph.removeALlGeneratedAttributes();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.generateInDegreeAttributes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGenerateInDegreeAttributesFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testRemoveAllGeneratedAttributesSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.removeALlGeneratedAttributes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testRemoveAllGeneratedAttributesSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testRemoveAllGeneratedAttributesFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.removeALlGeneratedAttributes();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testRemoveAllGeneratedAttributesFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAverageWeightedDegreeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAverageWeightedDegree("test", false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAverageWeightedDegreeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAverageWeightedDegreeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAverageWeightedDegree("test", false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAverageWeightedDegreeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGenerateWeightedInSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            stringGraph.removeALlGeneratedAttributes();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.generateWeightedInDegreeAttributes("test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGenerateWeightedInSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGenerateWeightedInFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            stringGraph.removeALlGeneratedAttributes();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.generateWeightedInDegreeAttributes("test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGenerateWeightedInFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }


    private static void testGetDensitySlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getDensity();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetDensitySlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetDensityFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getDensity();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetDensityFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetLinksPerNodeSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getLinksPerNode();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetLinksPerNodeSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetLinksPerNodeFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getLinksPerNode();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetLinksPerNodeFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testChangeNameOfNodeAttributeSlow2(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.changeNameOfNodeAttribute("test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testChangeNameOfNodeAttributeSlow2");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testChangeNameOfNodeAttributeFast2(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.changeNameOfNodeAttribute("test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testChangeNameOfNodeAttributeFast2");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testChangeNameOfEdgeAttributeSlow2(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.changeNameOfEdgeAttribute("test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testChangeNameOfEdgeAttributeSlow2");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testChangeNameOfEdgeAttributeFast2(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.changeNameOfEdgeAttribute("test", "test");
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testChangeNameOfEdgeAttributeFast2");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testCalculateLowestNumberOfHopsSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.calculateLowestNumberOfHops();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testCalculateLowestNumberOfHopsSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testCalculateLowestNumberOfHopsFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.calculateLowestNumberOfHops();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testCalculateLowestNumberOfHopsFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetDiameterSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getDiameter();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetDiameterSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetDiameterFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getDiameter();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetDiameterFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAveragePathLengthSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAveragePathLength();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAveragePathLengthSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAveragePathLengthFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAveragePathLength();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAveragePathLengthFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetRadiusSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getRadius();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetRadiusSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetRadiusFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getRadius();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetRadiusFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetEccentricityMapSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getEccentricityMap(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetEccentricityMapSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetEccentricityMapFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getEccentricityMap(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetEccentricityMapFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetInNeighboursSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            String s = stringGraph.getAllNodes().get(10);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getInNeighbours(s);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetInNeighboursSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetInNeighboursFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            String s = stringGraph.getAllNodes().get(10);
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getInNeighbours(s);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetInNeighboursFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetHubsMapSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getHubsMap(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetHubsMapSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetHubsMapFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getHubsMap(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetHubsMapFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAuthorityMapSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAuthorityMap(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAuthorityMapSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetAuthorityMapFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getAuthorityMap(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetAuthorityMapFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetStronglyConnectedComponentsSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getStronglyConnectedComponents();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetStronglyConnectedComponentsSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetStronglyConnectedComponentsFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getStronglyConnectedComponents();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetStronglyConnectedComponentsFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetWeaklyConnectedComponentsSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getWeaklyConnectedComponents();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetWeaklyConnectedComponentsSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetWeaklyConnectedComponentsFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getWeaklyConnectedComponents();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetWeaklyConnectedComponentsFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetEigenvectorCentralitySlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getEigenvectorCentrality(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetEigenvectorCentralitySlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetEigenvectorCentralityFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getEigenvectorCentrality(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetEigenvectorCentralityFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testCalculateLowestNumberOfHopsPathSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.calculateLowestNumberOfHopsPath();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testCalculateLowestNumberOfHopsPathSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testCalculateLowestNumberOfHopsPathFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.calculateLowestNumberOfHopsPath();
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testCalculateLowestNumberOfHopsPathFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetBetweennessCentralitySlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getBetweennessCentrality(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetBetweennessCentralitySlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetBetweennessCentralityFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getBetweennessCentrality(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetBetweennessCentralityFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetClosenessCentralitySlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getClosenessCentrality(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetClosenessCentralitySlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetClosenessCentralityFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getClosenessCentrality(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetClosenessCentralityFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetCommunityClassesSyncLabelPropagationSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getCommunityClassesSyncLabelPropagation(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetCommunityClassesSyncLabelPropagationSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetCommunityClassesSyncLabelPropagationFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getCommunityClassesSyncLabelPropagation(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetCommunityClassesSyncLabelPropagationFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetCommunityClassesAsyncLabelPropagationSlow(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, false);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getCommunityClassesAsyncLabelPropagation(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetCommunityClassesAsyncLabelPropagationSlow");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }

    private static void testGetCommunityClassesAsyncLabelPropagationFast(String nameOfFile) {
        long total = 0;
        long memory = 0;
        Graph<String> stringGraph = IOGraph.readGMLFile(nameOfFile, true);
        for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
            Runtime runtime = Runtime.getRuntime();
            stringGraph.removeALlGeneratedAttributes();
            runtime.gc();
            long time = System.nanoTime();
            stringGraph.getCommunityClassesAsyncLabelPropagation(false);
            total += System.nanoTime() - time;
            memory += runtime.totalMemory() - runtime.freeMemory();
        }
        System.out.println("Name of file: " + nameOfFile);
        System.out.println("Name of function: testGetCommunityClassesAsyncLabelPropagationFast");
        System.out.println("Time: ms " + total / MILI_SECONDS);
        System.out.println("Used memory is mb: " + memory / MB);
    }
}