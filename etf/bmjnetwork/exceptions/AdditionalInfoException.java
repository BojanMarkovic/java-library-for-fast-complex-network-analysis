package etf.bmjnetwork.exceptions;

public class AdditionalInfoException extends RuntimeException {
    private static final String EXCEPTION_START_STRING = "Additional info exception, reason: ";
    private static final long serialVersionUID = 216611067308841558L;

    public AdditionalInfoException(String message, Throwable cause) {
        super(EXCEPTION_START_STRING + message, cause);
    }
}
