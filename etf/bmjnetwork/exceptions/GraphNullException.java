package etf.bmjnetwork.exceptions;

public class GraphNullException extends RuntimeException {
    private static final String EXCEPTION_START_STRING = "Null exception happened, reason: ";
    private static final long serialVersionUID = 6928195913268997944L;

    public GraphNullException(String message) {
        super(EXCEPTION_START_STRING + message);
    }

    public GraphNullException(String message, NullPointerException e) {
        super(EXCEPTION_START_STRING + message, e);
    }
}
