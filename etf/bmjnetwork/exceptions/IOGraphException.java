package etf.bmjnetwork.exceptions;

import java.io.IOException;

public class IOGraphException extends RuntimeException {
    private static final String EXCEPTION_START_STRING = "IO graph exception, reason: ";
    private static final long serialVersionUID = -3567349453163226530L;

    public IOGraphException(String message) {
        super(EXCEPTION_START_STRING + message);
    }

    public IOGraphException(String message, IOException e) {
        super(EXCEPTION_START_STRING + message, e);
    }
}
