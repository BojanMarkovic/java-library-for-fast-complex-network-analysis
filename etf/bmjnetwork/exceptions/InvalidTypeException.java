package etf.bmjnetwork.exceptions;

public class InvalidTypeException extends RuntimeException {
    private static final String EXCEPTION_START_STRING = "Invalid type exception, reason: ";
    private static final long serialVersionUID = 2072761610752958059L;

    public InvalidTypeException(String message) {
        super(EXCEPTION_START_STRING + message);
    }

    public InvalidTypeException(String message, NumberFormatException e) {
        super(EXCEPTION_START_STRING + message, e);
    }
}
