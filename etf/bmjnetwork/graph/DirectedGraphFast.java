package etf.bmjnetwork.graph;

import etf.bmjnetwork.exceptions.GraphNullException;
import etf.bmjnetwork.structures.Edge;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static etf.bmjnetwork.graph.NodeImpl.GENERATED_START_ATTRIBUTE;


/**
 * Generic directed graph class. It is thread-safe. Intended to be used with thousands/millions of nodes and edges.
 * Warning: Some functions use parallel streams - may increase CPU usage.
 *
 * @param <T> Node type of a graph.
 */
public class DirectedGraphFast<T> extends GraphFast<T> {
    private static final long serialVersionUID = -2896937474765906363L;

    @Override
    public Edge<T> removeEdge(T nodeFrom, T nodeTo) {
        try {
            return internalGraph.containsKey(nodeFrom) ? internalGraph.get(nodeFrom).removeEdge(nodeTo) : null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to removeEdge!", e);
        }
    }

    @Override
    public boolean addEdge(T source, T destination) {
        addNode(source);
        addNode(destination);
        return ((NodeImpl<T>) internalGraph.get(source)).addNeighbour(destination);
    }

    @Override
    public boolean addEdge(T source, T destination, String attributeName, Object attributeValue) {
        addNode(source);
        addNode(destination);
        return ((NodeImpl<T>) internalGraph.get(source)).addNeighbour(destination, attributeName, attributeValue);
    }

    @Override
    public boolean addEdge(T source, T destination, ConcurrentHashMap<String, Object> attributes) {
        if (attributes == null) throw new GraphNullException("Null was passed to addEdge!");
        addNode(source);
        addNode(destination);
        return ((NodeImpl<T>) internalGraph.get(source)).addNeighbour(destination, attributes);
    }

    @Override
    public void addEdgesFrom(List<T> sourceList, List<T> destinationList) {
        try {
            IntStream.range(0, sourceList.size()).parallel().forEach(index -> addEdge(sourceList.get(index), destinationList.get(index)));
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addEdgesFrom!", e);
        }
    }

    @Override
    public boolean addAttributeToEdge(T source, T destination, String attributeName, Object attributeValue) {
        try {
            return internalGraph.get(source).addAttributeToEdge(destination, attributeName, attributeValue);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addAttributeToEdge!", e);
        }
    }

    @Override
    public void addAttributeToEdge(T source, T destination, ConcurrentHashMap<String, Object> attributes) {
        try {
            internalGraph.get(source).setAttributesToEdge(destination, attributes);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addAttributeToEdge!", e);
        }
    }

    @Override
    public void generateInDegreeAttributes() {
        Map<T, Integer> degree = new ConcurrentHashMap<>();
        internalGraph.values().parallelStream().flatMap(node -> node.getNeighbours().stream()).forEach(neighbour -> {
            degree.putIfAbsent(neighbour, 0);
            degree.compute(neighbour, (t, value) -> value + 1);
        });
        internalGraph.values().parallelStream().forEach(node -> {
            T key = node.getMyNode();
            node.addAttribute(GENERATED_START_ATTRIBUTE + "InDegree", degree.get(key) == null ? 0 : degree.get(key));
        });
    }

    @Override
    public void generateWeightedInDegreeAttributes(String columnToUseAsWeight) {
        try {
            Map<T, Double> degree = new ConcurrentHashMap<>();
            internalGraph.values().parallelStream().forEach(node -> node.getNeighboursInternal().forEach((neighbour, attributes) -> {
                degree.putIfAbsent(neighbour, 0.0);
                degree.compute(neighbour, (t, value) -> value + (attributes.containsKey(columnToUseAsWeight) ?
                        Double.parseDouble(String.valueOf(attributes.get(columnToUseAsWeight))) : 1.0));
            }));
            internalGraph.values().parallelStream().forEach(node -> {
                T key = node.getMyNode();
                node.addAttribute(GENERATED_START_ATTRIBUTE + "WeightedInDegree", degree.get(key) == null ? 0.0 : degree.get(key));
            });
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to generateWeightedInDegreeAttributes!", e);
        }
    }

    @Override
    public List<Edge<T>> getInNeighbours(T nodeTo) {
        try {
            return internalGraph.values().parallelStream().map(node -> node.getNeighbourEdge(nodeTo))
                    .filter(Objects::nonNull).collect(Collectors.toCollection(ArrayList::new));
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getInNeighbours!", e);
        }
    }

    @Override
    public Map<T, ConcurrentHashMap<T, ArrayList<ArrayList<T>>>> calculateLowestNumberOfHopsPath() {
        Map<T, ConcurrentHashMap<T, ArrayList<ArrayList<T>>>> distances = new ConcurrentHashMap<>();
        internalGraph.values().parallelStream().forEach(node -> {
            T myNode = node.getMyNode();
            ConcurrentHashMap<T, ArrayList<ArrayList<T>>> routes = new ConcurrentHashMap<>();
            HashSet<T> currentLevel = new HashSet<>(node.getNeighbours());
            Collection<T> visitedSet = new HashSet<>();

            currentLevel.remove(myNode);
            visitedSet.add(myNode);

            Collection<T> nextLevel = new HashSet<>();
            currentLevel.forEach(nodeTo -> {
                ArrayList<T> route = new ArrayList<>();
                route.add(myNode);
                route.add(nodeTo);
                ArrayList<ArrayList<T>> newRoutes = new ArrayList<>();
                newRoutes.add(route);
                routes.put(nodeTo, newRoutes);
                nextLevel.addAll(internalGraph.get(nodeTo).getNeighbours());
            });

            while (!nextLevel.isEmpty()) {
                visitedSet.addAll(currentLevel);
                currentLevel.clear();
                currentLevel.addAll(nextLevel.stream().filter(t -> !visitedSet.contains(t)).collect(Collectors.toSet()));
                nextLevel.clear();

                currentLevel.forEach(nodeTo -> {
                    ArrayList<ArrayList<T>> newRoutes = new ArrayList<>();
                    List<T> inNeighbours = getInNeighbours(nodeTo).stream().map(Edge::getNodeFrom).collect(Collectors.toList());
                    routes.entrySet().stream().filter(entry -> inNeighbours.contains(entry.getKey()) && !currentLevel.contains(entry.getKey())).forEach(entry ->
                            entry.getValue().forEach(oneRoute -> {
                                ArrayList<T> route = new ArrayList<>(oneRoute);
                                route.add(nodeTo);
                                newRoutes.add(route);
                            }));


                    routes.put(nodeTo, newRoutes);
                    nextLevel.addAll(internalGraph.get(nodeTo).getNeighbours());
                });
            }
            distances.put(myNode, routes);
        });
        return distances;
    }
}
