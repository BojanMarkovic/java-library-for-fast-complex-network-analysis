package etf.bmjnetwork.graph;

import etf.bmjnetwork.exceptions.GraphNullException;
import etf.bmjnetwork.structures.Edge;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static etf.bmjnetwork.graph.NodeImpl.GENERATED_START_ATTRIBUTE;

/**
 * Generic graph abstract class. It is thread-safe.
 *
 * @param <T> Node type of a graph.
 */
public abstract class Graph<T> implements Serializable {
    static final int HITS_MAX_ITERATIONS = 50;
    static final double ROUNDING_CONSTANT = 10000.0;
    static final double EPSILON = 1.0e-8;
    private static final long serialVersionUID = -2476615393921495415L;
    final ConcurrentHashMap<T, Node<T>> internalGraph = new ConcurrentHashMap<>();
    int hashedInternalGraph = -1;
    int diameter = -1;
    double averagePathLength = -1.0;
    int radius = -1;
    Map<T, Integer> eccentricityMap = new ConcurrentHashMap<>();
    Map<T, Double> hubsMap = new ConcurrentHashMap<>();
    Map<T, Double> authorityMap = new ConcurrentHashMap<>();
    private int hashedInternalGraphMetrics = -1;

    /**
     * The function removes a node and all of it's edges from the graph.
     *
     * @param nodeToRemove Object to remove.
     * @return Removed node.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public Node<T> removeNode(T nodeToRemove) {
        try {
            Node<T> removed = internalGraph.remove(nodeToRemove);
            internalGraph.values().forEach(node -> node.removeEdge(nodeToRemove));
            return removed;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to removeNode!", e);
        }
    }

    /**
     * The function removes an edge from the graph.
     *
     * @param nodeFrom Object from which edge starts.
     * @param nodeTo   Object to which edge goes.
     * @return Removed edge
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public abstract Edge<T> removeEdge(T nodeFrom, T nodeTo);

    /**
     * The function adds a new node to the graph. If it already exists nothing happens.
     *
     * @param node Object to add.
     * @return True if node didn't exist in the graph, false if it already existed.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public boolean addNode(T node) {
        try {
            return internalGraph.putIfAbsent(node, new NodeImpl<>(node)) == null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addNode!", e);
        }
    }

    /**
     * The function adds a new node to the graph. If it already exists nothing happens.
     *
     * @param node           Object to add.
     * @param attributeName  Attribute name to associate to the node.
     * @param attributeValue Attribute value to associate to attribute name.
     * @return True if node didn't exist in the graph, false if it already existed.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public boolean addNode(T node, String attributeName, Object attributeValue) {
        try {
            Node<T> newNode = new NodeImpl<>(node);
            newNode.addAttribute(attributeName, attributeValue);
            return internalGraph.putIfAbsent(node, newNode) == null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addNode!", e);
        }
    }

    /**
     * The function adds a new node to the graph. If it already exists nothing happens.
     *
     * @param node       Object to add.
     * @param attributes Attributes associated with node.
     * @return True if node didn't exist in the graph, false if it already existed.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public boolean addNode(T node, Map<String, Object> attributes) {
        try {
            Node<T> newNode = new NodeImpl<>(node, attributes);
            return internalGraph.putIfAbsent(node, newNode) == null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addNode!", e);
        }
    }

    /**
     * The function adds list of nodes to the graph. If they already exist nothing happens.
     *
     * @param listOfNodes List of objects to add.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public void addNodesFrom(Iterable<T> listOfNodes) {
        try {
            listOfNodes.forEach(node -> internalGraph.putIfAbsent(node, new NodeImpl<>(node)));
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addNodesFrom!", e);
        }
    }

    /**
     * The function adds or changes attribute of a node.
     *
     * @param node           Node to modify.
     * @param attributeName  Attribute name to associate to the node.
     * @param attributeValue Attribute value to associate to attribute name.
     * @return True if node didn't have attribute, false if it had attribute, or if node doesn't exist
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public boolean addAttributeToNode(T node, String attributeName, Object attributeValue) {
        try {
            return internalGraph.get(node).addAttribute(attributeName, attributeValue);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addAttributeToNode!", e);
        }
    }

    /**
     * The function changes all attributes of a node if node exists.
     *
     * @param node       Node to modify.
     * @param attributes Attributes associated with node.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public void addAttributeToNode(T node, Map<String, Object> attributes) {
        try {
            internalGraph.get(node).setAttributes(attributes);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addAttributeToNode!", e);
        }
    }

    /**
     * This function adds a new edge to the graph. If it already exists nothing happens.
     *
     * @param source      Object from which edge starts.
     * @param destination Object to which edge goes.
     * @return True if edge didn't exist in the graph, false if it already existed.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public abstract boolean addEdge(T source, T destination);

    /**
     * This function adds a new edge to the graph. If it already exists nothing happens.
     *
     * @param source         Object from which edge starts.
     * @param destination    Object to which edge goes.
     * @param attributeName  Attribute name to associate to the edge.
     * @param attributeValue Attribute value to associate to attribute name.
     * @return True if edge didn't exist in the graph, false if it already existed.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public abstract boolean addEdge(T source, T destination, String attributeName, Object attributeValue);

    /**
     * This function adds a new edge to the graph. If it already exists nothing happens.
     *
     * @param source      Object from which edge starts.
     * @param destination Object to which edge goes.
     * @param attributes  Attributes associated with edge.
     * @return True if edge didn't exist in the graph, false if it already existed.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public abstract boolean addEdge(T source, T destination, ConcurrentHashMap<String, Object> attributes);

    /**
     * The function adds list of edges to the graph. If they already exist nothing happens. Object from sourceList at INDEX
     * location will be connected to the object from destinationList at INDEX location.
     *
     * @param sourceList      List of objects to from which edge starts.
     * @param destinationList List of objects to which edge goes.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public abstract void addEdgesFrom(List<T> sourceList, List<T> destinationList);

    /**
     * The function adds or changes attribute of a edge.
     *
     * @param source         Object from which edge starts.
     * @param destination    Object to which edge goes.
     * @param attributeName  Attribute name to associate to the edge.
     * @param attributeValue Attribute value to associate to attribute name.
     * @return True if edge didn't have attribute, false if it had attribute, or if edge doesn't exist
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public abstract boolean addAttributeToEdge(T source, T destination, String attributeName, Object attributeValue);

    /**
     * The function changes all attributes of a edge if it exists.
     *
     * @param source      Object from which edge starts.
     * @param destination Object to which edge goes.
     * @param attributes  Attributes associated with edge.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public abstract void addAttributeToEdge(T source, T destination, ConcurrentHashMap<String, Object> attributes);

    /**
     * The function returns total number of nodes in a graph.
     *
     * @return Number of nodes.
     */
    public int getNodeCount() {
        return internalGraph.size();
    }

    /**
     * The function returns total number of edges in a graph.
     *
     * @return Number of edges.
     */
    public int getEdgesCount() {
        AtomicInteger sum = new AtomicInteger();
        internalGraph.values().forEach(node -> sum.getAndAdd(node.getNeighbours().size()));
        return sum.get();
    }

    /**
     * The function returns all nodes from a graph.
     *
     * @return List of nodes.
     */
    public List<T> getAllNodes() {
        return new ArrayList<>(internalGraph.keySet());
    }

    /**
     * The function returns all nodes objects from a graph.
     *
     * @return List of node objects.
     */
    public List<Node<T>> getAllNodesInfo() {
        return new ArrayList<>(internalGraph.values());
    }

    /**
     * The function returns node object identified by the node parameter.
     *
     * @param node Identification of node object to return.
     * @return Node object.
     * @throws GraphNullException Null was passed to getNode.
     */
    public Node<T> getNode(T node) {
        try {
            return internalGraph.get(node);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getNode!", e);
        }
    }

    /**
     * The function returns edge object identified by nodeFrom and nodeTo parameters.
     *
     * @param nodeFrom Object from which edge starts.
     * @param nodeTo   Object to which edge goes.
     * @return Edge object.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public Edge<T> getEdge(T nodeFrom, T nodeTo) {
        try {
            if (!internalGraph.containsKey(nodeFrom)) return null;
            return internalGraph.get(nodeFrom).getNeighbourEdge(nodeTo);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getEdge!", e);
        }
    }

    /**
     * The function returns all edge objects from graph.
     *
     * @return List of edge objects.
     */
    public List<Edge<T>> getAllEdgesInfo() {
        List<Edge<T>> edges = Collections.synchronizedList(new ArrayList<>());
        internalGraph.values().forEach(node -> edges.addAll(node.getNeighboursEdgeInfo()));
        return edges;
    }

    /**
     * The function checks if object exists in a graph.
     *
     * @param node Object to check.
     * @return True if node exists in a graph, false if it doesn't exist.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public boolean hasNode(T node) {
        try {
            return internalGraph.containsKey(node);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to hasNode!", e);
        }
    }

    /**
     * The function checks if graph contains edge identified by source and destination parameters.
     *
     * @param source      Object from which edge starts.
     * @param destination Object to which edge goes.
     * @return True if edge exists in a graph, false if it doesn't exist.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public boolean hasEdge(T source, T destination) {
        try {
            if (!internalGraph.containsKey(source)) return false;
            return internalGraph.get(source).checkIfHaveEdge(destination);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to hasEdge!", e);
        }
    }

    /**
     * The function prints basic info about graph.
     *
     * @return Basic info.
     */
    @Override
    public String toString() {
        return String.format("Number of nodes: %d\nNumber of edges: %d\n", getNodeCount(), getEdgesCount());
    }

    /**
     * Helper function for getting all node attributes.
     *
     * @return Hashmap of all node attributes.
     */
    public Map<String, Object> getAllNodeAttributes() {
        Map<String, Object> nodeAttributes = new HashMap<>();
        internalGraph.values().forEach(node -> nodeAttributes.putAll(node.getAttributes()));
        return nodeAttributes;
    }

    /**
     * Helper function for getting all edge attributes.
     *
     * @return Hashmap of all edge attributes.
     */
    public Map<String, Object> getAllEdgeAttributes() {
        HashMap<String, Object> edgeAttributes = new HashMap<>();
        internalGraph.values().forEach(node -> node.getNeighboursInternal().values().forEach(edgeAttributes::putAll));
        return edgeAttributes;
    }

    /**
     * The function returns average degree of a graph.
     *
     * @param writeAsAttribute If true the function will set out degree attribute of all nodes.
     * @return Average degree of a graph.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public double getAverageDegree(Boolean writeAsAttribute) {
        try {
            AtomicInteger degree = new AtomicInteger(0);
            internalGraph.values().forEach(node -> {
                int size = node.getNeighbours().size();
                degree.getAndAdd(size);
                if (writeAsAttribute) {
                    node.addAttribute(GENERATED_START_ATTRIBUTE + "OutDegree", size);
                }
            });
            return degree.doubleValue() / internalGraph.size();
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getAverageDegree!", e);
        }
    }

    /**
     * The function generates in degree attribute for all nodes.
     */
    public abstract void generateInDegreeAttributes();

    /**
     * The function removes all generated attributes from all nodes.
     */
    public void removeALlGeneratedAttributes() {
        internalGraph.values().forEach(Node::removeGeneratedAttributes);
    }

    /**
     * The function returns average weighted degree of a graph.
     * If edge doesn't have columnToUseAsWeight attribute, value 1.0 will be used.
     *
     * @param columnToUseAsWeight Name of attribute to be used as weight for calculations.
     * @param writeAsAttribute    If true the function will set weighted out degree attribute of all nodes.
     * @return Average weighted degree of a graph.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public double getAverageWeightedDegree(String columnToUseAsWeight, Boolean writeAsAttribute) {
        try {
            AtomicReference<Double> degree = new AtomicReference<>(0.0);
            internalGraph.values().forEach(node -> {
                AtomicReference<Double> sum = new AtomicReference<>((double) 0);
                node.getNeighboursAttributes().forEach(attribute ->
                        sum.updateAndGet(v -> v + (attribute.containsKey(columnToUseAsWeight) ?
                                Double.parseDouble(String.valueOf(attribute.get(columnToUseAsWeight))) : 1.0)));
                double sumDouble = sum.get();
                degree.updateAndGet(v -> v + sumDouble);
                if (writeAsAttribute) {
                    node.addAttribute(GENERATED_START_ATTRIBUTE + "WeightedOutDegree", sum);
                }
            });
            return degree.get() / internalGraph.size();
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getAverageWeightedDegree!", e);
        }
    }

    /**
     * The function generates weighted in degree attribute of all nodes.
     * If edge doesn't have columnToUseAsWeight attribute, value 1.0 will be used.
     *
     * @param columnToUseAsWeight Name of attribute to be used as weight for calculations.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public abstract void generateWeightedInDegreeAttributes(String columnToUseAsWeight);

    /**
     * The function returns density of a graph.
     *
     * @return Density of a graph.
     */
    public double getDensity() {
        double size = internalGraph.size();
        return getEdgesCount() / (size * (size - 1.0));
    }

    /**
     * The function returns number of links per node of a graph.
     *
     * @return Number of links per node of a graph.
     */
    public double getLinksPerNode() {
        return getEdgesCount() / (double) internalGraph.size();
    }

    /**
     * Helper function for manipulation on a label attribute after reading a GEXF or GML file.
     *
     * @throws GraphNullException Attribute don't exist or null was passed to a function.
     */
    void changeNodeToLabelAttribute() {
        ConcurrentHashMap<T, T> mapIdToLabel = new ConcurrentHashMap<>();
        Iterable<Node<T>> values = new ArrayList<>(internalGraph.values());
        internalGraph.clear();
        values.forEach(node -> mapIdToLabel.put(node.getMyNode(), (T) node.getAttributes().get("label")));
        values.forEach(node -> ((NodeImpl<T>) node).changeNodeToLabelAttribute(mapIdToLabel));
        values.forEach(value -> internalGraph.put(value.getMyNode(), value));
    }

    /**
     * The function for changing name of attribute of all nodes.
     *
     * @param oldColumnName Old attribute name.
     * @param newColumnName New attribute name.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public void changeNameOfNodeAttribute(String oldColumnName, String newColumnName) {
        try {
            internalGraph.values().forEach(node -> node.changeNameOfNodeAttribute(oldColumnName, newColumnName));
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to changeNameOfNodeAttribute!", e);
        }
    }

    /**
     * The function for changing name of attribute of all edges.
     *
     * @param oldColumnName Old attribute name.
     * @param newColumnName New attribute name.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public void changeNameOfEdgeAttribute(String oldColumnName, String newColumnName) {
        try {
            internalGraph.values().forEach(node -> node.changeNameOfEdgeAttribute(oldColumnName, newColumnName));
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to changeNameOfEdgeAttribute!", e);
        }
    }

    /**
     * Helper function for calculating lowest number of hops between all nodes.
     *
     * @return Map containing all lowest hops between all nodes.
     */
    public Map<T, ConcurrentHashMap<T, Integer>> calculateLowestNumberOfHops() {
        Map<T, ConcurrentHashMap<T, Integer>> distances = new ConcurrentHashMap<>();
        internalGraph.values().forEach(node -> calculateLowestNumberOfHops(distances, node));
        return distances;
    }

    void calculateLowestNumberOfHops(Map<T, ConcurrentHashMap<T, Integer>> distances, Node<T> node) {
        T myNode = node.getMyNode();
        ConcurrentHashMap<T, Integer> distance = new ConcurrentHashMap<>();
        HashSet<T> currentLevel = new HashSet<>(node.getNeighbours());
        Collection<T> visitedSet = new HashSet<>();
        AtomicInteger currentLevelAdd = new AtomicInteger(1);
        if (currentLevel.contains(myNode)) distance.put(myNode, 0);
        do {
            Collection<T> nextLevel = new HashSet<>();
            currentLevel.forEach(nodeTo -> {
                distance.putIfAbsent(nodeTo, currentLevelAdd.get());
                nextLevel.addAll(internalGraph.get(nodeTo).getNeighbours());
            });
            currentLevelAdd.getAndIncrement();
            visitedSet.addAll(currentLevel);
            currentLevel.clear();
            currentLevel.addAll(nextLevel.stream().filter(t -> !visitedSet.contains(t)).collect(Collectors.toSet()));
        } while (!currentLevel.isEmpty());
        distances.put(myNode, distance);
    }

    /**
     * Helper function for calculating multiple graph metrics.
     */
    void calculateMetrics() {
        if (hashedInternalGraphMetrics != internalGraph.hashCode()) {
            Map<T, ConcurrentHashMap<T, Integer>> distances = calculateLowestNumberOfHops();
            AtomicInteger max = new AtomicInteger();
            AtomicInteger min = new AtomicInteger(Integer.MAX_VALUE);
            AtomicInteger sum = new AtomicInteger();
            AtomicInteger count = new AtomicInteger();
            Map<T, Integer> eccentricity = new ConcurrentHashMap<>();
            distances.forEach((key, value) -> {
                AtomicInteger maxInternal = new AtomicInteger();
                value.values().forEach(distance -> {
                    if (maxInternal.get() < distance) maxInternal.set(distance);
                    if (min.get() > distance) min.set(distance);
                    sum.addAndGet(distance);
                    count.getAndIncrement();
                });
                int myMax = maxInternal.get();
                if (min.get() > myMax) min.set(myMax);
                if (max.get() < myMax) max.set(myMax);
                eccentricity.put(key, myMax);
            });
            eccentricityMap = eccentricity;
            diameter = max.get();
            radius = min.get();
            averagePathLength = sum.doubleValue() / count.doubleValue();
            hashedInternalGraphMetrics = internalGraph.hashCode();
        }
    }

    /**
     * The function for calculating graph diameter.
     *
     * @return Graph diameter.
     */
    public int getDiameter() {
        calculateMetrics();
        return diameter;
    }

    /**
     * The function for calculating average path length.
     *
     * @return Average path length.
     */
    public double getAveragePathLength() {
        calculateMetrics();
        return averagePathLength;
    }

    /**
     * The function for calculating radius.
     *
     * @return Graph radius.
     */
    public int getRadius() {
        calculateMetrics();
        return radius;
    }

    /**
     * The function calculates eccentricity of all nodes.
     *
     * @param writeAsAttribute If eccentricity should be saved as attribute.
     * @return Map of node to eccentricity entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public Map<T, Integer> getEccentricityMap(Boolean writeAsAttribute) {
        try {
            calculateMetrics();
            if (writeAsAttribute) {
                internalGraph.values().forEach(node -> node.addAttribute(GENERATED_START_ATTRIBUTE + "Eccentricity", eccentricityMap.get(node.getMyNode())));
            }
            return Collections.unmodifiableMap(eccentricityMap);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getEccentricityMap!", e);
        }
    }

    /**
     * The function returns all edges going to a nodeTo.
     *
     * @param nodeTo Node to which all edges should go.
     * @return List of edges containing nodeTo.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public abstract List<Edge<T>> getInNeighbours(T nodeTo);

    /**
     * Helper function for running hits algorithm.
     *
     * @param maxIterations Max number of iterations to run hits algorithm.
     * @param epsilon       Error tolerance used to check convergence.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    void runHits(int maxIterations, double epsilon) {
        try {
            if (hashedInternalGraph != internalGraph.hashCode()) {
                ConcurrentHashMap<T, Double> hubs = new ConcurrentHashMap<>();
                ConcurrentHashMap<T, Double> authorities = new ConcurrentHashMap<>();
                internalGraph.values().forEach(node -> {
                    T key = node.getMyNode();
                    hubs.put(key, 1.0);
                    authorities.put(key, 1.0);
                });
                Map<T, Double> lastHubs = new ConcurrentHashMap<>(hubs);
                for (int count = 0; count < maxIterations; count++) {
                    AtomicReference<Double> normalised = new AtomicReference<>((double) 0);
                    internalGraph.keySet().forEach(myNode -> {
                        AtomicReference<Double> sum = new AtomicReference<>((double) 0);
                        getInNeighbours(myNode).forEach(edge -> sum.updateAndGet(v -> v + hubs.get(edge.getNodeFrom())));
                        double authority = sum.get();
                        authorities.put(myNode, authority);
                        normalised.updateAndGet(v -> v + authority * authority);
                    });
                    normalised.set(Math.sqrt(normalised.get()));
                    authorities.forEach((key1, value1) -> authorities.put(key1, value1 / normalised.get()));

                    internalGraph.values().forEach(node -> {
                        AtomicReference<Double> sum = new AtomicReference<>((double) 0);
                        node.getNeighbours().forEach(edge -> sum.updateAndGet(v -> v + authorities.get(edge)));
                        double hub = sum.get();
                        hubs.put(node.getMyNode(), hub);
                        normalised.updateAndGet(v -> v + hub * hub);
                    });
                    normalised.set(Math.sqrt(normalised.get()));
                    hubs.forEach((key, value) -> hubs.put(key, value / normalised.get()));
                    AtomicReference<Double> sum = new AtomicReference<>((double) 0);
                    lastHubs.forEach((key, value) -> sum.updateAndGet(v -> v + Math.abs(value - hubs.get(key))));
                    if (sum.get() < epsilon) {
                        break;
                    }
                    lastHubs = new ConcurrentHashMap<>(hubs);
                }
                authorityMap = authorities;
                hubsMap = hubs;
                hashedInternalGraph = internalGraph.hashCode();
            }
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to runHits!", e);
        }
    }

    /**
     * The function for calculating hub values.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @param maxIterations    Max number of iterations to run internal hits algorithm.
     * @param epsilon          Error tolerance used to check convergence.
     * @return Map of node to hub value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public Map<T, Double> getHubsMap(Boolean writeAsAttribute, int maxIterations, double epsilon) {
        try {
            runHits(maxIterations, epsilon);
            if (writeAsAttribute) {
                internalGraph.values().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "Hub", hubsMap.get(node.getMyNode()) == null ? 0.0 : hubsMap.get(node.getMyNode())));
            }
            return Collections.unmodifiableMap(hubsMap);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getHubsMap!", e);
        }
    }

    /**
     * The function for calculating hub values.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @return Map of node to hub value entries.
     */
    public Map<T, Double> getHubsMap(Boolean writeAsAttribute) {
        return getHubsMap(writeAsAttribute, HITS_MAX_ITERATIONS, EPSILON);
    }

    /**
     * The function for calculating authority values.
     *
     * @param writeAsAttribute If authority values should be saved as attribute.
     * @param maxIterations    Max number of iterations to run internal hits algorithm.
     * @param epsilon          Error tolerance used to check convergence.
     * @return Map of node to authority value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public Map<T, Double> getAuthorityMap(Boolean writeAsAttribute, int maxIterations, double epsilon) {
        try {
            runHits(maxIterations, epsilon);
            if (writeAsAttribute) {
                internalGraph.values().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "Authority", authorityMap.get(node.getMyNode()) == null ? 0.0 : authorityMap.get(node.getMyNode())));
            }
            return Collections.unmodifiableMap(authorityMap);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getHubsMap!", e);
        }
    }

    /**
     * The function for calculating authority values.
     *
     * @param writeAsAttribute If authority values should be saved as attribute.
     * @return Map of node to authority value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public Map<T, Double> getAuthorityMap(Boolean writeAsAttribute) {
        return getAuthorityMap(writeAsAttribute, HITS_MAX_ITERATIONS, EPSILON);
    }

    /**
     * The function for calculating strongly connected components.
     *
     * @return Number of strongly connected components.
     */
    public int getStronglyConnectedComponents() {
        AtomicInteger connectedComponents = new AtomicInteger();
        HashSet<T> visited = new HashSet<>();
        internalGraph.values().forEach(node -> {
            if (!visited.contains(node)) {
                connectedComponents.getAndIncrement();
            }
            visited.addAll(node.getNeighbours());
        });

        return connectedComponents.get();
    }

    /**
     * The function for calculating weakly connected components.
     *
     * @return Number of weakly connected components.
     */
    public int getWeaklyConnectedComponents() {
        ConcurrentLinkedQueue<Set<T>> sets = new ConcurrentLinkedQueue<>();
        internalGraph.values().forEach(node -> {
            Set<T> set = new HashSet<>(node.getNeighbours());
            set.add(node.getMyNode());
            sets.add(set);
        });

        int size = sets.size();
        for (int count = 0; count < size; count++) {
            Set<T> set = sets.remove();
            sets.remove(set);
            sets.forEach(set2 -> {
                Collection<T> intersection = new HashSet<>(set);
                intersection.retainAll(set2);
                if (!intersection.isEmpty()) {
                    set.addAll(set2);
                    sets.remove(set2);
                }
            });
            sets.add(set);
        }
        return sets.size();
    }

    /**
     * The function for calculating eigenvector centrality.
     *
     * @param writeAsAttribute    If hub values should be saved as attribute.
     * @param maxIterations       Max number of iterations to run internal algorithms.
     * @param attributeNameWeight Attribute name that will be used as weight, if null edge weight won't be used.
     * @return Map of node to eigenvector centrality value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public ConcurrentHashMap<T, Double> getEigenvectorCentrality(Boolean writeAsAttribute, int maxIterations, String attributeNameWeight) {
        try {
            double size = internalGraph.size();
            ConcurrentHashMap<T, BigDecimal> eigenvectorInternal = new ConcurrentHashMap<>();
            internalGraph.keySet().forEach(key -> eigenvectorInternal.put(key, new BigDecimal(1.0 / size)));
            for (int count = 0; count < maxIterations; count++) {
                Map<T, BigDecimal> lastEigenvector = new ConcurrentHashMap<>(eigenvectorInternal);
                eigenvectorInternal.keySet().forEach(key -> {
                    Node<T> node = internalGraph.get(key);
                    Map<T, Map<String, Object>> neighboursInternal = node.getNeighboursInternal();
                    node.getNeighbours().forEach(neighbour -> eigenvectorInternal.compute(neighbour, (key2, value2) -> value2.add(lastEigenvector.get(key).multiply(
                            new BigDecimal(String.valueOf(attributeNameWeight == null ? 1.0 : neighboursInternal.get(neighbour).get(attributeNameWeight)))))));
                });
            }

            AtomicReference<BigDecimal> max = new AtomicReference<>(new BigDecimal(0));
            eigenvectorInternal.values().forEach(value -> {
                if (value.compareTo(max.get()) > 0) {
                    max.set(value);
                }
            });
            ConcurrentHashMap<T, Double> eigenvector = new ConcurrentHashMap<>();
            eigenvectorInternal.forEach((key, value) -> eigenvector.put(key, Math.round(
                    value.divide(max.get(), 10, RoundingMode.HALF_UP).doubleValue() * ROUNDING_CONSTANT) / ROUNDING_CONSTANT));
            if (writeAsAttribute) {
                internalGraph.values().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "EigenvectorCentrality", eigenvector.get(node.getMyNode())));
            }
            return eigenvector;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getEigenvectorCentrality!", e);
        }
    }

    /**
     * The function for calculating eigenvector centrality.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @return Map of node to eigenvector centrality value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public ConcurrentHashMap<T, Double> getEigenvectorCentrality(Boolean writeAsAttribute) {
        return getEigenvectorCentrality(writeAsAttribute, 10, null);
    }

    /**
     * Helper function for calculating lowest number of hops between all nodes. Using BFS algorithm.
     *
     * @return Map containing all paths for lowest hops between all nodes.
     */
    public abstract Map<T, ConcurrentHashMap<T, ArrayList<ArrayList<T>>>> calculateLowestNumberOfHopsPath();

    /**
     * The function for calculating betweenness centrality.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @return Map of node to betweenness centrality value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public ConcurrentHashMap<T, Double> getBetweennessCentrality(Boolean writeAsAttribute) {
        try {
            ConcurrentHashMap<T, Double> betweenness = new ConcurrentHashMap<>();
            Map<T, ConcurrentHashMap<T, ArrayList<ArrayList<T>>>> shortestPaths = calculateLowestNumberOfHopsPath();
            shortestPaths.keySet().forEach(key -> {
                AtomicReference<Double> betweennessSum = new AtomicReference<>(0.0);
                internalGraph.keySet().forEach(node -> {
                    if (!node.equals(key)) {
                        shortestPaths.get(node).forEach((nodeTo, paths) -> {
                            if (!nodeTo.equals(key)) {
                                AtomicInteger sumTotal = new AtomicInteger();
                                AtomicInteger sum = new AtomicInteger();
                                sumTotal.addAndGet(paths.size());
                                paths.forEach(path -> {
                                    if (path.contains(key)) {
                                        sum.getAndIncrement();
                                    }
                                });
                                if (sumTotal.get() != 0) {
                                    betweennessSum.updateAndGet(v -> v + sum.doubleValue() / sumTotal.doubleValue());
                                }
                            }
                        });
                    }
                });
                betweenness.put(key, betweennessSum.get());
            });

            if (writeAsAttribute) {
                internalGraph.values().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "BetweennessCentrality", betweenness.get(node.getMyNode())));
            }

            return betweenness;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getBetweennessCentrality!", e);
        }
    }

    /**
     * The function for calculating closeness centrality.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @return Map of node to closeness centrality value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public Map<T, Double> getClosenessCentrality(Boolean writeAsAttribute) {
        try {
            Map<T, ConcurrentHashMap<T, Integer>> distances = calculateLowestNumberOfHops();
            Map<T, Double> closeness = new ConcurrentHashMap<>();
            distances.forEach((key, value) -> {
                AtomicReference<Double> sum = new AtomicReference<>((double) 0);
                value.forEach((key2, value2) -> {
                    if (key != key2) {
                        sum.updateAndGet(v -> v + value2);
                    }
                });
                closeness.put(key, value.size() / sum.get());
            });
            closeness.forEach((key, value) -> {
                if (value.isNaN()) {
                    closeness.put(key, 0.0);
                }
            });

            if (writeAsAttribute) {
                internalGraph.values().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "ClosenessCentrality", closeness.get(node.getMyNode())));
            }
            return closeness;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getClosenessCentrality!", e);
        }
    }

    /**
     * The function for calculating community classes using synchronized label propagation algorithm.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @param maxIterations    Max number of iterations to run internal algorithms.
     * @return Map of node to community class value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public ConcurrentHashMap<T, Integer> getCommunityClassesSyncLabelPropagation(Boolean writeAsAttribute, int maxIterations) {
        try {
            AtomicInteger id = new AtomicInteger();
            ConcurrentHashMap<T, Integer> communities = new ConcurrentHashMap<>();
            internalGraph.keySet().forEach(key -> communities.put(key, id.getAndIncrement()));
            for (int count = 0; count < maxIterations; count++) {
                Map<T, Integer> lastCommunities = new ConcurrentHashMap<>(communities);
                lastCommunities.forEach((key, value) -> runLabelPropagation(communities, key, value));
                if (lastCommunities.hashCode() == communities.hashCode()) return communities;
            }

            if (writeAsAttribute) {
                internalGraph.values().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "CommunityClasses", communities.get(node.getMyNode())));
            }
            return communities;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getCommunityClasses!", e);
        }
    }

    /**
     * The function for calculating community classes using asynchronized label propagation algorithm.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @param maxIterations    Max number of iterations to run internal algorithms.
     * @return Map of node to community class value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public ConcurrentHashMap<T, Integer> getCommunityClassesAsyncLabelPropagation(Boolean writeAsAttribute, int maxIterations) {
        try {
            AtomicInteger id = new AtomicInteger();
            ConcurrentHashMap<T, Integer> communities = new ConcurrentHashMap<>();
            internalGraph.keySet().forEach(key -> communities.put(key, id.getAndIncrement()));
            for (int count = 0; count < maxIterations; count++) {
                communities.forEach((key, value) -> runLabelPropagation(communities, key, value));
            }

            if (writeAsAttribute) {
                internalGraph.values().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "CommunityClasses", communities.get(node.getMyNode())));
            }
            return communities;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getCommunityClasses!", e);
        }
    }

    /**
     * The function for calculating community classes using synchronized label propagation algorithm.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @return Map of node to community class value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public ConcurrentHashMap<T, Integer> getCommunityClassesSyncLabelPropagation(Boolean writeAsAttribute) {
        return getCommunityClassesSyncLabelPropagation(writeAsAttribute, 1000);
    }

    /**
     * The function for calculating community classes using asynchronized label propagation algorithm.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @return Map of node to community class value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public ConcurrentHashMap<T, Integer> getCommunityClassesAsyncLabelPropagation(Boolean writeAsAttribute) {
        return getCommunityClassesAsyncLabelPropagation(writeAsAttribute, 1000);
    }

    private void runLabelPropagation(ConcurrentHashMap<T, Integer> communities, T key, Integer value) {
        Map<Integer, Integer> colorClasses = new HashMap<>();
        ArrayList<T> neighbours = new ArrayList<>(internalGraph.get(key).getNeighbours());
        getInNeighbours(key).forEach(edge -> neighbours.add(edge.getNodeFrom()));
        neighbours.forEach(neighbour -> {
            int colorClass = communities.get(neighbour);
            colorClasses.putIfAbsent(colorClass, 0);
            colorClasses.compute(colorClass, (key2, value2) -> value2 + 1);
        });
        AtomicInteger max = new AtomicInteger();
        AtomicInteger color = new AtomicInteger(value);
        colorClasses.forEach((key2, value2) -> {
            if (max.get() < value2) {
                max.set(value2);
                color.set(key2);
            }
        });
        communities.put(key, color.get());
    }
}
