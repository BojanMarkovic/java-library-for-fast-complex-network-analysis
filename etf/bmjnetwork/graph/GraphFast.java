package etf.bmjnetwork.graph;

import etf.bmjnetwork.exceptions.GraphNullException;
import etf.bmjnetwork.structures.Edge;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static etf.bmjnetwork.graph.NodeImpl.GENERATED_START_ATTRIBUTE;

/**
 * Generic graph abstract class. It is thread-safe. Intended to be used with thousands/millions of nodes and edges.
 * Warning: Some functions use parallel streams - may increase CPU usage.
 *
 * @param <T> Node type of a graph.
 */
public abstract class GraphFast<T> extends Graph<T> {
    private static final long serialVersionUID = -2476615393921495415L;

    /**
     * The function removes a node and all of it's edges from the graph.
     * Warning: Function uses parallel streams - may increase CPU usage.
     *
     * @param nodeToRemove Object to remove.
     * @return Removed node.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public Node<T> removeNode(T nodeToRemove) {
        try {
            Node<T> removed = internalGraph.remove(nodeToRemove);
            internalGraph.values().parallelStream().forEach(node -> node.removeEdge(nodeToRemove));
            return removed;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to removeNode!", e);
        }
    }

    @Override
    public boolean addNode(T node) {
        try {
            return internalGraph.putIfAbsent(node, new NodeFast<>(node)) == null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addNode!", e);
        }
    }

    @Override
    public boolean addNode(T node, String attributeName, Object attributeValue) {
        try {
            Node<T> newNode = new NodeFast<>(node);
            newNode.addAttribute(attributeName, attributeValue);
            return internalGraph.putIfAbsent(node, newNode) == null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addNode!", e);
        }
    }

    @Override
    public boolean addNode(T node, Map<String, Object> attributes) {
        try {
            Node<T> newNode = new NodeFast<>(node, attributes);
            return internalGraph.putIfAbsent(node, newNode) == null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addNode!", e);
        }
    }

    @Override
    public void addNodesFrom(Iterable<T> listOfNodes) {
        try {
            listOfNodes.forEach(node -> internalGraph.putIfAbsent(node, new NodeFast<>(node)));
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addNodesFrom!", e);
        }
    }

    @Override
    public boolean addAttributeToNode(T node, String attributeName, Object attributeValue) {
        try {
            return internalGraph.get(node).addAttribute(attributeName, attributeValue);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addAttributeToNode!", e);
        }
    }

    @Override
    public void addAttributeToNode(T node, Map<String, Object> attributes) {
        try {
            internalGraph.get(node).setAttributes(attributes);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addAttributeToNode!", e);
        }
    }

    /**
     * The function adds list of edges to the graph. If they already exist nothing happens. Object from sourceList at INDEX
     * location will be connected to the object from destinationList at INDEX location.
     * Warning: Function uses parallel streams - may increase CPU usage.
     *
     * @param sourceList      List of objects to from which edge starts.
     * @param destinationList List of objects to which edge goes.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public abstract void addEdgesFrom(List<T> sourceList, List<T> destinationList);

    /**
     * The function returns total number of edges in a graph.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @return Number of edges.
     */
    @Override
    public int getEdgesCount() {
        return internalGraph.values().parallelStream().mapToInt(node -> node.getNeighbours().size()).sum();
    }

    @Override
    public Node<T> getNode(T node) {
        try {
            return internalGraph.get(node);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getNode!", e);
        }
    }

    @Override
    public Edge<T> getEdge(T nodeFrom, T nodeTo) {
        try {
            if (!internalGraph.containsKey(nodeFrom)) return null;
            return internalGraph.get(nodeFrom).getNeighbourEdge(nodeTo);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getEdge!", e);
        }
    }

    /**
     * The function returns all edge objects from graph.
     * Warning: Function uses parallel streams - may increase CPU usage.
     *
     * @return List of edge objects.
     */
    @Override
    public List<Edge<T>> getAllEdgesInfo() {
        List<Edge<T>> edges = Collections.synchronizedList(new ArrayList<>());
        internalGraph.values().parallelStream().map(Node::getNeighboursEdgeInfo).forEach(edges::addAll);
        return edges;
    }

    @Override
    public boolean hasNode(T node) {
        try {
            return internalGraph.containsKey(node);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to hasNode!", e);
        }
    }

    @Override
    public boolean hasEdge(T source, T destination) {
        try {
            if (!internalGraph.containsKey(source)) return false;
            return internalGraph.get(source).checkIfHaveEdge(destination);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to hasEdge!", e);
        }
    }

    @Override
    public Map<String, Object> getAllNodeAttributes() {
        HashMap<String, Object> nodeAttributes = new HashMap<>();
        internalGraph.values().stream().map(Node::getAttributes).forEach(nodeAttributes::putAll);
        return nodeAttributes;
    }

    /**
     * The function returns average degree of a graph.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param writeAsAttribute If true the function will set out degree attribute of all nodes.
     * @return Average degree of a graph.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public double getAverageDegree(Boolean writeAsAttribute) {
        try {
            AtomicInteger degree = new AtomicInteger(0);
            internalGraph.values().parallelStream().forEach(node -> {
                int size = node.getNeighbours().size();
                degree.getAndAdd(size);
                if (writeAsAttribute) {
                    node.addAttribute(GENERATED_START_ATTRIBUTE + "OutDegree", size);
                }
            });
            return degree.doubleValue() / internalGraph.size();
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getAverageDegree!", e);
        }
    }

    /**
     * The function generates in degree attribute for all nodes.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     */
    public abstract void generateInDegreeAttributes();

    /**
     * The function removes all generated attributes from all nodes.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     */
    @Override
    public void removeALlGeneratedAttributes() {
        internalGraph.values().parallelStream().forEach(Node::removeGeneratedAttributes);
    }

    /**
     * The function returns average weighted degree of a graph.
     * If edge doesn't have columnToUseAsWeight attribute, value 1.0 will be used.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param columnToUseAsWeight Name of attribute to be used as weight for calculations.
     * @param writeAsAttribute    If true the function will set weighted out degree attribute of all nodes.
     * @return Average weighted degree of a graph.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public double getAverageWeightedDegree(String columnToUseAsWeight, Boolean writeAsAttribute) {
        try {
            AtomicReference<Double> degree = new AtomicReference<>(0.0);
            internalGraph.values().parallelStream().forEach(node -> {
                double sum = node.getNeighboursAttributes().stream().mapToDouble(
                        attribute -> attribute.containsKey(columnToUseAsWeight) ?
                                Double.parseDouble(String.valueOf(attribute.get(columnToUseAsWeight))) : 1.0).sum();
                degree.updateAndGet(v -> v + sum);
                if (writeAsAttribute) {
                    node.addAttribute(GENERATED_START_ATTRIBUTE + "WeightedOutDegree", sum);
                }
            });
            return degree.get() / internalGraph.size();
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getAverageWeightedDegree!", e);
        }
    }

    /**
     * The function generates weighted in degree attribute of all nodes.
     * If edge doesn't have columnToUseAsWeight attribute, value 1.0 will be used.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param columnToUseAsWeight Name of attribute to be used as weight for calculations.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public abstract void generateWeightedInDegreeAttributes(String columnToUseAsWeight);

    /**
     * Helper function for manipulation on a label attribute after reading a GEXF or GML file.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @throws GraphNullException Attribute don't exist or null was passed to a function.
     */
    @Override
    void changeNodeToLabelAttribute() {
        ConcurrentHashMap<T, T> mapIdToLabel = new ConcurrentHashMap<>();
        Collection<Node<T>> values = new ArrayList<>(internalGraph.values());
        internalGraph.clear();
        values.forEach(node -> mapIdToLabel.put(node.getMyNode(), (T) node.getAttributes().get("label")));
        values.parallelStream().forEach(node -> ((NodeImpl<T>) node).changeNodeToLabelAttribute(mapIdToLabel));
        values.forEach(value -> internalGraph.put(value.getMyNode(), value));
    }

    /**
     * The function for changing name of attribute of all nodes.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param oldColumnName Old attribute name.
     * @param newColumnName New attribute name.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public void changeNameOfNodeAttribute(String oldColumnName, String newColumnName) {
        try {
            internalGraph.values().parallelStream().forEach(node -> node.changeNameOfNodeAttribute(oldColumnName, newColumnName));
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to changeNameOfNodeAttribute!", e);
        }
    }

    /**
     * The function for changing name of attribute of all edges.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param oldColumnName Old attribute name.
     * @param newColumnName New attribute name.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public void changeNameOfEdgeAttribute(String oldColumnName, String newColumnName) {
        try {
            internalGraph.values().parallelStream().forEach(node -> node.changeNameOfEdgeAttribute(oldColumnName, newColumnName));
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to changeNameOfEdgeAttribute!", e);
        }
    }

    /**
     * Helper function for calculating lowest number of hops between all nodes.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @return Map containing all lowest hops between all nodes.
     */
    @Override
    public Map<T, ConcurrentHashMap<T, Integer>> calculateLowestNumberOfHops() {
        Map<T, ConcurrentHashMap<T, Integer>> distances = new ConcurrentHashMap<>();
        internalGraph.values().parallelStream().forEach(node -> calculateLowestNumberOfHops(distances, node));
        return distances;
    }

    /**
     * The function calculates eccentricity of all nodes.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param writeAsAttribute If eccentricity should be saved as attribute.
     * @return Map of node to eccentricity entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public Map<T, Integer> getEccentricityMap(Boolean writeAsAttribute) {
        try {
            calculateMetrics();
            if (writeAsAttribute) {
                internalGraph.values().parallelStream().forEach(node -> node.addAttribute(GENERATED_START_ATTRIBUTE + "Eccentricity", eccentricityMap.get(node.getMyNode())));
            }
            return Collections.unmodifiableMap(eccentricityMap);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getEccentricityMap!", e);
        }
    }

    /**
     * The function returns all edges going to a nodeTo.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param nodeTo Node to which all edges should go.
     * @return List of edges containing nodeTo.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    public abstract List<Edge<T>> getInNeighbours(T nodeTo);

    /**
     * Helper function for running hits algorithm.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param maxIterations Max number of iterations to run hits algorithm.
     * @param epsilon       Error tolerance used to check convergence.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    void runHits(int maxIterations, double epsilon) {
        try {
            if (hashedInternalGraph != internalGraph.hashCode()) {
                ConcurrentHashMap<T, Double> hubs = new ConcurrentHashMap<>();
                ConcurrentHashMap<T, Double> authorities = new ConcurrentHashMap<>();
                internalGraph.values().parallelStream().map(Node::getMyNode).forEach(myNode -> {
                    hubs.put(myNode, 1.0);
                    authorities.put(myNode, 1.0);
                });
                Map<T, Double> lastHubs = new ConcurrentHashMap<>(hubs);
                for (int count = 0; count < maxIterations; count++) {
                    AtomicReference<Double> normalised = new AtomicReference<>((double) 0);
                    internalGraph.keySet().parallelStream().forEach(myNode -> {
                        double authority = getInNeighbours(myNode).stream().mapToDouble(edge -> hubs.get(edge.getNodeFrom())).sum();
                        authorities.put(myNode, authority);
                        normalised.updateAndGet(v -> v + authority * authority);
                    });
                    normalised.set(Math.sqrt(normalised.get()));
                    authorities.entrySet().parallelStream().forEach(entry -> authorities.put(entry.getKey(), entry.getValue() / normalised.get()));

                    internalGraph.values().parallelStream().forEach(node -> {
                        double hub = node.getNeighbours().stream().mapToDouble(authorities::get).sum();
                        hubs.put(node.getMyNode(), hub);
                        normalised.updateAndGet(v -> v + hub * hub);
                    });
                    normalised.set(Math.sqrt(normalised.get()));
                    hubs.entrySet().parallelStream().forEach(entry -> hubs.put(entry.getKey(), entry.getValue() / normalised.get()));
                    double sum = lastHubs.entrySet().stream().mapToDouble(entry -> Math.abs(entry.getValue() - hubs.get(entry.getKey()))).sum();
                    if (sum < epsilon) {
                        break;
                    }
                    lastHubs = new ConcurrentHashMap<>(hubs);
                }
                authorityMap = authorities;
                hubsMap = hubs;
                hashedInternalGraph = internalGraph.hashCode();
            }
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to runHits!", e);
        }
    }

    /**
     * The function for calculating hub values.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @param maxIterations    Max number of iterations to run internal hits algorithm.
     * @param epsilon          Error tolerance used to check convergence.
     * @return Map of node to hub value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public Map<T, Double> getHubsMap(Boolean writeAsAttribute, int maxIterations, double epsilon) {
        try {
            runHits(maxIterations, epsilon);
            if (writeAsAttribute) {
                internalGraph.values().parallelStream().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "Hub", hubsMap.get(node.getMyNode()) == null ? 0.0 : hubsMap.get(node.getMyNode())));
            }
            return Collections.unmodifiableMap(hubsMap);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getHubsMap!", e);
        }
    }

    /**
     * The function for calculating hub values.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @return Map of node to hub value entries.
     */
    @Override
    public Map<T, Double> getHubsMap(Boolean writeAsAttribute) {
        return getHubsMap(writeAsAttribute, HITS_MAX_ITERATIONS, EPSILON);
    }

    /**
     * The function for calculating authority values.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param writeAsAttribute If authority values should be saved as attribute.
     * @param maxIterations    Max number of iterations to run internal hits algorithm.
     * @param epsilon          Error tolerance used to check convergence.
     * @return Map of node to authority value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public Map<T, Double> getAuthorityMap(Boolean writeAsAttribute, int maxIterations, double epsilon) {
        try {
            runHits(maxIterations, epsilon);
            if (writeAsAttribute) {
                internalGraph.values().parallelStream().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "Authority", authorityMap.get(node.getMyNode()) == null ? 0.0 : authorityMap.get(node.getMyNode())));
            }
            return Collections.unmodifiableMap(authorityMap);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getHubsMap!", e);
        }
    }

    /**
     * The function for calculating authority values.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param writeAsAttribute If authority values should be saved as attribute.
     * @return Map of node to authority value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public Map<T, Double> getAuthorityMap(Boolean writeAsAttribute) {
        return getAuthorityMap(writeAsAttribute, HITS_MAX_ITERATIONS, EPSILON);
    }

    /**
     * The function for calculating weakly connected components.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @return Number of weakly connected components.
     */
    @Override
    public int getWeaklyConnectedComponents() {
        ConcurrentLinkedQueue<Set<T>> sets = new ConcurrentLinkedQueue<>();
        internalGraph.values().parallelStream().forEach(node -> {
            Set<T> set = new HashSet<>(node.getNeighbours());
            set.add(node.getMyNode());
            sets.add(set);
        });

        int size = sets.size();
        for (int count = 0; count < size; count++) {
            Set<T> set = sets.remove();
            sets.remove(set);
            sets.forEach(set2 -> {
                Collection<T> intersection = new HashSet<>(set);
                intersection.retainAll(set2);
                if (!intersection.isEmpty()) {
                    set.addAll(set2);
                    sets.remove(set2);
                }
            });
            sets.add(set);
        }
        return sets.size();
    }

    /**
     * The function for calculating eigenvector centrality.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param writeAsAttribute    If hub values should be saved as attribute.
     * @param maxIterations       Max number of iterations to run internal algorithms.
     * @param attributeNameWeight Attribute name that will be used as weight, if null edge weight won't be used.
     * @return Map of node to eigenvector centrality value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public ConcurrentHashMap<T, Double> getEigenvectorCentrality(Boolean writeAsAttribute, int maxIterations, String attributeNameWeight) {
        try {
            double size = internalGraph.size();
            ConcurrentHashMap<T, BigDecimal> eigenvectorInternal = internalGraph.keySet().parallelStream().collect(
                    Collectors.toMap(key -> key, key -> new BigDecimal(1.0 / size), (key, value) -> value, ConcurrentHashMap::new));
            for (int count = 0; count < maxIterations; count++) {
                Map<T, BigDecimal> lastEigenvector = new ConcurrentHashMap<>(eigenvectorInternal);
                eigenvectorInternal.keySet().parallelStream().forEach(key -> {
                    Node<T> node = internalGraph.get(key);
                    Map<T, Map<String, Object>> neighboursInternal = node.getNeighboursInternal();
                    node.getNeighbours().forEach(neighbour -> eigenvectorInternal.compute(neighbour, (key2, value2) -> value2.add(lastEigenvector.get(key).multiply(
                            new BigDecimal(String.valueOf(attributeNameWeight == null ? 1.0 : neighboursInternal.get(neighbour).get(attributeNameWeight)))))));
                });
            }

            BigDecimal max = eigenvectorInternal.values().stream().max(BigDecimal::compareTo).get();
            ConcurrentHashMap<T, Double> eigenvector = eigenvectorInternal.entrySet().parallelStream().collect(
                    Collectors.toMap(Map.Entry::getKey, entry ->
                            Math.round(entry.getValue().divide(max, 10, RoundingMode.HALF_UP).doubleValue() * ROUNDING_CONSTANT)
                                    / ROUNDING_CONSTANT, (key, value) -> value, ConcurrentHashMap::new));
            if (writeAsAttribute) {
                internalGraph.values().parallelStream().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "EigenvectorCentrality", eigenvector.get(node.getMyNode())));
            }
            return eigenvector;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getEigenvectorCentrality!", e);
        }
    }

    /**
     * The function for calculating eigenvector centrality.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @return Map of node to eigenvector centrality value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public ConcurrentHashMap<T, Double> getEigenvectorCentrality(Boolean writeAsAttribute) {
        return getEigenvectorCentrality(writeAsAttribute, 10, null);
    }

    /**
     * Helper function for calculating lowest number of hops between all nodes. Using BFS algorithm.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @return Map containing all paths for lowest hops between all nodes.
     */
    public abstract Map<T, ConcurrentHashMap<T, ArrayList<ArrayList<T>>>> calculateLowestNumberOfHopsPath();

    /**
     * The function for calculating betweenness centrality.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @return Map of node to betweenness centrality value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public ConcurrentHashMap<T, Double> getBetweennessCentrality(Boolean writeAsAttribute) {
        try {
            ConcurrentHashMap<T, Double> betweenness = new ConcurrentHashMap<>();
            Map<T, ConcurrentHashMap<T, ArrayList<ArrayList<T>>>> shortestPaths = calculateLowestNumberOfHopsPath();
            shortestPaths.entrySet().parallelStream().map(Map.Entry::getKey).forEach(key -> {
                AtomicReference<Double> betweennessSum = new AtomicReference<>(0.0);
                internalGraph.keySet().stream().filter(node -> !node.equals(key)).forEach(node -> shortestPaths.get(node).forEach((nodeTo, paths) -> {
                    if (!nodeTo.equals(key)) {
                        AtomicInteger sumTotal = new AtomicInteger();
                        AtomicInteger sum = new AtomicInteger();
                        sumTotal.addAndGet(paths.size());
                        paths.stream().filter(path -> path.contains(key)).forEach(path -> sum.getAndIncrement());
                        if (sumTotal.get() != 0) {
                            betweennessSum.updateAndGet(v -> v + sum.doubleValue() / sumTotal.doubleValue());
                        }
                    }
                }));
                betweenness.put(key, betweennessSum.get());
            });

            if (writeAsAttribute) {
                internalGraph.values().parallelStream().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "BetweennessCentrality", betweenness.get(node.getMyNode())));
            }

            return betweenness;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getBetweennessCentrality!", e);
        }
    }

    /**
     * The function for calculating closeness centrality.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @return Map of node to closeness centrality value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public ConcurrentHashMap<T, Double> getClosenessCentrality(Boolean writeAsAttribute) {
        try {
            Map<T, ConcurrentHashMap<T, Integer>> distances = calculateLowestNumberOfHops();
            ConcurrentHashMap<T, Double> closeness = new ConcurrentHashMap<>();
            distances.entrySet().parallelStream().forEach(entry -> {
                T key = entry.getKey();
                Map<T, Integer> value = entry.getValue();
                closeness.put(key, value.size() / (value.entrySet().stream().filter(entryValue -> key != entryValue.getKey()).mapToDouble(Map.Entry::getValue).sum()));
            });
            closeness.entrySet().parallelStream().filter(entry -> entry.getValue().isNaN()).forEach(entry -> closeness.put(entry.getKey(), 0.0));

            if (writeAsAttribute) {
                internalGraph.values().parallelStream().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "ClosenessCentrality", closeness.get(node.getMyNode())));
            }
            return closeness;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getClosenessCentrality!", e);
        }
    }

    /**
     * The function for calculating community classes using synchronized label propagation algorithm.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @param maxIterations    Max number of iterations to run internal algorithms.
     * @return Map of node to community class value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public ConcurrentHashMap<T, Integer> getCommunityClassesSyncLabelPropagation(Boolean writeAsAttribute, int maxIterations) {
        try {
            AtomicInteger id = new AtomicInteger();
            ConcurrentHashMap<T, Integer> communities = internalGraph.keySet().parallelStream().collect(
                    Collectors.toMap(node -> node, node -> id.getAndIncrement(), (key, value) -> value, ConcurrentHashMap::new));
            for (int count = 0; count < maxIterations; count++) {
                Map<T, Integer> lastCommunities = new ConcurrentHashMap<>(communities);
                lastCommunities.entrySet().parallelStream().forEach(entry -> runLabelPropagation(communities, entry));
                if (lastCommunities.hashCode() == communities.hashCode()) return communities;
            }

            if (writeAsAttribute) {
                internalGraph.values().parallelStream().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "CommunityClasses", communities.get(node.getMyNode())));
            }
            return communities;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getCommunityClasses!", e);
        }
    }

    /**
     * The function for calculating community classes using synchronized label propagation algorithm.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @return Map of node to community class value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public ConcurrentHashMap<T, Integer> getCommunityClassesSyncLabelPropagation(Boolean writeAsAttribute) {
        return getCommunityClassesSyncLabelPropagation(writeAsAttribute, 1000);
    }

    /**
     * The function for calculating community classes using asynchronized label propagation algorithm.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param writeAsAttribute If hub values should be saved as attribute.
     * @param maxIterations    Max number of iterations to run internal algorithms.
     * @return Map of node to community class value entries.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    @Override
    public ConcurrentHashMap<T, Integer> getCommunityClassesAsyncLabelPropagation(Boolean writeAsAttribute, int maxIterations) {
        try {
            AtomicInteger id = new AtomicInteger();
            ConcurrentHashMap<T, Integer> communities = internalGraph.keySet().parallelStream().collect(
                    Collectors.toMap(node -> node, node -> id.getAndIncrement(), (key, value) -> value, ConcurrentHashMap::new));
            for (int count = 0; count < maxIterations; count++) {
                communities.entrySet().parallelStream().forEach(entry -> runLabelPropagation(communities, entry));
            }

            if (writeAsAttribute) {
                internalGraph.values().parallelStream().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "CommunityClasses", communities.get(node.getMyNode())));
            }
            return communities;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getCommunityClasses!", e);
        }
    }

    private void runLabelPropagation(ConcurrentHashMap<T, Integer> communities, Map.Entry<T, Integer> entry) {
        T key = entry.getKey();
        Map<Integer, Integer> colorClasses = new HashMap<>();
        ArrayList<T> neighbours = new ArrayList<>();
        neighbours.addAll(internalGraph.get(key).getNeighbours());
        neighbours.addAll(getInNeighbours(key).stream().map(Edge::getNodeFrom).collect(Collectors.toList()));
        neighbours.forEach(neighbour -> {
            int colorClass = communities.get(neighbour);
            colorClasses.putIfAbsent(colorClass, 0);
            colorClasses.compute(colorClass, (key2, value2) -> value2 + 1);
        });
        AtomicInteger max = new AtomicInteger();
        AtomicInteger color = new AtomicInteger(entry.getValue());
        colorClasses.forEach((key2, value2) -> {
            if (max.get() < value2) {
                max.set(value2);
                color.set(key2);
            }
        });
        communities.put(key, color.get());
    }
}
