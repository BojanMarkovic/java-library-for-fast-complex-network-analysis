package etf.bmjnetwork.graph;

import etf.bmjnetwork.exceptions.AdditionalInfoException;
import etf.bmjnetwork.exceptions.GraphNullException;
import etf.bmjnetwork.exceptions.IOGraphException;
import etf.bmjnetwork.exceptions.InvalidTypeException;
import etf.bmjnetwork.structures.Pair;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

/**
 * Class for IO operations on a graph.
 * Warning: Some functions use parallel streams - may increase CPU usage.
 */
public final class IOGraph {
    private static final Pattern GRAPH_GML = Pattern.compile("^\\s*graph.*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern GRAPH_BRACKET_GML = Pattern.compile("^\\s*graph\\s*\\[\\s*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern GRAPHICS_GML = Pattern.compile("^\\s*graphics.*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern NODE_GML = Pattern.compile("\\s*node.*", Pattern.CASE_INSENSITIVE);
    private static final Pattern NODE_BRACKET_GML = Pattern.compile("^\\s*node\\s*\\[\\s*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern EDGE_GML = Pattern.compile("\\s*edge.*", Pattern.CASE_INSENSITIVE);
    private static final Pattern EDGE_BRACKET_GML = Pattern.compile("^\\s*edge\\s*\\[\\s*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern OPEN_BRACKET_GML = Pattern.compile("\\s*\\[\\s*");
    private static final Pattern CLOSE_BRACKET_GML = Pattern.compile("\\s*]\\s*");
    private static final Pattern ID_GML = Pattern.compile("^\\s*id\\s+\\d+\\s*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern SOURCE_GML = Pattern.compile("^\\s*source\\s+\\d+\\s*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern TARGET_GML = Pattern.compile("^\\s*target\\s+\\d+\\s*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern ATTRIBUTE_GML = Pattern.compile("^\\s*[\\w-]+\\s+.+$");
    private static final Pattern DIRECTION_GML = Pattern.compile("^\\s*[\\w]+\\s+[01]\\s*$");
    private static final Pattern GRAPH_GEXF = Pattern.compile("^\\s*<graph.*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern UNDIRECTED_GEXF = Pattern.compile("^\\s*<graph.+defaultedgetype=\"undirected\".*>$", Pattern.CASE_INSENSITIVE);
    private static final Pattern EDGE_ATTRIBUTE_TYPE_GEXF = Pattern.compile("^\\s*<attributes.+class=\"edge\".*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern NODE_ATTRIBUTE_TYPE_GEXF = Pattern.compile("^\\s*<attributes.+class=\"node\".*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern ATTRIBUTES_END_TYPE_GEXF = Pattern.compile("^\\s*</attributes>\\s*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern ATTRIBUTE_TYPE_GEXF = Pattern.compile("^\\s*<attribute .*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern ATTRIBUTE_GEXF = Pattern.compile("^\\s*<attvalue .*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern NODE_GEXF = Pattern.compile("^\\s*<node .*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern EDGE_GEXF = Pattern.compile("^\\s*<edge .*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern NODES_GEXF = Pattern.compile("^\\s*<nodes.*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern NODE_END_GEXF = Pattern.compile("^\\s*</node>\\s*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern NODES_END_GEXF = Pattern.compile("^\\s*</nodes>\\s*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern EDGES_GEXF = Pattern.compile("^\\s*<edges.*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern EDGE_END_GEXF = Pattern.compile("^\\s*</edge>\\s*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern EDGES_END_GEXF = Pattern.compile("^\\s*</edges>\\s*$", Pattern.CASE_INSENSITIVE);
    private static final String SPACE_CHAR = " ";
    private static final String QUOTE_CHAR = "\"";
    private static final String EMPTY_CHAR = "";
    private static final String START_SPACE_CHAR = "^\\s+";
    private static final String END_XML_CHAR = "/>";
    private static final int BIG_CAPACITY = 1000;
    private static final int SMALL_CAPACITY = 100;
    private static final char DOT = '.';
    private static final char QUOTATION = '"';

    /**
     * Private default constructor.
     */
    private IOGraph() {
    }

    /**
     * Helper function that converts String value to a value in a className type.
     *
     * @param className Name of a class associated to the value.
     * @param value     Value to be converted to right type.
     * @return Value in a right class type.
     * @throws InvalidTypeException Conversion error.
     * @throws GraphNullException   Null was passed to a function.
     */
    private static Object toObject(String className, String value) {
        try {
            if ("boolean".compareToIgnoreCase(className) == 0) return Boolean.parseBoolean(value);
            if ("byte".compareToIgnoreCase(className) == 0) return Byte.parseByte(value);
            if ("short".compareToIgnoreCase(className) == 0) return Short.parseShort(value);
            if ("integer".compareToIgnoreCase(className) == 0) return Integer.parseInt(value);
            if ("long".compareToIgnoreCase(className) == 0) return Long.parseLong(value);
            if ("float".compareToIgnoreCase(className) == 0) return Float.parseFloat(value);
            if ("double".compareToIgnoreCase(className) == 0) return Double.parseDouble(value);
            if ("string".compareToIgnoreCase(className) == 0) return value;
        } catch (NumberFormatException e) {
            throw new InvalidTypeException("Conversion error!", e);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to toObject function!", e);
        }
        throw new InvalidTypeException("Conversion error!");
    }

    /**
     * The function for reading GML file and converting it to Graph class.
     *
     * @param fileName  Name of a file to be read.
     * @param fastGraph Boolean parameter that determines type of graph that will be created.
     * @return GraphSlow Generated and initialised by the file.
     * @throws AdditionalInfoException Special exception with additional information if IO error occurs.
     * @throws IOGraphException        Invalid file, invalid formatting.
     * @throws GraphNullException      Null was passed to some internal function.
     */
    public static Graph<String> readGMLFile(String fileName, Boolean fastGraph) {
        Graph<String> graph = fastGraph ? new DirectedGraphFast<>() : new DirectedGraph<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            line = tryToFind(br, GRAPH_GML, "File must start with graph keyword!");
            if (!GRAPH_BRACKET_GML.matcher(line).find()) {
                tryToFind(br, OPEN_BRACKET_GML, "Character '[' after graph is missing or invalid formatting!");
            }
            while ((line = br.readLine()) != null) {
                if (NODE_GML.matcher(line).find()) {
                    if (!NODE_BRACKET_GML.matcher(line).find()) {
                        tryToFind(br, OPEN_BRACKET_GML, "Character '[' after node is missing or invalid formatting!");
                    }
                    String id = null;
                    Map<String, Object> attributes = new ConcurrentHashMap<>();
                    while ((line = br.readLine()) != null) {
                        if (ID_GML.matcher(line).find()) {
                            id = line.replaceAll(START_SPACE_CHAR, EMPTY_CHAR).split(SPACE_CHAR)[1];
                        } else if (GRAPHICS_GML.matcher(line).find()) {
                            tryToFind(br, CLOSE_BRACKET_GML, "Invalid formatting in graphics section!");
                        } else if (parseGMLAttributesAndEnd(line, attributes)) break;
                    }
                    graph.addNode(id, attributes);
                } else if (EDGE_GML.matcher(line).find()) {
                    if (!EDGE_BRACKET_GML.matcher(line).find()) {
                        tryToFind(br, OPEN_BRACKET_GML, "Character '[' after edge is missing or invalid formatting!");
                    }
                    String source = null;
                    String target = null;
                    ConcurrentHashMap<String, Object> attributes = new ConcurrentHashMap<>();
                    while ((line = br.readLine()) != null) {
                        if (ID_GML.matcher(line).find()) {
                        } else if (SOURCE_GML.matcher(line).find()) {
                            source = line.replaceAll(START_SPACE_CHAR, EMPTY_CHAR).split(SPACE_CHAR)[1];
                        } else if (TARGET_GML.matcher(line).find()) {
                            target = line.replaceAll(START_SPACE_CHAR, EMPTY_CHAR).split(SPACE_CHAR)[1];
                        } else if (parseGMLAttributesAndEnd(line, attributes)) break;
                    }
                    graph.addEdge(source, target, attributes);
                } else if (DIRECTION_GML.matcher(line).find()) {
                    if (line.contains("0")) {
                        graph = fastGraph ? new UndirectedGraphFast<>() : new UndirectedGraph<>();
                    }
                }
            }
        } catch (FileNotFoundException e) {
            throw new AdditionalInfoException(String.format("File was not found: %s", fileName), e);
        } catch (IOException e) {
            throw new IOGraphException(String.format("Some IO exception on file: %s", fileName), e);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function!", e);
        }
        graph.changeNodeToLabelAttribute();
        return graph;
    }

    /**
     * Helper function for parsing attributes of nodes and edges in GML files.
     *
     * @param line       Line to be parsed for attributes.
     * @param attributes Map to fill with parsed attributes.
     * @return Special boolean expression intended for use in readGMLFile function.
     * @throws IOGraphException   Invalid file, invalid formatting.
     * @throws GraphNullException Null was passed to some internal function.
     */
    private static boolean parseGMLAttributesAndEnd(String line, Map<String, Object> attributes) {
        try {
            if (ATTRIBUTE_GML.matcher(line).find()) {
                String[] split = line.replaceAll(START_SPACE_CHAR, EMPTY_CHAR).split(SPACE_CHAR);
                attributes.put(split[0], split[1].replaceAll(QUOTE_CHAR, EMPTY_CHAR));
            } else if (CLOSE_BRACKET_GML.matcher(line).find()) {
                return true;
            } else {
                throw new IOGraphException(String.format("Invalid line, invalid formatting! Line: %s", line));
            }
            return false;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function.", e);
        }
    }

    /**
     * Helper function for reading GML and GEXF files.
     *
     * @param bufferedReader BufferedReader of already opened file.
     * @param pattern        Pattern to look for while reading a file.
     * @param error          Error that will be thrown in case pattern can't be matched inside of a file.
     * @return Line matching the pattern.
     * @throws IOException        If some IO error happens during reading of a file.
     * @throws IOGraphException   Invalid file, invalid formatting.
     * @throws GraphNullException Null was passed to some internal function.
     */
    private static String tryToFind(BufferedReader bufferedReader, Pattern pattern, String error) throws IOException {
        try {
            String line;
            do line = bufferedReader.readLine(); while ((!pattern.matcher(line).find()) && line != null);
            if (line == null) {
                throw new IOGraphException(error);
            }
            return line;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function.", e);
        }
    }

    /**
     * The function for reading GEXF file and converting it to Graph class. The function will try to ignore everything
     * that isn't supposed to be in GEXF file, should be careful with duplicate attributes.
     *
     * @param fileName  Name of a file to be read.
     * @param fastGraph Boolean parameter that determines type of graph that will be created.
     * @return GraphSlow Generated and initialised by the file.
     * @throws AdditionalInfoException Special exception with additional information if IO error occurs.
     * @throws IOGraphException        Invalid file, invalid formatting.
     * @throws GraphNullException      Null was passed to some internal function.
     */
    public static Graph<String> readGEXFFile(String fileName, Boolean fastGraph) {
        Graph<String> graph;
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line = tryToFind(br, GRAPH_GEXF, "File doesn't have <graph>");
            if (UNDIRECTED_GEXF.matcher(line).find()) {
                graph = fastGraph ? new UndirectedGraphFast<>() : new UndirectedGraph<>();
            } else {
                graph = fastGraph ? new DirectedGraphFast<>() : new DirectedGraph<>();
            }
            ConcurrentHashMap<String, Pair<String, String>> nodeAttributesTypes = null;
            ConcurrentHashMap<String, Pair<String, String>> edgeAttributesTypes = null;
            while ((line = br.readLine()) != null) {
                if (EDGE_ATTRIBUTE_TYPE_GEXF.matcher(line).find() && !line.contains(END_XML_CHAR)) {
                    edgeAttributesTypes = readAttributeTypes(br);
                } else if (NODE_ATTRIBUTE_TYPE_GEXF.matcher(line).find() && !line.contains(END_XML_CHAR)) {
                    nodeAttributesTypes = readAttributeTypes(br);
                } else if (NODES_GEXF.matcher(line).find() && !line.contains(END_XML_CHAR)) {
                    while (((line = br.readLine()) != null) && (!NODES_END_GEXF.matcher(line).find())) {
                        if (NODE_GEXF.matcher(line).find()) {
                            String id = readXMLParameter(line, "id");
                            String label = readXMLParameter(line, "label");
                            Map<String, Object> attributes = readAttributes(br, line, nodeAttributesTypes, NODE_END_GEXF);
                            attributes.put("label", label);
                            graph.addNode(id, attributes);
                        }
                    }
                } else if (EDGES_GEXF.matcher(line).find()) {
                    while (((line = br.readLine()) != null) &&
                            (!EDGES_END_GEXF.matcher(line).find())) {
                        if (EDGE_GEXF.matcher(line).find()) {
                            ConcurrentHashMap<String, Object> attributes = new ConcurrentHashMap<>();
                            String source = readXMLParameter(line, "source");
                            String target = readXMLParameter(line, "target");
                            if (line.contains("weight")) {
                                attributes.put("weight", Double.parseDouble(readXMLParameter(line, "weight")));
                            }
                            attributes.putAll(readAttributes(br, line, edgeAttributesTypes, EDGE_END_GEXF));
                            graph.addEdge(source, target, attributes);
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            throw new AdditionalInfoException(String.format("File was not found: %s", fileName), e);
        } catch (IOException e) {
            throw new IOGraphException(String.format("Some IO exception on file: %s", fileName), e);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function.", e);
        }
        graph.changeNodeToLabelAttribute();
        return graph;
    }

    /**
     * Helper function that reads all attributes and their associated types from a file.
     *
     * @param bufferedReader      BufferedReader of already opened file.
     * @param line                Last line that was read.
     * @param nodeAttributesTypes Map of attributes and their associated types.
     * @param endString           Ending XML String to look for.
     * @return All attributes and converts them to their associated types.
     * @throws IOException        If some IO error happens during reading of a file.
     * @throws GraphNullException Null was passed to some internal function.
     */
    private static Map<String, Object> readAttributes(BufferedReader bufferedReader, String line, ConcurrentHashMap<String,
            Pair<String, String>> nodeAttributesTypes, Pattern endString) throws IOException {
        Map<String, Object> attributes = new ConcurrentHashMap<>();
        try {
            if (!line.contains(END_XML_CHAR)) {
                while (((line = bufferedReader.readLine()) != null) && (!endString.matcher(line).find())) {
                    if (ATTRIBUTE_GEXF.matcher(line).find()) {
                        String aFor = readXMLParameter(line, "for");
                        String value = readXMLParameter(line, "value");
                        Pair<String, String> pair = nodeAttributesTypes.get(aFor);
                        attributes.put(pair.getObject1(), toObject(pair.getObject2(), value));
                    }
                }
            }
            return attributes;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function.", e);
        }
    }

    /**
     * Helper function that reads all attributes and their associated types from a file.
     *
     * @param bufferedReader BufferedReader of already opened file.
     * @return All attributes and their associated types.
     * @throws IOException        If some IO error happens during reading of a file.
     * @throws GraphNullException Null was passed to some internal function.
     */
    private static ConcurrentHashMap<String, Pair<String, String>> readAttributeTypes(BufferedReader bufferedReader) throws IOException {
        ConcurrentHashMap<String, Pair<String, String>> attributesTypes = new ConcurrentHashMap<>();
        String line;
        try {
            while (((line = bufferedReader.readLine()) != null) && (!ATTRIBUTES_END_TYPE_GEXF.matcher(line).find())) {
                if (ATTRIBUTE_TYPE_GEXF.matcher(line).find()) {
                    String id = readXMLParameter(line, "id");
                    String title = readXMLParameter(line, "title");
                    String type = readXMLParameter(line, "type");
                    attributesTypes.put(id, new Pair<>(title, type));
                }
            }
            return attributesTypes;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function.", e);
        }
    }

    /**
     * Helper function to read data from XML tag.
     *
     * @param line   Line to parse.
     * @param toRead Element to be read.
     * @return String value of the element.
     * @throws AdditionalInfoException XML parameter invalidly formatted.
     * @throws GraphNullException      Null was passed to some internal function.
     */
    private static String readXMLParameter(String line, String toRead) {
        try {
            String substring = line.substring(line.indexOf(toRead + "=\"") + toRead.length() + 2);
            return substring.substring(0, substring.indexOf(QUOTATION));
        } catch (IndexOutOfBoundsException e) {
            throw new AdditionalInfoException("XML parameter invalidly formatted", e);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function.", e);
        }
    }

    /**
     * The function generates GML file from graph.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     * Important: Some types of T not supported.
     *
     * @param fileName Name of output file.
     * @param graph    GraphSlow to write to a file.
     * @throws AdditionalInfoException IO exception on a file.
     * @throws GraphNullException      Null was passed to some internal function.
     */
    public static void writeGMLFile(String fileName, Graph<?> graph) {
        try (FileWriter fileWriter = new FileWriter(fileName)) {
            StringBuffer stringBuffer = new StringBuffer(BIG_CAPACITY);
            stringBuffer.append("graph [\n  directed 1\n");
            AtomicInteger id = new AtomicInteger();
            Map<Object, Integer> mapLabelToId = new ConcurrentHashMap<>();
            graph.getAllNodesInfo().parallelStream().forEach(node -> {
                StringBuilder stringBuilder = new StringBuilder(SMALL_CAPACITY);
                int myId = id.getAndIncrement();
                mapLabelToId.put(node.getMyNode(), myId);
                stringBuilder.append(String.format("  node [\n    id %s\n    label \"%s\"\n", myId, node.getMyNode()));
                stringBuilder.append(writeGMLAttributes(node.getAttributes()));
                stringBuffer.append(stringBuilder);
            });
            graph.getAllEdgesInfo().forEach(edge -> stringBuffer.append(String.format("   edge [\n    source %s\n    target %s\n%s",
                    mapLabelToId.get(edge.getNodeFrom()), mapLabelToId.get(edge.getNodeTo()), writeGMLAttributes(edge.getAttributes()))));
            stringBuffer.append("]\n");

            fileWriter.write(stringBuffer.toString());
        } catch (IOException e) {
            throw new AdditionalInfoException(String.format("Some IO exception on a file: %s", fileName), e);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function.", e);
        }
    }

    /**
     * Helper function for writing GML attributes in correct format.
     *
     * @param attributes Attributes to write.
     * @return StringBuilder Containing formatted attributes.
     * @throws GraphNullException Null was passed to some internal function.
     */
    private static StringBuilder writeGMLAttributes(Map<String, Object> attributes) {
        try {
            StringBuilder stringBuilder = new StringBuilder(SMALL_CAPACITY);
            attributes.forEach((name, value) -> {
                try {
                    if (value instanceof Number) {
                        stringBuilder.append(String.format("    %s %s\n", name, value));
                    } else {
                        stringBuilder.append(String.format("    %s %s\n", name, Double.parseDouble((String) value)));
                    }
                } catch (NumberFormatException e) {
                    stringBuilder.append(String.format("    %s \"%s\"\n", name, value));
                }
            });
            stringBuilder.append("  ]\n");
            return stringBuilder;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function.", e);
        }
    }

    /**
     * The function generates GEXF file from graph.
     * Warning: Some functions use parallel streams may increase CPU usage.
     * Important: Some types of T not supported.
     *
     * @param fileName            Name of output file.
     * @param graph               GraphSlow to write to a file.
     * @param attributeNameWeight Name of attribute that will be used as weight.
     * @throws AdditionalInfoException IO exception on a file.
     * @throws GraphNullException      Null was passed to some internal function.
     */
    public static void writeGEXFFile(String fileName, Graph<?> graph, String attributeNameWeight) {
        try (FileWriter fileWriter = new FileWriter(fileName)) {
            StringBuffer stringBuffer = new StringBuffer(BIG_CAPACITY);
            stringBuffer.append(String.format("%s\n%s\n  %s\n", "<?xml version='1.0' encoding='utf-8'?>",
                    "<gexf xmlns=\"http://www.gexf.net/1.2draft\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.gexf.net/1.2draft http://www.gexf.net/1.2draft/gexf.xsd\" version=\"1.2\">",
                    "<graph defaultedgetype=\"directed\" mode=\"static\" name=\"\">"));
            Map<String, Integer> nodesToIdAttributes = new ConcurrentHashMap<>();
            stringBuffer.append(generateGEXFAttributes(graph.getAllNodeAttributes(), nodesToIdAttributes, "node"));
            Map<String, Integer> edgesToIdAttributes = new ConcurrentHashMap<>();
            stringBuffer.append(generateGEXFAttributes(graph.getAllEdgeAttributes(), edgesToIdAttributes, "edge"));
            if (graph.getNodeCount() > 0) {
                stringBuffer.append("    <nodes>\n");
                graph.getAllNodesInfo().parallelStream().forEach(node -> {
                    StringBuilder stringBuilder = new StringBuilder(SMALL_CAPACITY);
                    stringBuilder.append(String.format("      <node id=\"%s\" label=\"%s\">\n", node.getMyNode(), node.getMyNode()));
                    if (!node.getAttributes().isEmpty()) {
                        stringBuilder.append("        <attvalues>\n");
                        node.getAttributes().forEach((name, value) -> stringBuilder.append(
                                String.format("          <attvalue for=\"%s\" value=\"%s\" />\n", nodesToIdAttributes.get(name), value)));
                        stringBuilder.append("        </attvalues>\n");
                    }
                    stringBuilder.append("      </node>\n");
                    stringBuffer.append(stringBuilder);
                });
                stringBuffer.append("    </nodes>\n    <edges>\n");
                graph.getAllEdgesInfo().parallelStream().forEach(edge -> {
                    StringBuilder stringBuilder = new StringBuilder(SMALL_CAPACITY);
                    Map<String, Object> attributes = edge.getAttributes();
                    if (attributes.containsKey(attributeNameWeight)) {
                        stringBuilder.append(String.format("      <edge source=\"%s\" target=\"%s\" weight=\"%s\">\n", edge.getNodeFrom(), edge.getNodeTo(), attributes.get("weight")));
                    } else {
                        stringBuilder.append(String.format("      <edge source=\"%s\" target=\"%s\">\n", edge.getNodeFrom(), edge.getNodeTo()));
                    }
                    if (!attributes.isEmpty()) {
                        stringBuilder.append("        <attvalues>\n");
                        attributes.forEach((name, value) -> stringBuilder.append(
                                String.format("          <attvalue for=\"%s\" value=\"%s\" />\n", edgesToIdAttributes.get(name), value)));
                        stringBuilder.append("        </attvalues>\n");
                    }
                    stringBuilder.append("      </edge>\n");
                    stringBuffer.append(stringBuilder);
                });
                stringBuffer.append("    </edges>\n");
            }
            stringBuffer.append("  </graph>\n</gexf>");

            fileWriter.write(stringBuffer.toString());
        } catch (IOException e) {
            throw new AdditionalInfoException(String.format("Some IO exception on a file: %s", fileName), e);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function.", e);
        }
    }

    /**
     * Helper function for writing GEXF attributes definitions in correct format.
     *
     * @param allAttributes  Attributes to write.
     * @param toIdAttributes Map that is going to contain mapping from id of attribute to its name.
     * @param type           Edge/node attributes.
     * @return StringBuilder containing formatted attributes.
     * @throws GraphNullException Null was passed to some internal function.
     */
    private static StringBuilder generateGEXFAttributes(Map<String, Object> allAttributes, Map<String, Integer> toIdAttributes, String type) {
        try {
            StringBuilder stringBuilder = new StringBuilder(SMALL_CAPACITY);
            if (!allAttributes.isEmpty()) {
                stringBuilder.append(String.format("    <attributes mode=\"static\" class=\"%s\">\n", type));
                AtomicInteger id = new AtomicInteger();
                allAttributes.forEach((name, value) -> {
                    int myId = id.getAndIncrement();
                    toIdAttributes.put(name, myId);
                    stringBuilder.append(String.format("      <attribute id=\"%s\" title=\"%s\" type=\"%s\" />\n", myId, name,
                            value.getClass().toString().substring(value.getClass().toString().lastIndexOf(DOT) + 1).toLowerCase()));
                });
                stringBuilder.append("    </attributes>\n");
            }
            return stringBuilder;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function.", e);
        }
    }

    /**
     * The function generates CSV file from graph nodes.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param fileName Name of output file.
     * @param graph    GraphSlow to write to a file.
     * @throws AdditionalInfoException IO exception on a file.
     * @throws GraphNullException      Null was passed to some internal function.
     */
    public static void writeCSVFileNode(String fileName, Graph<?> graph) {
        try (FileWriter fileWriter = new FileWriter(fileName)) {
            StringBuffer stringBuffer = new StringBuffer(BIG_CAPACITY);
            List<String> nodeAttributes = new ArrayList<>(graph.getAllNodeAttributes().keySet());
            nodeAttributes.add("id");
            stringBuffer.append(generateCSVAttributes(nodeAttributes));
            int size = nodeAttributes.size();
            graph.getAllNodesInfo().parallelStream().forEach(node -> {
                List<Object> values = new ArrayList<>(Arrays.asList(new String[size]));
                node.getAttributes().forEach((attribute, value) -> values.set(nodeAttributes.indexOf(attribute), value));
                StringBuilder stringBuilder = new StringBuilder(SMALL_CAPACITY);
                values.forEach(t -> {
                    if (t != null) {
                        stringBuilder.append(t).append(",");
                    } else {
                        stringBuilder.append(node.getMyNode());
                    }
                });
                stringBuilder.append("\n");
                stringBuffer.append(stringBuilder);
            });
            fileWriter.write(stringBuffer.toString());
        } catch (IOException e) {
            throw new AdditionalInfoException(String.format("Some IO exception on a file: %s", fileName), e);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function.", e);
        }
    }

    /**
     * The function generates CSV file from graph edges.
     * Warning: Some functions use parallel streams - may increase CPU usage.
     *
     * @param fileName Name of output file.
     * @param graph    GraphSlow to write to a file.
     * @throws AdditionalInfoException IO exception on a file.
     * @throws GraphNullException      Null was passed to some internal function.
     */
    public static void writeCSVFileEdge(String fileName, Graph<?> graph) {
        try (FileWriter fileWriter = new FileWriter(fileName)) {
            StringBuffer stringBuffer = new StringBuffer(BIG_CAPACITY);
            List<String> edgeAttributes = new ArrayList<>(graph.getAllEdgeAttributes().keySet());
            edgeAttributes.add("source");
            edgeAttributes.add("target");
            stringBuffer.append(generateCSVAttributes(edgeAttributes));
            int size = edgeAttributes.size();
            graph.getAllEdgesInfo().parallelStream().forEach(edge -> {
                List<Object> values = new ArrayList<>(Arrays.asList(new String[size]));
                edge.getAttributes().forEach((attribute, value) -> values.set(edgeAttributes.indexOf(attribute), value));
                StringBuilder stringBuilder = new StringBuilder(SMALL_CAPACITY);
                for (Object t : values) {
                    if (t != null) {
                        stringBuilder.append(t).append(",");
                    } else {
                        stringBuilder.append(edge.getNodeFrom()).append(",").append(edge.getNodeTo());
                        break;
                    }
                }
                stringBuilder.append("\n");
                stringBuffer.append(stringBuilder);
            });
            fileWriter.write(stringBuffer.toString());
        } catch (IOException e) {
            throw new AdditionalInfoException(String.format("Some IO exception on a file: %s", fileName), e);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function.", e);
        }
    }

    private static StringBuilder generateCSVAttributes(Iterable<String> attributes) {
        StringBuilder stringBuilder = new StringBuilder(SMALL_CAPACITY);
        attributes.forEach(attribute -> stringBuilder.append(attribute.replaceAll(",", " ")).append(","));
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        stringBuilder.append("\n");
        return stringBuilder;
    }

    /**
     * The function for reading CSV file and filling the graph with nodes.
     *
     * @param fileName Name of a file to be read.
     * @param graph    GraphSlow initialised by the file.
     * @throws AdditionalInfoException Special exception with additional information if IO error occurs.
     * @throws IOGraphException        Invalid file, invalid formatting.
     * @throws GraphNullException      Null was passed to some internal function.
     */
    public static void readCSVFileNode(String fileName, Graph<String> graph) {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line = br.readLine();
            ArrayList<String> attributes = new ArrayList<>(Arrays.asList(line.split(",")));
            if (!attributes.contains("id"))
                throw new IOGraphException("Invalid format of CSV file, it should have id column");
            int id = attributes.indexOf("id");
            while ((line = br.readLine()) != null) {
                String[] split = line.split(",");
                String node = null;
                Map<String, Object> nodeAttributes = new ConcurrentHashMap<>();
                for (int index = 0, splitLength = split.length; index < splitLength; index++) {
                    if (index == id) {
                        node = split[index];
                    } else {
                        nodeAttributes.put(attributes.get(index), split[index]);
                    }
                }
                graph.addNode(node, nodeAttributes);
            }
        } catch (FileNotFoundException e) {
            throw new AdditionalInfoException(String.format("File was not found: %s", fileName), e);
        } catch (IOException e) {
            throw new IOGraphException(String.format("Some IO exception on file: %s", fileName), e);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function.", e);
        }
    }

    /**
     * The function for reading CSV file and filling the graph with edges.
     *
     * @param fileName Name of a file to be read.
     * @param graph    GraphSlow initialised by the file.
     * @throws AdditionalInfoException Special exception with additional information if IO error occurs.
     * @throws IOGraphException        Invalid file, invalid formatting.
     * @throws GraphNullException      Null was passed to some internal function.
     */
    public static void readCSVFileEdge(String fileName, Graph<String> graph) {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line = br.readLine();
            ArrayList<String> attributes = new ArrayList<>(Arrays.asList(line.split(",")));
            if (!attributes.contains("source") || !attributes.contains("target"))
                throw new IOGraphException("Invalid format of CSV file, it should have source and target columns");
            int source = attributes.indexOf("source");
            int target = attributes.indexOf("target");
            while ((line = br.readLine()) != null) {
                String[] split = line.split(",");
                String nodeSource = null;
                String nodeTarget = null;
                ConcurrentHashMap<String, Object> nodeAttributes = new ConcurrentHashMap<>();
                for (int index = 0, splitLength = split.length; index < splitLength; index++) {
                    if ((index != source) && (index != target)) {
                        nodeAttributes.put(attributes.get(index), split[index]);
                    } else if (index == source) {
                        nodeSource = split[index];
                    } else {
                        nodeTarget = split[index];
                    }
                }
                graph.addEdge(nodeSource, nodeTarget, nodeAttributes);
            }
        } catch (FileNotFoundException e) {
            throw new AdditionalInfoException(String.format("File was not found: %s", fileName), e);
        } catch (IOException e) {
            throw new IOGraphException(String.format("Some IO exception on file: %s", fileName), e);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to some internal function.", e);
        }
    }

}
