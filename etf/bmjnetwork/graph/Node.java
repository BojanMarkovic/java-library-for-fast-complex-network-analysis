package etf.bmjnetwork.graph;

import etf.bmjnetwork.exceptions.GraphNullException;
import etf.bmjnetwork.structures.Edge;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Generic node interface. Normal node intended for smaller graphs.
 *
 * @param <T> Node type.
 */
public interface Node<T> extends Serializable {
    /**
     * The function checks if edge to a node exists.
     *
     * @param node Object to which edge goes.
     * @return True if edge exists in a graph, false if it doesn't exist.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    boolean checkIfHaveEdge(T node);

    /**
     * The function returns edge object if it exists.
     *
     * @param node Object to which edge goes.
     * @return Edge object.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    Edge<T> getNeighbourEdge(T node);

    /**
     * The function removes edge.
     *
     * @param node Object to which edge goes.
     * @return Removed edge object.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    Edge<T> removeEdge(T node);

    /**
     * The function adds or changes attribute of the node.
     *
     * @param attributeName  Attribute name to associate to the node.
     * @param attributeValue Attribute value to associate to attribute name.
     * @return True if attribute didn't exist in the graph, false if it already existed.
     * @throws GraphNullException Null was passed to a function.
     */
    boolean addAttribute(String attributeName, Object attributeValue);

    /**
     * The function adds or changes attribute of a edge.
     *
     * @param node           Object to which edge goes.
     * @param attributeName  Attribute name to associate to the edge.
     * @param attributeValue Attribute value to associate to attribute name.
     * @return True if attribute didn't exist in the graph, false if it already existed or if edge doesn't exist.
     * @throws GraphNullException Neighbour doesn't exist or null was passed to a function.
     */
    boolean addAttributeToEdge(T node, String attributeName, Object attributeValue);

    /**
     * The function changes all attributes of a edge.
     *
     * @param node       Object to which edge goes.
     * @param attributes Attributes associated with edge.
     * @return True if attribute didn't exist in the graph, false if it already existed or if edge doesn't exist.
     * @throws GraphNullException Neighbour doesn't exist or null was passed to a function.
     */
    boolean setAttributesToEdge(T node, ConcurrentHashMap<String, Object> attributes);

    /**
     * The function returns all edge objects from graph.
     *
     * @return List of edge objects.
     */
    List<Edge<T>> getNeighboursEdgeInfo();

    /**
     * The function returns all neighbour attributes.
     *
     * @return All neighbour attributes.
     */
    Collection<ConcurrentHashMap<String, Object>> getNeighboursAttributes();

    /**
     * The function returns all neighbours and its edge attributes as hashmap.
     *
     * @return Hashmap of all neighbours.
     */
    Map<T, Map<String, Object>> getNeighboursInternal();

    /**
     * The function returns all neighbour objects.
     *
     * @return Neighbour objects.
     */
    List<T> getNeighbours();

    /**
     * The function returns all attributes of the node.
     *
     * @return Attributes of the node.
     */
    Map<String, Object> getAttributes();

    /**
     * The function changes all attributes of the node.
     *
     * @param attributes Attributes associated with node.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    void setAttributes(Map<String, Object> attributes);

    /**
     * The function returns object stored in node.
     *
     * @return Object from the node.
     */
    T getMyNode();

    /**
     * The function removes all generated attributes from a node.
     */
    void removeGeneratedAttributes();

    /**
     * The function for changing name attribute of node.
     *
     * @param oldColumnName Old attribute name.
     * @param newColumnName New attribute name.
     * @throws GraphNullException Attribute doesn't exist or null was passed to a function.
     */
    void changeNameOfNodeAttribute(String oldColumnName, String newColumnName);

    /**
     * The function for changing name attribute of all edges.
     *
     * @param oldColumnName Old attribute name.
     * @param newColumnName New attribute name.
     * @throws GraphNullException Attribute doesn't exist or null was passed to a function.
     */
    void changeNameOfEdgeAttribute(String oldColumnName, String newColumnName);

    /**
     * The function returns desired node attribute.
     *
     * @param columnName Attribute to get from node.
     * @return Node attribute.
     * @throws GraphNullException Attribute doesn't exist.
     */
    Object getNodeAttribute(String columnName);

    /**
     * The function returns desired edge attribute.
     *
     * @param nodeTo     Edge goes to this node.
     * @param columnName Attribute to get from edge.
     * @return Edge attribute.
     * @throws GraphNullException Neighbour or attribute don't exist.
     */
    Object getEdgeAttribute(T nodeTo, String columnName);

    /**
     * The function removes attribute from node.
     *
     * @param columnName Attribute to remove from node.
     * @return Node attribute.
     * @throws GraphNullException Attribute doesn't exist.
     */
    Object removeNodeAttribute(String columnName);

    /**
     * The function removes attribute from all edges of the node.
     *
     * @param columnName Attribute to remove from edges.
     * @throws GraphNullException Attribute doesn't exist.
     */
    void removeEdgeAttribute(String columnName);

    /**
     * The function removes attribute from edges of the node.
     *
     * @param nodeTo     Edge goes to this node.
     * @param columnName Attribute to remove from edges.
     * @return Edge attribute.
     * @throws GraphNullException Neighbour or attribute don't exist.
     */
    Object removeEdgeAttribute(T nodeTo, String columnName);
}
