package etf.bmjnetwork.graph;

import java.util.Map;

/**
 * Generic node class. It is thread-safe. Fast node intended for extremely large graphs.
 * Warning: Overridden functions use parallel streams - may increase CPU usage.
 *
 * @param <T> Node type.
 */
public class NodeFast<T> extends NodeImpl<T> {
    private static final long serialVersionUID = -6223203136495112055L;

    NodeFast(T node) {
        super(node);
    }

    NodeFast(T node, Map<String, Object> attributes) {
        super(node, attributes);
    }

    /**
     * The function removes all generated attributes from a node.
     * Warning: Overridden functions use parallel streams - may increase CPU usage.
     */
    @Override
    public void removeGeneratedAttributes() {
        attributes.forEach((key, value) -> {
            if (key.startsWith(GENERATED_START_ATTRIBUTE)) attributes.remove(key);
        });
        neighbours.values().parallelStream().forEach(attribute -> attribute.keySet().stream()
                .filter(key -> key.startsWith(GENERATED_START_ATTRIBUTE)).forEach(attribute::remove));
    }

    /**
     * Helper function for generating string from edges.
     * Warning: Overridden functions use parallel streams - may increase CPU usage.
     *
     * @return Neighbours converted to string.
     */
    @Override
    protected String getNeighboursToString() {
        StringBuffer stringBuffer = new StringBuffer(STRING_BUILDER_START_CAPACITY);
        stringBuffer.append("Edge{ nodeFrom=");
        neighbours.entrySet().parallelStream().forEach(entry -> stringBuffer.append(writeEdgeToString(entry)));
        return stringBuffer.toString();
    }
}
