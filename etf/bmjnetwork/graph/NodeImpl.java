package etf.bmjnetwork.graph;

import etf.bmjnetwork.exceptions.GraphNullException;
import etf.bmjnetwork.structures.Edge;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Generic node class. It is thread-safe. Normal node intended for smaller graphs.
 *
 * @param <T> Node type.
 */
public class NodeImpl<T> implements Node<T> {
    static final int STRING_BUILDER_START_CAPACITY = 100;
    static final String GENERATED_START_ATTRIBUTE = "_*";
    private static final long serialVersionUID = 8577462164853878829L;
    ConcurrentHashMap<T, ConcurrentHashMap<String, Object>> neighbours = new ConcurrentHashMap<>();
    Map<String, Object> attributes = new ConcurrentHashMap<>();
    private T myNode;

    /**
     * Constructor of node class.
     *
     * @param node Object to be in a node.
     * @throws GraphNullException Null parameter was passed to a constructor.
     */
    NodeImpl(T node) {
        if (node == null) throw new GraphNullException("Null was passed to Node constructor!");
        myNode = node;
    }

    /**
     * Constructor of node class.
     *
     * @param node       Object to be in a node.
     * @param attributes Attributes associated with node.
     * @throws GraphNullException Null parameter was passed to a constructor.
     */
    NodeImpl(T node, Map<String, Object> attributes) {
        if (node == null) throw new GraphNullException("Null was passed to Node constructor!");
        try {
            myNode = node;
            this.attributes = new ConcurrentHashMap<>(attributes);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to Node constructor!", e);
        }
    }

    @Override
    public boolean checkIfHaveEdge(T node) {
        try {
            return neighbours.containsKey(node);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to checkIfHaveEdge!", e);
        }
    }

    @Override
    public Edge<T> getNeighbourEdge(T node) {
        try {
            return neighbours.containsKey(node) ? new Edge<>(myNode, node, neighbours.get(node)) : null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getNeighbourEdge!", e);
        }
    }

    /**
     * The function adds new neighbour. If it already exists nothing happens.
     *
     * @param destination Object to which edge goes.
     * @return True if neighbour didn't exist in the graph, false if it already existed.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    boolean addNeighbour(T destination) {
        try {
            return neighbours.putIfAbsent(destination, new ConcurrentHashMap<>()) == null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addNeighbour!", e);
        }
    }

    /**
     * The function adds new neighbour. If it already exists nothing happens.
     *
     * @param destination    Object to which edge goes.
     * @param attributeName  Attribute name to associate to the edge.
     * @param attributeValue Attribute value to associate to attribute name.
     * @return True if neighbour didn't exist in the graph, false if it already existed.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    boolean addNeighbour(T destination, String attributeName, Object attributeValue) {
        try {
            ConcurrentHashMap<String, Object> edgeAttributes = new ConcurrentHashMap<>();
            edgeAttributes.put(attributeName, attributeValue);
            return neighbours.putIfAbsent(destination, edgeAttributes) == null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addNeighbour!", e);
        }
    }

    /**
     * The function adds new neighbour. If it already exists nothing happens.
     *
     * @param destination Object to which edge goes.
     * @param attributes  Attributes associated with edge.
     * @return True if neighbour didn't exist in the graph, false if it already existed.
     * @throws GraphNullException Null parameter was passed to a function.
     */
    boolean addNeighbour(T destination, ConcurrentHashMap<String, Object> attributes) {
        try {
            return neighbours.putIfAbsent(destination, attributes) == null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addNeighbour!", e);
        }
    }

    @Override
    public Edge<T> removeEdge(T node) {
        try {
            return neighbours.containsKey(node) ? new Edge<>(myNode, node, neighbours.remove(node)) : null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to removeEdge!", e);
        }
    }

    @Override
    public boolean addAttribute(String attributeName, Object attributeValue) {
        try {
            return attributes.put(attributeName, attributeValue) == null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addAttribute!", e);
        }
    }

    @Override
    public boolean addAttributeToEdge(T node, String attributeName, Object attributeValue) {
        try {
            return neighbours.get(node).put(attributeName, attributeValue) == null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Neighbour doesn't exist or null was passed to addAttributeToEdge!", e);
        }
    }

    @Override
    public boolean setAttributesToEdge(T node, ConcurrentHashMap<String, Object> attributes) {
        try {
            return neighbours.put(node, attributes) == null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Neighbour doesn't exist or null was passed to setAttributesToEdge!", e);
        }
    }

    @Override
    public List<Edge<T>> getNeighboursEdgeInfo() {
        ArrayList<Edge<T>> edges = new ArrayList<>();
        neighbours.forEach((nodeTo, edgeAttributes) -> edges.add(new Edge<>(myNode, nodeTo, edgeAttributes)));
        return edges;
    }

    @Override
    public Collection<ConcurrentHashMap<String, Object>> getNeighboursAttributes() {
        return neighbours.values();
    }

    @Override
    public Map<T, Map<String, Object>> getNeighboursInternal() {
        return Collections.unmodifiableMap(neighbours);
    }

    @Override
    public List<T> getNeighbours() {
        return new ArrayList<>(neighbours.keySet());
    }

    @Override
    public Map<String, Object> getAttributes() {
        return Collections.unmodifiableMap(attributes);
    }

    @Override
    public void setAttributes(Map<String, Object> attributes) {
        try {
            this.attributes = new ConcurrentHashMap<>(attributes);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to setAttributes!", e);
        }
    }

    @Override
    public T getMyNode() {
        return myNode;
    }

    /**
     * Helper function for generating string from edges.
     *
     * @return Neighbours converted to string.
     */
    String getNeighboursToString() {
        StringBuffer stringBuffer = new StringBuffer(STRING_BUILDER_START_CAPACITY);
        stringBuffer.append("Edge{ nodeFrom=");
        neighbours.entrySet().forEach(entry -> stringBuffer.append(writeEdgeToString(entry)));
        return stringBuffer.toString();
    }

    /**
     * Helper function for generating string from edges.
     *
     * @param entry Internal object for gathering required info about edge.
     * @return Edge converted to String.
     */
    StringBuilder writeEdgeToString(Map.Entry<T, ConcurrentHashMap<String, Object>> entry) {
        StringBuilder stringBuilder = new StringBuilder(STRING_BUILDER_START_CAPACITY);
        stringBuilder.append(String.format("Edge{ nodeFrom= %s, nodeTo= %s, attributes= ", myNode, entry.getKey()));
        Map<String, Object> edgeAttributes = entry.getValue();
        if (edgeAttributes.isEmpty()) {
            stringBuilder.append("No attributes!");
        } else {
            edgeAttributes.forEach((name, value) -> stringBuilder.append(String.format("Name= %s, Value= %s", name, value)));
        }
        return stringBuilder;
    }

    /**
     * Helper function for generating string from node.
     *
     * @return Attributes converted to string.
     */
    private String attributesToString() {
        if (attributes.isEmpty()) return "No attributes!";
        StringBuilder stringBuilder = new StringBuilder(STRING_BUILDER_START_CAPACITY);
        attributes.forEach((name, value) -> stringBuilder.append(String.format("Name= %s, Value= %s", name, value)));
        return stringBuilder.toString();
    }

    /**
     * The function prints formatted node.
     *
     * @return Node in string format.
     */
    @Override
    public String toString() {
        return String.format("Node'{' node= %s, neighbours= %s, attributes= %s'}'\n", myNode, getNeighboursToString(), attributesToString());
    }

    @Override
    public void removeGeneratedAttributes() {
        attributes.forEach((key, value) -> {
            if (key.startsWith(GENERATED_START_ATTRIBUTE)) attributes.remove(key);
        });
        neighbours.values().forEach(attribute -> attribute.keySet().forEach(key -> {
            if (key.startsWith(GENERATED_START_ATTRIBUTE)) {
                attribute.remove(key);
            }
        }));
    }

    /**
     * Helper function for manipulation on a label attribute after reading a GEXF or GML file.
     *
     * @param mapIdToLabel Map containing id to label mapping.
     * @throws GraphNullException Attribute don't exist or null was passed to a function.
     */
    void changeNodeToLabelAttribute(Map<T, T> mapIdToLabel) {
        try {
            myNode = mapIdToLabel.get(myNode);
            attributes.remove("label");
            ConcurrentHashMap<T, ConcurrentHashMap<String, Object>> newNeighbours = new ConcurrentHashMap<>();
            neighbours.forEach((neighbour, edgeAttributes) -> newNeighbours.put(mapIdToLabel.get(neighbour), edgeAttributes));
            neighbours = newNeighbours;
        } catch (NullPointerException e) {
            throw new GraphNullException("Neighbour or attribute doesn't exist or null was passed to a function!", e);
        }
    }

    @Override
    public void changeNameOfNodeAttribute(String oldColumnName, String newColumnName) {
        try {
            Object value = attributes.remove(oldColumnName);
            if (value != null) {
                attributes.put(newColumnName, value);
            }
        } catch (NullPointerException e) {
            throw new GraphNullException("Attribute doesn't exist or null was passed to a function!", e);
        }
    }

    @Override
    public void changeNameOfEdgeAttribute(String oldColumnName, String newColumnName) {
        try {
            neighbours.values().forEach(edgeAttributes -> {
                Object value = edgeAttributes.remove(oldColumnName);
                if (value != null) {
                    edgeAttributes.put(newColumnName, value);
                }
            });
        } catch (NullPointerException e) {
            throw new GraphNullException("Attribute doesn't exist or null was passed to a function!", e);
        }
    }

    @Override
    public Object getNodeAttribute(String columnName) {
        try {
            return attributes.get(columnName);
        } catch (NullPointerException e) {
            throw new GraphNullException("Attribute doesn't exist or null was passed to a function!", e);
        }
    }

    @Override
    public Object getEdgeAttribute(T nodeTo, String columnName) {
        try {
            return neighbours.get(nodeTo).get(columnName);
        } catch (NullPointerException e) {
            throw new GraphNullException("Neighbour or attribute don't exist or null was passed to a function!", e);
        }
    }

    @Override
    public Object removeNodeAttribute(String columnName) {
        try {
            return attributes.remove(columnName);
        } catch (NullPointerException e) {
            throw new GraphNullException("Attribute doesn't exist or null was passed to a function!", e);
        }
    }

    @Override
    public void removeEdgeAttribute(String columnName) {
        try {
            neighbours.forEach((neighbour, attribute) -> attribute.remove(columnName));
        } catch (NullPointerException e) {
            throw new GraphNullException("Attribute doesn't exist or null was passed to a function!", e);
        }
    }

    @Override
    public Object removeEdgeAttribute(T nodeTo, String columnName) {
        try {
            return neighbours.get(nodeTo).remove(columnName);
        } catch (NullPointerException e) {
            throw new GraphNullException("Neighbour or attribute don't exist or null was passed to a function!", e);
        }
    }
}
