package etf.bmjnetwork.graph;

import etf.bmjnetwork.exceptions.GraphNullException;
import etf.bmjnetwork.structures.Edge;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static etf.bmjnetwork.graph.NodeImpl.GENERATED_START_ATTRIBUTE;

public class UndirectedGraph<T> extends Graph<T> {
    private static final long serialVersionUID = 1554398221422971275L;
    private static final double CONSTANT_2 = 2.0;

    @Override
    public Edge<T> removeEdge(T nodeFrom, T nodeTo) {
        try {
            if (internalGraph.containsKey(nodeTo)) {
                internalGraph.get(nodeTo).removeEdge(nodeFrom);
            }
            return internalGraph.containsKey(nodeFrom) ? internalGraph.get(nodeFrom).removeEdge(nodeTo) : null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to removeEdge!", e);
        }
    }

    @Override
    public boolean addEdge(T source, T destination) {
        addNode(source);
        addNode(destination);
        ((NodeImpl<T>) internalGraph.get(destination)).addNeighbour(source);
        return ((NodeImpl<T>) internalGraph.get(source)).addNeighbour(destination);
    }

    @Override
    public boolean addEdge(T source, T destination, String attributeName, Object attributeValue) {
        addNode(source);
        addNode(destination);
        ((NodeImpl<T>) internalGraph.get(destination)).addNeighbour(source, attributeName, attributeValue);
        return ((NodeImpl<T>) internalGraph.get(source)).addNeighbour(destination, attributeName, attributeValue);

    }

    @Override
    public boolean addEdge(T source, T destination, ConcurrentHashMap<String, Object> attributes) {
        if (attributes == null) throw new GraphNullException("Null was passed to addEdge!");
        addNode(source);
        addNode(destination);
        ((NodeImpl<T>) internalGraph.get(destination)).addNeighbour(source, attributes);
        return ((NodeImpl<T>) internalGraph.get(source)).addNeighbour(destination, attributes);
    }

    @Override
    public boolean addAttributeToEdge(T source, T destination, String attributeName, Object attributeValue) {
        try {
            internalGraph.get(destination).addAttributeToEdge(source, attributeName, attributeValue);
            return internalGraph.get(source).addAttributeToEdge(destination, attributeName, attributeValue);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addAttributeToEdge!", e);
        }
    }

    @Override
    public void addAttributeToEdge(T source, T destination, ConcurrentHashMap<String, Object> attributes) {
        try {
            internalGraph.get(destination).setAttributesToEdge(source, attributes);
            internalGraph.get(source).setAttributesToEdge(destination, attributes);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addAttributeToEdge!", e);
        }
    }

    @Override
    public void addEdgesFrom(List<T> sourceList, List<T> destinationList) {
        try {
            for (int index = 0, size = sourceList.size(); index < size; index++) {
                addEdge(sourceList.get(index), destinationList.get(index));
            }
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addEdgesFrom!", e);
        }
    }

    @Override
    public void generateInDegreeAttributes() {
        getAverageDegree(true);
    }

    @Override
    public void generateWeightedInDegreeAttributes(String columnToUseAsWeight) {
        getAverageWeightedDegree(columnToUseAsWeight, true);
    }

    @Override
    public List<Edge<T>> getInNeighbours(T nodeTo) {
        return internalGraph.get(nodeTo).getNeighboursEdgeInfo();
    }

    @Override
    public Map<T, ConcurrentHashMap<T, ArrayList<ArrayList<T>>>> calculateLowestNumberOfHopsPath() {
        Map<T, ConcurrentHashMap<T, ArrayList<ArrayList<T>>>> distances = new ConcurrentHashMap<>();
        internalGraph.values().forEach(node -> {
            T myNode = node.getMyNode();
            ConcurrentHashMap<T, ArrayList<ArrayList<T>>> routes = new ConcurrentHashMap<>();
            Collection<T> currentLevel = new HashSet<>(node.getNeighbours());
            Collection<T> visitedSet = new HashSet<>();

            currentLevel.remove(myNode);
            visitedSet.add(myNode);

            Collection<T> nextLevel = new HashSet<>();
            currentLevel.forEach(nodeTo -> {
                ArrayList<T> route = new ArrayList<>();
                route.add(myNode);
                route.add(nodeTo);
                ArrayList<ArrayList<T>> newRoutes = new ArrayList<>();
                newRoutes.add(route);
                routes.put(nodeTo, newRoutes);
                nextLevel.addAll(internalGraph.get(nodeTo).getNeighbours());
            });

            while (!nextLevel.isEmpty()) {
                visitedSet.addAll(currentLevel);
                currentLevel.clear();
                nextLevel.forEach(value -> {
                    if (!visitedSet.contains(value)) {
                        currentLevel.add(value);
                    }
                });
                nextLevel.clear();

                currentLevel.forEach(nodeTo -> {
                    ArrayList<ArrayList<T>> newRoutes = new ArrayList<>();
                    List<T> inNeighbours = internalGraph.get(nodeTo).getNeighbours();
                    routes.forEach((key, value) -> {
                        if (inNeighbours.contains(key) && !currentLevel.contains(key)) {
                            value.forEach(oneRoute -> {
                                ArrayList<T> route = new ArrayList<>(oneRoute);
                                route.add(nodeTo);
                                newRoutes.add(route);
                            });
                        }
                    });


                    routes.put(nodeTo, newRoutes);
                    nextLevel.addAll(internalGraph.get(nodeTo).getNeighbours());
                });
            }
            distances.put(myNode, routes);
        });
        return distances;
    }

    @Override
    public int getEdgesCount() {
        return super.getEdgesCount() / 2;
    }

    @Override
    public List<Edge<T>> getAllEdgesInfo() {
        List<Edge<T>> edges = Collections.synchronizedList(new ArrayList<>());
        internalGraph.values().forEach(node -> edges.addAll(node.getNeighboursEdgeInfo()));
        for (int i = 0; i < edges.size(); i++) {
            Edge<T> edge = edges.get(i);
            edges.removeIf(tempEdge ->
                    (edge.getNodeFrom().equals(tempEdge.getNodeFrom())) && (edge.getNodeTo().equals(tempEdge.getNodeTo())));
        }
        return edges;
    }

    @Override
    public double getDensity() {
        double size = internalGraph.size();
        return CONSTANT_2 * getEdgesCount() / (size * (size - 1.0));
    }

    @Override
    public ConcurrentHashMap<T, Double> getBetweennessCentrality(Boolean writeAsAttribute) {
        try {
            ConcurrentHashMap<T, Double> betweenness = new ConcurrentHashMap<>();
            Map<T, ConcurrentHashMap<T, ArrayList<ArrayList<T>>>> shortestPaths = calculateLowestNumberOfHopsPath();
            shortestPaths.keySet().forEach(key -> {
                AtomicReference<Double> betweennessSum = new AtomicReference<>(0.0);
                internalGraph.keySet().forEach(node -> {
                    if (!node.equals(key)) {
                        shortestPaths.get(node).forEach((nodeTo, paths) -> {
                            if (!nodeTo.equals(key)) {
                                AtomicInteger sumTotal = new AtomicInteger();
                                AtomicInteger sum = new AtomicInteger();
                                sumTotal.addAndGet(paths.size());
                                paths.forEach(path -> {
                                    if (path.contains(key)) {
                                        sum.getAndIncrement();
                                    }
                                });
                                if (sumTotal.get() != 0) {
                                    betweennessSum.updateAndGet(v -> v + sum.doubleValue() / sumTotal.doubleValue());
                                }
                            }
                        });
                    }
                });
                betweenness.put(key, betweennessSum.get() / CONSTANT_2);
            });

            if (writeAsAttribute) {
                internalGraph.values().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "BetweennessCentrality", betweenness.get(node.getMyNode())));
            }

            return betweenness;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getBetweennessCentrality!", e);
        }
    }

    @Override
    void runHits(int maxIterations, double epsilon) {
        try {
            if (hashedInternalGraph != internalGraph.hashCode()) {
                ConcurrentHashMap<T, Double> hubs = new ConcurrentHashMap<>();
                ConcurrentHashMap<T, Double> authorities = new ConcurrentHashMap<>();
                internalGraph.values().forEach(node -> {
                    T key = node.getMyNode();
                    hubs.put(key, 1.0);
                    authorities.put(key, 1.0);
                });
                Map<T, Double> lastHubs = new ConcurrentHashMap<>(hubs);
                for (int count = 0; count < maxIterations; count++) {
                    AtomicReference<Double> normalised = new AtomicReference<>((double) 0);
                    internalGraph.values().forEach(node -> {
                        T myNode = node.getMyNode();
                        AtomicReference<Double> sum = new AtomicReference<>((double) 0);
                        node.getNeighbours().forEach(edge -> sum.updateAndGet(v -> v + hubs.get(edge)));
                        double authority = sum.get();
                        authorities.put(myNode, authority);
                        normalised.updateAndGet(v -> v + authority * authority);
                    });
                    normalised.set(Math.sqrt(normalised.get()));
                    authorities.forEach((key1, value1) -> authorities.put(key1, value1 / normalised.get()));

                    internalGraph.values().forEach(node -> {
                        AtomicReference<Double> sum = new AtomicReference<>((double) 0);
                        node.getNeighbours().forEach(edge -> sum.updateAndGet(v -> v + authorities.get(edge)));
                        double hub = sum.get();
                        hubs.put(node.getMyNode(), hub);
                        normalised.updateAndGet(v -> v + hub * hub);
                    });
                    normalised.set(Math.sqrt(normalised.get()));
                    hubs.forEach((key, value) -> hubs.put(key, value / normalised.get()));
                    AtomicReference<Double> sum = new AtomicReference<>((double) 0);
                    lastHubs.forEach((key, value) -> sum.updateAndGet(v -> v + Math.abs(value - hubs.get(key))));
                    if (sum.get() < epsilon) {
                        break;
                    }
                    lastHubs = new ConcurrentHashMap<>(hubs);
                }
                authorityMap = authorities;
                hubsMap = hubs;
                hashedInternalGraph = internalGraph.hashCode();
            }
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to runHits!", e);
        }
    }
}
