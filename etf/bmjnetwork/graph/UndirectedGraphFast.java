package etf.bmjnetwork.graph;

import etf.bmjnetwork.exceptions.GraphNullException;
import etf.bmjnetwork.structures.Edge;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static etf.bmjnetwork.graph.NodeImpl.GENERATED_START_ATTRIBUTE;


/**
 * Generic undirected graph abstract class. It is thread-safe. Intended to be used with thousands/millions of nodes and edges.
 * Warning: Some functions use parallel streams - may increase CPU usage.
 *
 * @param <T> Node type of a graph.
 */
public class UndirectedGraphFast<T> extends GraphFast<T> {
    private static final long serialVersionUID = 3044608530778899297L;
    private static final double CONSTANT_2 = 2.0;

    @Override
    public Edge<T> removeEdge(T nodeFrom, T nodeTo) {
        try {
            if (internalGraph.containsKey(nodeTo)) {
                internalGraph.get(nodeTo).removeEdge(nodeFrom);
            }
            return internalGraph.containsKey(nodeFrom) ? internalGraph.get(nodeFrom).removeEdge(nodeTo) : null;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to removeEdge!", e);
        }
    }

    @Override
    public boolean addEdge(T source, T destination) {
        addNode(source);
        addNode(destination);
        ((NodeImpl<T>) internalGraph.get(destination)).addNeighbour(source);
        return ((NodeImpl<T>) internalGraph.get(source)).addNeighbour(destination);
    }

    @Override
    public boolean addEdge(T source, T destination, String attributeName, Object attributeValue) {
        addNode(source);
        addNode(destination);
        ((NodeImpl<T>) internalGraph.get(destination)).addNeighbour(source, attributeName, attributeValue);
        return ((NodeImpl<T>) internalGraph.get(source)).addNeighbour(destination, attributeName, attributeValue);

    }

    @Override
    public boolean addEdge(T source, T destination, ConcurrentHashMap<String, Object> attributes) {
        if (attributes == null) throw new GraphNullException("Null was passed to addEdge!");
        addNode(source);
        addNode(destination);
        ((NodeImpl<T>) internalGraph.get(destination)).addNeighbour(source, attributes);
        return ((NodeImpl<T>) internalGraph.get(source)).addNeighbour(destination, attributes);
    }

    @Override
    public boolean addAttributeToEdge(T source, T destination, String attributeName, Object attributeValue) {
        try {
            internalGraph.get(destination).addAttributeToEdge(source, attributeName, attributeValue);
            return internalGraph.get(source).addAttributeToEdge(destination, attributeName, attributeValue);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addAttributeToEdge!", e);
        }
    }

    @Override
    public void addAttributeToEdge(T source, T destination, ConcurrentHashMap<String, Object> attributes) {
        try {
            internalGraph.get(destination).setAttributesToEdge(source, attributes);
            internalGraph.get(source).setAttributesToEdge(destination, attributes);
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addAttributeToEdge!", e);
        }
    }

    @Override
    public void addEdgesFrom(List<T> sourceList, List<T> destinationList) {
        try {
            IntStream.range(0, sourceList.size()).parallel().forEach(index -> addEdge(sourceList.get(index), destinationList.get(index)));
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to addEdgesFrom!", e);
        }
    }

    @Override
    public void generateInDegreeAttributes() {
        getAverageDegree(true);
    }

    @Override
    public void generateWeightedInDegreeAttributes(String columnToUseAsWeight) {
        getAverageWeightedDegree(columnToUseAsWeight, true);
    }

    @Override
    public List<Edge<T>> getInNeighbours(T nodeTo) {
        return internalGraph.get(nodeTo).getNeighboursEdgeInfo();
    }

    @Override
    public Map<T, ConcurrentHashMap<T, ArrayList<ArrayList<T>>>> calculateLowestNumberOfHopsPath() {
        Map<T, ConcurrentHashMap<T, ArrayList<ArrayList<T>>>> distances = new ConcurrentHashMap<>();
        internalGraph.values().parallelStream().forEach(node -> {
            T myNode = node.getMyNode();
            ConcurrentHashMap<T, ArrayList<ArrayList<T>>> routes = new ConcurrentHashMap<>();
            HashSet<T> currentLevel = new HashSet<>(node.getNeighbours());
            Collection<T> visitedSet = new HashSet<>();

            currentLevel.remove(myNode);
            visitedSet.add(myNode);

            Collection<T> nextLevel = new HashSet<>();
            currentLevel.forEach(nodeTo -> {
                ArrayList<T> route = new ArrayList<>();
                route.add(myNode);
                route.add(nodeTo);
                ArrayList<ArrayList<T>> newRoutes = new ArrayList<>();
                newRoutes.add(route);
                routes.put(nodeTo, newRoutes);
                nextLevel.addAll(internalGraph.get(nodeTo).getNeighbours());
            });

            while (!nextLevel.isEmpty()) {
                visitedSet.addAll(currentLevel);
                currentLevel.clear();
                currentLevel.addAll(nextLevel.stream().filter(t -> !visitedSet.contains(t)).collect(Collectors.toSet()));
                nextLevel.clear();

                currentLevel.forEach(nodeTo -> {
                    ArrayList<ArrayList<T>> newRoutes = new ArrayList<>();
                    List<T> inNeighbours = internalGraph.get(nodeTo).getNeighbours();
                    routes.entrySet().stream().filter(entry -> inNeighbours.contains(entry.getKey()) && !currentLevel.contains(entry.getKey())).forEach(entry ->
                            entry.getValue().forEach(oneRoute -> {
                                ArrayList<T> route = new ArrayList<>(oneRoute);
                                route.add(nodeTo);
                                newRoutes.add(route);
                            }));


                    routes.put(nodeTo, newRoutes);
                    nextLevel.addAll(internalGraph.get(nodeTo).getNeighbours());
                });
            }
            distances.put(myNode, routes);
        });
        return distances;
    }

    @Override
    public int getEdgesCount() {
        return super.getEdgesCount() / 2;
    }

    /**
     * The function returns all edge objects from graph.
     * Warning: Function uses parallel streams - may increase CPU usage.
     *
     * @return List of edge objects.
     */
    @Override
    public List<Edge<T>> getAllEdgesInfo() {
        List<Edge<T>> edges = Collections.synchronizedList(new ArrayList<>());
        internalGraph.values().parallelStream().map(Node::getNeighboursEdgeInfo).forEach(edges::addAll);
        for (int i = 0; i < edges.size(); i++) {
            Edge<T> edge = edges.get(i);
            edges.removeIf(tempEdge ->
                    (edge.getNodeFrom().equals(tempEdge.getNodeFrom())) && (edge.getNodeTo().equals(tempEdge.getNodeTo())));
        }
        return edges;
    }

    @Override
    public double getDensity() {
        double size = internalGraph.size();
        return CONSTANT_2 * getEdgesCount() / (size * (size - 1.0));
    }

    @Override
    public ConcurrentHashMap<T, Double> getBetweennessCentrality(Boolean writeAsAttribute) {
        try {
            ConcurrentHashMap<T, Double> betweenness = new ConcurrentHashMap<>();
            Map<T, ConcurrentHashMap<T, ArrayList<ArrayList<T>>>> shortestPaths = calculateLowestNumberOfHopsPath();
            shortestPaths.entrySet().parallelStream().map(Map.Entry::getKey).forEach(key -> {
                AtomicReference<Double> betweennessSum = new AtomicReference<>(0.0);
                internalGraph.keySet().stream().filter(node -> !node.equals(key)).forEach(node -> shortestPaths.get(node).forEach((nodeTo, paths) -> {
                    if (!nodeTo.equals(key)) {
                        AtomicInteger sumTotal = new AtomicInteger();
                        AtomicInteger sum = new AtomicInteger();
                        sumTotal.addAndGet(paths.size());
                        paths.stream().filter(path -> path.contains(key)).forEach(path -> sum.getAndIncrement());
                        if (sumTotal.get() != 0) {
                            betweennessSum.updateAndGet(v -> v + sum.doubleValue() / sumTotal.doubleValue());
                        }
                    }
                }));
                betweenness.put(key, betweennessSum.get() / CONSTANT_2);
            });

            if (writeAsAttribute) {
                internalGraph.values().parallelStream().forEach(node -> node.addAttribute(
                        GENERATED_START_ATTRIBUTE + "BetweennessCentrality", betweenness.get(node.getMyNode())));
            }

            return betweenness;
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to getBetweennessCentrality!", e);
        }
    }

    @Override
    protected void runHits(int maxIterations, double epsilon) {
        try {
            if (hashedInternalGraph != internalGraph.hashCode()) {
                ConcurrentHashMap<T, Double> hubs = new ConcurrentHashMap<>();
                ConcurrentHashMap<T, Double> authorities = new ConcurrentHashMap<>();
                internalGraph.values().parallelStream().map(Node::getMyNode).forEach(myNode -> {
                    hubs.put(myNode, 1.0);
                    authorities.put(myNode, 1.0);
                });
                Map<T, Double> lastHubs = new ConcurrentHashMap<>(hubs);
                for (int count = 0; count < maxIterations; count++) {
                    AtomicReference<Double> normalised = new AtomicReference<>((double) 0);
                    internalGraph.values().parallelStream().forEach(node -> {
                        T myNode = node.getMyNode();
                        double authority = node.getNeighbours().stream().mapToDouble(hubs::get).sum();
                        authorities.put(myNode, authority);
                        normalised.updateAndGet(v -> v + authority * authority);
                    });
                    normalised.set(Math.sqrt(normalised.get()));
                    authorities.entrySet().parallelStream().forEach(entry -> authorities.put(entry.getKey(), entry.getValue() / normalised.get()));

                    internalGraph.values().parallelStream().forEach(node -> {
                        double hub = node.getNeighbours().stream().mapToDouble(authorities::get).sum();
                        hubs.put(node.getMyNode(), hub);
                        normalised.updateAndGet(v -> v + hub * hub);
                    });
                    normalised.set(Math.sqrt(normalised.get()));
                    hubs.entrySet().parallelStream().forEach(entry -> hubs.put(entry.getKey(), entry.getValue() / normalised.get()));
                    double sum = lastHubs.entrySet().stream().mapToDouble(entry -> Math.abs(entry.getValue() - hubs.get(entry.getKey()))).sum();
                    if (sum < epsilon) {
                        break;
                    }
                    lastHubs = new ConcurrentHashMap<>(hubs);
                }
                authorityMap = authorities;
                hubsMap = hubs;
                hashedInternalGraph = internalGraph.hashCode();
            }
        } catch (NullPointerException e) {
            throw new GraphNullException("Null was passed to runHits!", e);
        }
    }
}
