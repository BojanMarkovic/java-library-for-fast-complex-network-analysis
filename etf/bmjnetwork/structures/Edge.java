package etf.bmjnetwork.structures;

import etf.bmjnetwork.exceptions.GraphNullException;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Generic helper class. It is thread-safe. Intended to be used by user to manipulate edges. Modifications done to
 * this class don't affect main graph object!
 *
 * @param <T> Node type of edge.
 */
public class Edge<T> implements Serializable {
    private static final String NO_ATTRIBUTES = "No attributes!";
    private static final int STRING_BUILDER_START_CAPACITY = 50;
    private static final long serialVersionUID = 4832958743200187660L;
    private final T nodeFrom;
    private final T nodeTo;
    private final Map<String, Object> attributes;


    /**
     * Constructor for edge class.
     *
     * @param nodeFrom   Object from which edge starts.
     * @param nodeTo     Object to which edge goes.
     * @param attributes Attributes associated with edge.
     * @throws GraphNullException null was passed to a constructor.
     */
    public Edge(T nodeFrom, T nodeTo, Map<String, Object> attributes) {
        if ((nodeFrom == null) || (nodeTo == null) || (attributes == null))
            throw new GraphNullException("Null was passed to Edge constructor!");
        this.nodeFrom = nodeFrom;
        this.nodeTo = nodeTo;
        this.attributes = new ConcurrentHashMap<>(attributes);
    }

    /**
     * The function returns object from which edge starts.
     *
     * @return Object from which edge starts.
     */
    public T getNodeFrom() {
        return nodeFrom;
    }

    /**
     * The function returns object to which edge goes.
     *
     * @return Object to which edge goes.
     */
    public T getNodeTo() {
        return nodeTo;
    }

    /**
     * The function returns attributes of edge.
     *
     * @return Map of attributes of edge. (attribute name, attribute value)
     */
    public Map<String, Object> getAttributes() {
        return Collections.unmodifiableMap(attributes);
    }

    /**
     * The function prints formatted edge.
     *
     * @return Edge in string format.
     */
    @Override
    public String toString() {
        return String.format("Edge'{' nodeFrom= %s, nodeTo= %s, attributes= %s'}'\n", nodeFrom, nodeTo, attributesToString());
    }

    /**
     * Helper function for generating string from edge.
     *
     * @return Attributes converted to string.
     */
    private String attributesToString() {
        if ((attributes == null) || (attributes.isEmpty())) return NO_ATTRIBUTES;
        StringBuilder stringBuilder = new StringBuilder(STRING_BUILDER_START_CAPACITY);
        attributes.forEach((name, value) -> stringBuilder.append(String.format("Name= %s, Value= %s", name, value)));
        return stringBuilder.toString();
    }
}
