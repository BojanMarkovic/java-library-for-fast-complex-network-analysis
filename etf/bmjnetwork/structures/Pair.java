package etf.bmjnetwork.structures;

/**
 * Generic helper class containing two objects of different type.
 *
 * @param <T> First object type.
 * @param <U> Second object type.
 */
public class Pair<T, U> {
    private final T object1;
    private final U object2;

    /**
     * The constructor.
     *
     * @param object1 First object to store.
     * @param object2 Second object to store.
     */
    public Pair(T object1, U object2) {
        this.object1 = object1;
        this.object2 = object2;
    }

    /**
     * Getter of first object.
     *
     * @return First object.
     */
    public T getObject1() {
        return object1;
    }

    /**
     * Getter of second object.
     *
     * @return Second object.
     */
    public U getObject2() {
        return object2;
    }
}
