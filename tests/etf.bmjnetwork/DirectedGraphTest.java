package tests.etf.bmjnetwork;

import etf.bmjnetwork.graph.*;
import etf.bmjnetwork.structures.Edge;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.*;

@SuppressWarnings("ALL")
public class DirectedGraphTest {

    private static void removeNode(Graph<Integer> g) {
        assertTrue(g.addNode(5));
        assertNotNull(g.removeNode(5));
        assertEquals(0L, g.getNodeCount());
        assertTrue(g.addEdge(5, 10));
        assertNotNull(g.removeNode(5));
        assertEquals(1L, g.getNodeCount());
        assertEquals(0L, g.getEdgesCount());
        assertNull(g.removeNode(100));
    }

    private static void removeEdge(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertNotNull(g.removeEdge(5, 10));
        assertEquals(0L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertNull(g.removeEdge(10, 5));
    }

    private static void addNode(Graph<Integer> g) {
        assertTrue(g.addNode(5));
        assertEquals(1L, g.getNodeCount());
        assertFalse(g.addNode(5));
    }

    private static void testAddNode(Graph<Integer> g) {
        assertTrue(g.addNode(5, "test", 5));
        assertEquals(1L, g.getNodeCount());
        assertEquals(5, g.getNode(5).getAttributes().get("test"));
        assertFalse(g.addNode(5, "test", 5));
        assertFalse(g.addNode(5));
    }

    private static void testAddNode1(Graph<Integer> g) {
        Map<String, Object> attributes = new ConcurrentHashMap<>();
        attributes.putIfAbsent("test", 5);
        attributes.putIfAbsent("test2", 52);
        assertTrue(g.addNode(5, attributes));
        assertEquals(1L, g.getNodeCount());
        assertEquals(5, g.getNode(5).getAttributes().get("test"));
        assertEquals(52, g.getNode(5).getAttributes().get("test2"));
        assertEquals(2L, g.getNode(5).getAttributes().size());
        assertFalse(g.addNode(5, attributes));
        assertFalse(g.addNode(5, "test", 5));
        assertFalse(g.addNode(5));
    }

    private static Collection<Integer> addNodesFrom(Graph<Integer> g) {
        Collection<Integer> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            list.add(i);
        }
        g.addNodesFrom(list);
        assertEquals(list.size(), g.getNodeCount());
        return list;
    }

    private static void addEdge(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertFalse(g.addEdge(5, 10));
    }

    private static void testAddEdge(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10, "test", 5));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertNull(g.getEdge(10, 5));
        assertFalse(g.addEdge(5, 10, "test", 5));
        assertFalse(g.addEdge(5, 10));
    }

    private static void testAddEdge1(Graph<Integer> g) {
        ConcurrentHashMap<String, Object> attributes = new ConcurrentHashMap<>();
        attributes.putIfAbsent("test", 5);
        attributes.putIfAbsent("test2", 52);
        assertTrue(g.addEdge(5, 10, attributes));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertEquals(52, g.getEdge(5, 10).getAttributes().get("test2"));
        assertEquals(2L, g.getEdge(5, 10).getAttributes().size());
        assertNull(g.getEdge(10, 5));
        assertFalse(g.addEdge(5, 10, "test", 5));
        assertFalse(g.addEdge(5, 10));
        assertFalse(g.addEdge(5, 10, attributes));
    }

    private static void addEdgesFrom(Graph<Integer> g) {
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            list1.add(i);
            list2.add(100 - i - 1);
        }
        g.addEdgesFrom(list1, list2);
        assertEquals(list1.size(), g.getNodeCount());
        assertEquals(list2.size(), g.getNodeCount());
        assertEquals(list2.size(), g.getEdgesCount());
    }

    private static void getEdgesCount(Graph<Integer> g) {
        assertTrue(g.addNode(5));
        assertEquals(0L, g.getEdgesCount());
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
    }

    private static void getAllNodes(Graph<Integer> g) {
        Collection<Integer> list = addNodesFrom(g);
        assertArrayEquals(list.toArray(), g.getAllNodes().toArray());
    }

    private static void getAllNodesInfo(Graph<Integer> g) {
        for (int i = 0; i < 100; i++) {
            assertTrue(g.addNode(i, "test", 5));
        }
        assertEquals(100L, g.getNodeCount());
        List<Node<Integer>> allNodesInfo = g.getAllNodesInfo();
        for (int i = 0; i < 100; i++) {
            assertEquals(new Integer(i), allNodesInfo.get(i).getMyNode());
            assertEquals(5, allNodesInfo.get(i).getAttributes().get("test"));
        }
    }

    private static void getNode(Graph<Integer> g) {
        assertTrue(g.addNode(5));
        assertEquals(1L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
        assertTrue(g.getNode(5).getAttributes().isEmpty());
        assertTrue(g.getNode(5).getNeighbours().isEmpty());
        assertNull(g.getNode(5).getNeighbourEdge(10));
    }

    private static void getEdge(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getEdge(5, 10).getNodeFrom());
        assertEquals(new Integer(10), g.getEdge(5, 10).getNodeTo());
        assertTrue(g.getEdge(5, 10).getAttributes().isEmpty());
    }

    private static void getAllEdgesInfo(Graph<Integer> g) {
        for (int i = 0; i < 100; i++) {
            assertTrue(g.addEdge(i, 100 - i - 1, "test", 5));
        }
        assertEquals(100L, g.getNodeCount());
        List<Edge<Integer>> allEdgesInfo = g.getAllEdgesInfo();
        for (int i = 0; i < 100; i++) {
            assertEquals(100L, allEdgesInfo.size());
            assertEquals(allEdgesInfo.get(i).getNodeFrom(), new Integer(100 - allEdgesInfo.get(i).getNodeTo() - 1));
            assertEquals(5, allEdgesInfo.get(i).getAttributes().get("test"));
        }
    }

    private static void hasNode(Graph<Integer> g) {
        addNodesFrom(g);
        assertTrue(g.hasNode(50));
        assertFalse(g.hasNode(-50));
    }

    private static void hasEdge(Graph<Integer> g) {
        addEdgesFrom(g);
        assertTrue(g.hasEdge(5, 94));
        assertFalse(g.hasEdge(0, 50));
    }

    private static void addAttributeToNode(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.addAttributeToNode(5, "bla", "qwe"));
        assertEquals("qwe", g.getNode(5).getAttributes().get("bla"));
    }

    private static void testAddAttributeToNode(Graph<Integer> g) {
        Map<String, Object> attributes = new ConcurrentHashMap<>();
        attributes.putIfAbsent("test", 5);
        attributes.putIfAbsent("test1", 52);
        assertTrue(g.addNode(5));
        g.addAttributeToNode(5, attributes);
        assertEquals(1L, g.getNodeCount());
        assertEquals(5, g.getNode(5).getAttributes().get("test"));
        assertEquals(52, g.getNode(5).getAttributes().get("test1"));
        assertEquals(2L, g.getNode(5).getAttributes().size());
    }

    private static void addAttributeToEdge(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.addAttributeToEdge(5, 10, "test", 5));
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertNull(g.getEdge(10, 5));
        assertFalse(g.addEdge(5, 10, "test", 5));
        assertFalse(g.addEdge(5, 10));
    }

    private static void testAddAttributeToEdge(Graph<Integer> g) {
        ConcurrentHashMap<String, Object> attributes = new ConcurrentHashMap<>();
        attributes.putIfAbsent("test", 5);
        attributes.putIfAbsent("test2", 52);
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        g.addAttributeToEdge(5, 10, attributes);
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertEquals(52, g.getEdge(5, 10).getAttributes().get("test2"));
        assertEquals(2L, g.getEdge(5, 10).getAttributes().size());
        assertNull(g.getEdge(10, 5));
        assertFalse(g.addEdge(5, 10, "test", 5));
        assertFalse(g.addEdge(5, 10));
        assertFalse(g.addEdge(5, 10, attributes));
    }

    private static void getAllNodeAttributes(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
        g.generateInDegreeAttributes();
        g.generateWeightedInDegreeAttributes("weight");
        assertEquals(2L, g.getNode(5).getAttributes().size());
        assertEquals(2L, g.getNode(10).getAttributes().size());
        assertEquals(2L, g.getAllNodeAttributes().size());
    }

    private static void getAllEdgeAttributes(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10, "test", 5));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertNull(g.getEdge(10, 5));
        assertEquals(1L, g.getAllEdgeAttributes().size());
    }

    private static void getAverageDegree(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10, "test", 5));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertNull(g.getEdge(10, 5));
        assertEquals(0.5, g.getAverageDegree(false), 0.0);
    }

    private static void generateOutDegreeAttributes(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
        g.getAverageDegree(true);
        assertEquals(1L, g.getNode(5).getAttributes().size());
        assertEquals(1L, g.getNode(10).getAttributes().size());
    }

    private static void generateInDegreeAttributes(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
        g.generateInDegreeAttributes();
        assertEquals(1L, g.getNode(5).getAttributes().size());
        assertEquals(1L, g.getNode(10).getAttributes().size());
    }

    private static void removeAllGeneratedAttributes(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
        g.generateInDegreeAttributes();
        g.generateWeightedInDegreeAttributes("weight");
        assertEquals(2L, g.getNode(5).getAttributes().size());
        assertEquals(2L, g.getNode(10).getAttributes().size());
        g.removeALlGeneratedAttributes();
        assertEquals(0L, g.getNode(5).getAttributes().size());
        assertEquals(0L, g.getNode(10).getAttributes().size());
    }

    private static void getAverageWeightedDegree(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10, "test", 5));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertNull(g.getEdge(10, 5));
        assertEquals(0.5, g.getAverageWeightedDegree("weight", false), 0.0);
        assertEquals(2.5, g.getAverageWeightedDegree("test", false), 0.0);
    }

    private static void generateWeightedOutDegreeAttributes(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
        g.getAverageWeightedDegree("weight", true);
        assertEquals(1L, g.getNode(5).getAttributes().size());
        assertEquals(1L, g.getNode(10).getAttributes().size());
    }

    private static void generateWeightedInDegreeAttributes(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
        g.generateWeightedInDegreeAttributes("weight");
        assertEquals(1L, g.getNode(5).getAttributes().size());
        assertEquals(1L, g.getNode(10).getAttributes().size());
    }

    private static void getDensity(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.addEdge(10, 5));
        assertEquals(1.0, g.getDensity(), 0.0);
    }

    private static void getLinksPerNode(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.addEdge(10, 5));
        assertEquals(1.0, g.getLinksPerNode(), 0.0);
    }

    private static void changeNameOfNodeAttribute(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.getNode(5).addAttribute("label", 45));
        g.changeNameOfNodeAttribute("label", "newLabel");
        assertEquals(45, g.getNode(5).getAttributes().get("newLabel"));
    }

    private static void changeNameOfEdgeAttribute(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.getNode(5).addAttributeToEdge(10, "label", 45));
        g.changeNameOfEdgeAttribute("label", "newLabel");
        assertEquals(45, g.getNode(5).getNeighbourEdge(10).getAttributes().get("newLabel"));
    }

    private static void testGraphMetics(Graph<String> graph, long parameter1, long parameter2) {
        assertEquals(581L, graph.getNodeCount());
        assertEquals(5330L, graph.getEdgesCount());
        assertEquals(917L, Math.round(graph.getAverageDegree(false) * 100.0));
        assertEquals(1225L, Math.round(graph.getAverageWeightedDegree("weight", false) * 100.0));
        assertEquals(parameter1, parameter2);
    }

    private static void getEccentricityMap(Graph<String> graph) {
        assertEquals(581L, graph.getNodeCount());
        assertEquals(5330L, graph.getEdgesCount());
        assertEquals(917L, Math.round(graph.getAverageDegree(false) * 100.0));
        assertEquals(1225L, Math.round(graph.getAverageWeightedDegree("weight", false) * 100.0));
        assertEquals(new Integer(5), graph.getEccentricityMap(false).get("103970"));
    }

    private static void getInNeighbours(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(1L, g.getInNeighbours(10).size());
        assertEquals(0L, g.getInNeighbours(5).size());
    }

    private static void checkIfAlgorithmRunsCorrectly(Graph<String> graph, long parameter, Map<String, Double> map) {
        assertEquals(581L, graph.getNodeCount());
        assertEquals(5330L, graph.getEdgesCount());
        assertEquals(917L, Math.round(graph.getAverageDegree(false) * 100.0));
        assertEquals(1225L, Math.round(graph.getAverageWeightedDegree("weight", false) * 100.0));
        assertEquals(parameter, Math.round(map.get("103970") * 10000.0));
    }

    private static void getBetweennessCentrality(Graph<String> graph) {
        assertEquals(581L, graph.getNodeCount());
        assertEquals(5330L, graph.getEdgesCount());
        assertEquals(917L, Math.round(graph.getAverageDegree(false) * 100.0));
        assertEquals(1225L, Math.round(graph.getAverageWeightedDegree("weight", false) * 100.0));
        assertEquals(1598L, Math.round(graph.getBetweennessCentrality(false).get("105657")));
    }

    private static void calculateLowestNumberOfHops(Graph<String> graph) {
        assertEquals(581L, graph.getNodeCount());
        assertEquals(5330L, graph.getEdgesCount());
        assertEquals(917L, Math.round(graph.getAverageDegree(false) * 100.0));
        assertEquals(2L, graph.calculateLowestNumberOfHops().get("105657").get("103970").longValue());
    }

    private static void calculateLowestNumberOfHopsPath(Graph<String> graph) {
        assertEquals(581L, graph.getNodeCount());
        assertEquals(5330L, graph.getEdgesCount());
        assertEquals(917L, Math.round(graph.getAverageDegree(false) * 100.0));
        Map<String, ConcurrentHashMap<String, ArrayList<ArrayList<String>>>> paths = graph.calculateLowestNumberOfHopsPath();
        Map<String, ConcurrentHashMap<String, Integer>> hops = graph.calculateLowestNumberOfHops();
        paths.forEach((key, nodes) -> nodes.forEach((keyNode, pathsNode) ->
                assertEquals(pathsNode.get(0).size() - 1, hops.get(key).get(keyNode).longValue())));
    }

    @Test
    public void removeNode() {
        removeNode(new DirectedGraphFast<>());
        removeNode(new DirectedGraph<>());
    }

    @Test
    public void removeEdge() {
        removeEdge(new DirectedGraphFast<>());
        removeEdge(new DirectedGraph<>());
    }

    @Test
    public void addNode() {
        addNode(new DirectedGraphFast<>());
        addNode(new DirectedGraph<>());
    }

    @Test
    public void testAddNode() {
        testAddNode(new DirectedGraphFast<>());
        testAddNode(new DirectedGraph<>());
    }

    @Test
    public void testAddNode1() {
        testAddNode1(new DirectedGraphFast<>());
        testAddNode1(new DirectedGraph<>());
    }

    @Test
    public void addNodesFrom() {
        addNodesFrom(new DirectedGraphFast<>());
        addNodesFrom(new DirectedGraph<>());
    }

    @Test
    public void addEdge() {
        addEdge(new DirectedGraphFast<>());
        addEdge(new DirectedGraph<>());
    }

    @Test
    public void testAddEdge() {
        testAddEdge(new DirectedGraphFast<>());
        testAddEdge(new DirectedGraph<>());
    }

    @Test
    public void testAddEdge1() {
        testAddEdge1(new DirectedGraphFast<>());
        testAddEdge1(new DirectedGraph<>());
    }

    @Test
    public void addEdgesFrom() {
        addEdgesFrom(new DirectedGraphFast<>());
        addEdgesFrom(new DirectedGraph<>());
    }

    @Test
    public void getNodeCount() {
        Graph<Integer> graph = new DirectedGraphFast<>();
        assertTrue(graph.addNode(5));
        assertEquals(1L, graph.getNodeCount());

        graph = new DirectedGraph<>();
        assertTrue(graph.addNode(5));
        assertEquals(1L, graph.getNodeCount());
    }

    @Test
    public void getEdgesCount() {
        getEdgesCount(new DirectedGraphFast<>());
        getEdgesCount(new DirectedGraph<>());
    }

    @Test
    public void getAllNodes() {
        getAllNodes(new DirectedGraphFast<>());
        getAllNodes(new DirectedGraph<>());
    }

    @Test
    public void getAllNodesInfo() {
        getAllNodesInfo(new DirectedGraphFast<>());
        getAllNodesInfo(new DirectedGraph<>());
    }

    @Test
    public void getNode() {
        getNode(new DirectedGraphFast<>());
        getNode(new DirectedGraph<>());
    }

    @Test
    public void getEdge() {
        getEdge(new DirectedGraphFast<>());
        getEdge(new DirectedGraph<>());
    }

    @Test
    public void getAllEdgesInfo() {
        getAllEdgesInfo(new DirectedGraphFast<>());
        getAllEdgesInfo(new DirectedGraph<>());
    }

    @Test
    public void hasNode() {
        hasNode(new DirectedGraphFast<>());
        hasNode(new DirectedGraph<>());
    }

    @Test
    public void hasEdge() {
        hasEdge(new DirectedGraphFast<>());
        hasEdge(new DirectedGraph<>());
    }

    @Test
    public void addAttributeToNode() {
        addAttributeToNode(new DirectedGraphFast<>());
        addAttributeToNode(new DirectedGraph<>());
    }

    @Test
    public void testAddAttributeToNode() {
        testAddAttributeToNode(new DirectedGraphFast<>());
        testAddAttributeToNode(new DirectedGraph<>());
    }

    @Test
    public void addAttributeToEdge() {
        addAttributeToEdge(new DirectedGraphFast<>());
        addAttributeToEdge(new DirectedGraph<>());
    }

    @Test
    public void testAddAttributeToEdge() {
        testAddAttributeToEdge(new DirectedGraphFast<>());
        testAddAttributeToEdge(new DirectedGraph<>());
    }

    @Test
    public void getAllNodeAttributes() {
        getAllNodeAttributes(new DirectedGraphFast<>());
        getAllNodeAttributes(new DirectedGraph<>());
    }

    @Test
    public void getAllEdgeAttributes() {
        getAllEdgeAttributes(new DirectedGraphFast<>());
        getAllEdgeAttributes(new DirectedGraph<>());
    }

    @Test
    public void getAverageDegree() {
        getAverageDegree(new DirectedGraphFast<>());
        getAverageDegree(new DirectedGraph<>());
    }

    @Test
    public void generateOutDegreeAttributes() {
        generateOutDegreeAttributes(new DirectedGraphFast<>());
        generateOutDegreeAttributes(new DirectedGraph<>());
    }

    @Test
    public void generateInDegreeAttributes() {
        generateInDegreeAttributes(new DirectedGraphFast<>());
        generateInDegreeAttributes(new DirectedGraph<>());
    }

    @Test
    public void removeAllGeneratedAttributes() {
        removeAllGeneratedAttributes(new DirectedGraphFast<>());
        removeAllGeneratedAttributes(new DirectedGraph<>());
    }

    @Test
    public void getAverageWeightedDegree() {
        getAverageWeightedDegree(new DirectedGraphFast<>());
        getAverageWeightedDegree(new DirectedGraph<>());
    }

    @Test
    public void generateWeightedOutDegreeAttributes() {
        generateWeightedOutDegreeAttributes(new DirectedGraphFast<>());
        generateWeightedOutDegreeAttributes(new DirectedGraph<>());
    }

    @Test
    public void generateWeightedInDegreeAttributes() {
        generateWeightedInDegreeAttributes(new DirectedGraphFast<>());
        generateWeightedInDegreeAttributes(new DirectedGraph<>());
    }

    @Test
    public void getDensity() {
        getDensity(new DirectedGraphFast<>());
        getDensity(new DirectedGraph<>());
    }

    @Test
    public void getLinksPerNode() {
        getLinksPerNode(new DirectedGraphFast<>());
        getLinksPerNode(new DirectedGraph<>());
    }

    @Test
    public void changeNameOfNodeAttribute() {
        changeNameOfNodeAttribute(new DirectedGraphFast<>());
        changeNameOfNodeAttribute(new DirectedGraph<>());
    }

    @Test
    public void changeNameOfEdgeAttribute() {
        changeNameOfEdgeAttribute(new DirectedGraphFast<>());
        changeNameOfEdgeAttribute(new DirectedGraph<>());
    }

    @Test
    public void testGraphMetics() {
        Graph<String> graph = IOGraph.readGMLFile("BasicALL.gml", true);
        testGraphMetics(graph, 9L, graph.getDiameter());
        graph = IOGraph.readGMLFile("BasicALL.gml", false);
        testGraphMetics(graph, 9L, graph.getDiameter());
    }

    @Test
    public void getAveragePathLength() {
        Graph<String> graph = IOGraph.readGMLFile("BasicALL.gml", true);
        testGraphMetics(graph, 269L, Math.round(graph.getAveragePathLength() * 100.0));
        graph = IOGraph.readGMLFile("BasicALL.gml", false);
        testGraphMetics(graph, 269L, Math.round(graph.getAveragePathLength() * 100.0));
    }

    @Test
    public void getRadius() {
        Graph<String> graph = IOGraph.readGMLFile("BasicALL.gml", true);
        testGraphMetics(graph, 0L, graph.getRadius());
        graph = IOGraph.readGMLFile("BasicALL.gml", false);
        testGraphMetics(graph, 0L, graph.getRadius());
    }

    @Test
    public void getEccentricityMap() {
        getEccentricityMap(IOGraph.readGMLFile("BasicALL.gml", true));
        getEccentricityMap(IOGraph.readGMLFile("BasicALL.gml", false));
    }

    @Test
    public void getInNeighbours() {
        getInNeighbours(new DirectedGraphFast<>());
        getInNeighbours(new DirectedGraph<>());
    }

    @Test
    public void getHubsMap() {
        Graph<String> graph = IOGraph.readGMLFile("BasicALL.gml", true);
        checkIfAlgorithmRunsCorrectly(graph, 172, graph.getHubsMap(false, 50, 1.0e-4));
        graph = IOGraph.readGMLFile("BasicALL.gml", false);
        checkIfAlgorithmRunsCorrectly(graph, 172, graph.getHubsMap(false, 50, 1.0e-4));
    }

    @Test
    public void getAuthorityMap() {
        Graph<String> graph = IOGraph.readGMLFile("BasicALL.gml", true);
        checkIfAlgorithmRunsCorrectly(graph, 594, graph.getAuthorityMap(false, 50, 1.0e-4));
        graph = IOGraph.readGMLFile("BasicALL.gml", false);
        checkIfAlgorithmRunsCorrectly(graph, 594, graph.getAuthorityMap(false, 50, 1.0e-4));
    }

    @Test
    public void getStronglyConnectedComponents() {
        Graph<String> graph = IOGraph.readGMLFile("BasicALL.gml", true);
        testGraphMetics(graph, 581L, graph.getStronglyConnectedComponents());
        graph = IOGraph.readGMLFile("BasicALL.gml", false);
        testGraphMetics(graph, 581L, graph.getStronglyConnectedComponents());
    }

    @Test
    public void getWeaklyConnectedComponents() {
        Graph<String> graph = IOGraph.readGMLFile("BasicALL.gml", true);
        testGraphMetics(graph, 18L, graph.getWeaklyConnectedComponents());
        graph = IOGraph.readGMLFile("BasicALL.gml", false);
        testGraphMetics(graph, 18L, graph.getWeaklyConnectedComponents());
    }

    @Test
    public void getEigenvectorCentrality() {
        Graph<String> graph = IOGraph.readGMLFile("BasicALL.gml", true);
        checkIfAlgorithmRunsCorrectly(graph, 177, graph.getEigenvectorCentrality(false, 10, null));
        graph = IOGraph.readGMLFile("BasicALL.gml", false);
        checkIfAlgorithmRunsCorrectly(graph, 177, graph.getEigenvectorCentrality(false, 10, null));
    }

    @Test
    public void getBetweennessCentrality() {
        getBetweennessCentrality(IOGraph.readGMLFile("BasicALL.gml", true));
        getBetweennessCentrality(IOGraph.readGMLFile("BasicALL.gml", false));
    }

    @Test
    public void getClosenessCentrality() {
        Graph<String> graph = IOGraph.readGMLFile("BasicALL.gml", true);
        checkIfAlgorithmRunsCorrectly(graph, 3855, graph.getClosenessCentrality(false));
        graph = IOGraph.readGMLFile("BasicALL.gml", false);
        checkIfAlgorithmRunsCorrectly(graph, 3855, graph.getClosenessCentrality(false));
    }

    @Test
    public void calculateLowestNumberOfHops() {
        calculateLowestNumberOfHops(IOGraph.readGMLFile("BasicALL.gml", true));
        calculateLowestNumberOfHops(IOGraph.readGMLFile("BasicALL.gml", false));
    }

    @Test
    public void calculateLowestNumberOfHopsPath() {
        calculateLowestNumberOfHopsPath(IOGraph.readGMLFile("BasicALL.gml", true));
        calculateLowestNumberOfHopsPath(IOGraph.readGMLFile("BasicALL.gml", false));
    }

    //Algorithm is not stable
    @Test
    public void getCommunityClassesSyncLabelPropagation() {
        assertTrue(true);
    }

    //Algorithm is not stable
    @Test
    public void getCommunityClassesAsyncLabelPropagation() {
        assertTrue(true);
    }
}