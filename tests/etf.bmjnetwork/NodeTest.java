package tests.etf.bmjnetwork;

import etf.bmjnetwork.graph.Graph;
import etf.bmjnetwork.graph.DirectedGraph;
import etf.bmjnetwork.graph.DirectedGraphFast;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.*;

@SuppressWarnings("ALL")
public class NodeTest {

    private static void checkIfHaveEdge(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.getNode(5).checkIfHaveEdge(10));
        assertFalse(g.getNode(5).checkIfHaveEdge(5));
    }

    private static void getNeighbour(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertNotNull(g.getNode(5).getNeighbourEdge(10));
        assertNull(g.getNode(5).getNeighbourEdge(100));
        assertEquals(new Integer(10), g.getNode(5).getNeighbourEdge(10).getNodeTo());
    }

    private static void removeEdge(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertNotNull(g.getNode(5).removeEdge(10));
        assertNull(g.getNode(5).removeEdge(10));
    }

    private static void addAttribute(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.getNode(5).addAttribute("bla", "qwe"));
        assertEquals("qwe", g.getNode(5).getAttributes().get("bla"));
    }

    private static void getNeighboursEdgeInfo(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10, "bla", 56));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(1L, g.getNode(5).getNeighboursEdgeInfo().size());
        assertNotNull(g.getNode(5).getNeighboursEdgeInfo().get(0));
        assertEquals(new Integer(10), g.getNode(5).getNeighboursEdgeInfo().get(0).getNodeTo());
        assertEquals(56, g.getNode(5).getNeighboursEdgeInfo().get(0).getAttributes().get("bla"));
    }

    private static void getNeighbours(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(1L, g.getNode(5).getNeighbours().size());
        assertEquals(new Integer(10), g.getNode(5).getNeighbours().get(0));
    }

    private static void getAttributes(Graph<Integer> g) {
        Map<String, Object> attributes = new ConcurrentHashMap<>();
        attributes.putIfAbsent("test", 5);
        attributes.putIfAbsent("test1", 52);
        assertTrue(g.addNode(5, attributes));
        assertEquals(1L, g.getNodeCount());
        assertEquals(5, g.getNode(5).getAttributes().get("test"));
        assertEquals(52, g.getNode(5).getAttributes().get("test1"));
        assertEquals(2L, g.getNode(5).getAttributes().size());
    }

    private static void setAttributes(Graph<Integer> g) {
        Map<String, Object> attributes = new ConcurrentHashMap<>();
        attributes.putIfAbsent("test", 5);
        attributes.putIfAbsent("test1", 52);
        assertTrue(g.addNode(5));
        g.getNode(5).setAttributes(attributes);
        assertEquals(1L, g.getNodeCount());
        assertEquals(5, g.getNode(5).getAttributes().get("test"));
        assertEquals(52, g.getNode(5).getAttributes().get("test1"));
        assertEquals(2L, g.getNode(5).getAttributes().size());
    }

    private static void getNode(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
    }

    private static void getNeighbourEdge(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertNotNull(g.getNode(5).getNeighbourEdge(10));
        assertEquals(new Integer(10), g.getNode(5).getNeighbourEdge(10).getNodeTo());
        assertNull(g.getNode(10).getNeighbourEdge(5));
    }

    private static void addAttributeToEdge(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.addAttributeToEdge(5, 10, "test", 5));
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertNull(g.getEdge(10, 5));
        assertFalse(g.addEdge(5, 10, "test", 5));
        assertFalse(g.addEdge(5, 10));
    }

    private static void testAddAttributeToEdge(Graph<Integer> g) {
        ConcurrentHashMap<String, Object> attributes = addingNeighbourAttributes(g);
        assertFalse(g.addEdge(5, 10, "test", 5));
        assertFalse(g.addEdge(5, 10));
        assertFalse(g.addEdge(5, 10, attributes));
    }

    private static void setAttributesToEdge(Graph<Integer> g) {
        ConcurrentHashMap<String, Object> attributes = new ConcurrentHashMap<>();
        attributes.put("test", 5);
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertFalse(g.getNode(5).setAttributesToEdge(10, attributes));
        assertEquals(1L, g.getNode(5).getNeighbourEdge(10).getAttributes().size());
    }

    private static void getNeighboursAttributes(Graph<Integer> g) {
        addingNeighbourAttributes(g);
        List<ConcurrentHashMap<String, Object>> list = new ArrayList<>(g.getNode(5).getNeighboursAttributes());
        assertEquals(2L, list.get(0).size());
    }

    private static ConcurrentHashMap<String, Object> addingNeighbourAttributes(Graph<Integer> g) {
        ConcurrentHashMap<String, Object> attributes = new ConcurrentHashMap<>();
        attributes.putIfAbsent("test", 5);
        attributes.putIfAbsent("test2", 52);
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        g.addAttributeToEdge(5, 10, attributes);
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertEquals(52, g.getEdge(5, 10).getAttributes().get("test2"));
        assertEquals(2L, g.getEdge(5, 10).getAttributes().size());
        assertNull(g.getEdge(10, 5));
        assertEquals(1L, g.getNode(5).getNeighboursAttributes().size());
        return attributes;
    }

    private static void getNeighboursInternal(Graph<Integer> g) {
        addingNeighbourAttributes(g);
        assertEquals(1L, g.getNode(5).getNeighboursInternal().size());
        assertEquals(2L, g.getNode(5).getNeighboursInternal().get(10).size());
    }

    private static void removeGeneratedAttributes(Graph<Integer> g) {
        getNode(g);
        g.generateInDegreeAttributes();
        g.generateWeightedInDegreeAttributes("weight");
        assertEquals(2L, g.getNode(5).getAttributes().size());
        assertEquals(2L, g.getNode(10).getAttributes().size());
        g.getNode(5).removeGeneratedAttributes();
        assertEquals(0L, g.getNode(5).getAttributes().size());
        assertNotEquals(0L, g.getNode(10).getAttributes().size());
    }

    private static void changeNameOfNodeAttribute(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.getNode(5).addAttribute("label", 45));
        g.getNode(5).changeNameOfNodeAttribute("label", "newLabel");
        assertEquals(45, g.getNode(5).getAttributes().get("newLabel"));
    }

    private static void changeNameOfEdgeAttribute(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.getNode(5).addAttributeToEdge(10, "label", 45));
        g.getNode(5).changeNameOfEdgeAttribute("label", "newLabel");
        assertEquals(45, g.getNode(5).getNeighbourEdge(10).getAttributes().get("newLabel"));
    }

    private static void removeEdgeAttribute(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertTrue(g.addEdge(5, 5));
        assertEquals(2L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.addAttributeToEdge(5, 10, "test", 5));
        assertTrue(g.addAttributeToEdge(5, 5, "test", 6));
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertEquals(6, g.getEdge(5, 5).getAttributes().get("test"));
        assertFalse(g.addEdge(5, 10, "test", 5));
        assertFalse(g.addEdge(5, 10));
        g.getNode(5).removeEdgeAttribute("test");
        assertNull(g.getNode(5).getEdgeAttribute(5, "test"));
    }

    @Test
    public void checkIfHaveEdge() {
        checkIfHaveEdge(new DirectedGraphFast<>());
        checkIfHaveEdge(new DirectedGraph<>());
    }

    @Test
    public void getNeighbour() {
        getNeighbour(new DirectedGraphFast<>());
        getNeighbour(new DirectedGraph<>());
    }

    @Test
    public void removeEdge() {
        removeEdge(new DirectedGraphFast<>());
        removeEdge(new DirectedGraph<>());
    }

    @Test
    public void addAttribute() {
        addAttribute(new DirectedGraphFast<>());
        addAttribute(new DirectedGraph<>());
    }

    @Test
    public void getNeighboursEdgeInfo() {
        getNeighboursEdgeInfo(new DirectedGraphFast<>());
        getNeighboursEdgeInfo(new DirectedGraph<>());
    }

    @Test
    public void getNeighbours() {
        getNeighbours(new DirectedGraphFast<>());
        getNeighbours(new DirectedGraph<>());
    }

    @Test
    public void getAttributes() {
        getAttributes(new DirectedGraphFast<>());
        getAttributes(new DirectedGraph<>());
    }

    @Test
    public void setAttributes() {
        setAttributes(new DirectedGraphFast<>());
        setAttributes(new DirectedGraph<>());
    }

    @Test
    public void getNode() {
        getNode(new DirectedGraphFast<>());
        getNode(new DirectedGraph<>());
    }

    @Test
    public void getNeighbourEdge() {
        getNeighbourEdge(new DirectedGraphFast<>());
        getNeighbourEdge(new DirectedGraph<>());
    }

    @Test
    public void addAttributeToEdge() {
        addAttributeToEdge(new DirectedGraphFast<>());
        addAttributeToEdge(new DirectedGraph<>());
    }

    @Test
    public void testAddAttributeToEdge() {
        testAddAttributeToEdge(new DirectedGraphFast<>());
        testAddAttributeToEdge(new DirectedGraph<>());
    }

    @Test
    public void setAttributesToEdge() {
        setAttributesToEdge(new DirectedGraphFast<>());
        setAttributesToEdge(new DirectedGraph<>());
    }

    @Test
    public void getNeighboursAttributes() {
        getNeighboursAttributes(new DirectedGraphFast<>());
        getNeighboursAttributes(new DirectedGraph<>());
    }

    @Test
    public void getNeighboursInternal() {
        getNeighboursInternal(new DirectedGraphFast<>());
        getNeighboursInternal(new DirectedGraph<>());
    }

    @Test
    public void getMyNode() {
        Graph<Integer> graph = new DirectedGraphFast<>();
        getNode(graph);
        assertNotEquals(new Integer(10), graph.getNode(5).getMyNode());

        graph = new DirectedGraph<>();
        getNode(graph);
        assertNotEquals(new Integer(10), graph.getNode(5).getMyNode());
    }

    @Test
    public void removeGeneratedAttributes() {
        removeGeneratedAttributes(new DirectedGraphFast<>());
        removeGeneratedAttributes(new DirectedGraph<>());
    }

    @Test
    public void changeNameOfNodeAttribute() {
        changeNameOfNodeAttribute(new DirectedGraphFast<>());
        changeNameOfNodeAttribute(new DirectedGraph<>());
    }

    @Test
    public void changeNameOfEdgeAttribute() {
        changeNameOfEdgeAttribute(new DirectedGraphFast<>());
        changeNameOfEdgeAttribute(new DirectedGraph<>());
    }

    @Test
    public void getNodeAttribute() {
        Graph<Integer> graph = new DirectedGraphFast<>();
        addAttribute(graph);
        assertEquals("qwe", graph.getNode(5).getNodeAttribute("bla"));

        graph = new DirectedGraph<>();
        addAttribute(graph);
        assertEquals("qwe", graph.getNode(5).getNodeAttribute("bla"));
    }

    @Test
    public void getEdgeAttribute() {
        Graph<Integer> graph = new DirectedGraphFast<>();
        addAttributeToEdge(graph);
        assertEquals(5, graph.getNode(5).getEdgeAttribute(10, "test"));

        graph = new DirectedGraph<>();
        addAttributeToEdge(graph);
        assertEquals(5, graph.getNode(5).getEdgeAttribute(10, "test"));
    }

    @Test
    public void removeNodeAttribute() {
        Graph<Integer> graph = new DirectedGraphFast<>();
        addAttribute(graph);
        assertEquals("qwe", graph.getNode(5).removeNodeAttribute("bla"));

        graph = new DirectedGraph<>();
        addAttribute(graph);
        assertEquals("qwe", graph.getNode(5).removeNodeAttribute("bla"));
    }

    @Test
    public void removeEdgeAttribute() {
        removeEdgeAttribute(new DirectedGraphFast<>());
        removeEdgeAttribute(new DirectedGraph<>());
    }

    @Test
    public void removeEdgeAttributeTest() {
        Graph<Integer> graph = new DirectedGraphFast<>();
        addAttributeToEdge(graph);
        assertEquals(5, graph.getNode(5).removeEdgeAttribute(10, "test"));

        graph = new DirectedGraph<>();
        addAttributeToEdge(graph);
        assertEquals(5, graph.getNode(5).removeEdgeAttribute(10, "test"));
    }
}