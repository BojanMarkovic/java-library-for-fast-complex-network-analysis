package tests.etf.bmjnetwork;

import etf.bmjnetwork.graph.*;
import etf.bmjnetwork.structures.Edge;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.*;

@SuppressWarnings("ALL")
public class UndirectedGraphTest {

    private static void testAddNode1(Graph<Integer> g) {
        Map<String, Object> attributes = new ConcurrentHashMap<>();
        attributes.putIfAbsent("test", 5);
        attributes.putIfAbsent("test2", 52);
        assertTrue(g.addNode(5, attributes));
        assertEquals(1L, g.getNodeCount());
        assertEquals(5, g.getNode(5).getAttributes().get("test"));
        assertEquals(52, g.getNode(5).getAttributes().get("test2"));
        assertEquals(2L, g.getNode(5).getAttributes().size());
        assertFalse(g.addNode(5, attributes));
        assertFalse(g.addNode(5, "test", 5));
        assertFalse(g.addNode(5));
    }

    private static Collection<Integer> addNodesFrom(Graph<Integer> g) {
        Collection<Integer> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            list.add(i);
        }
        g.addNodesFrom(list);
        assertEquals(list.size(), g.getNodeCount());
        return list;
    }

    private static void addAttributeToNode(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.addAttributeToNode(5, "bla", "qwe"));
        assertEquals("qwe", g.getNode(5).getAttributes().get("bla"));
    }

    private static void testAddAttributeToNode(Graph<Integer> g) {
        Map<String, Object> attributes = new ConcurrentHashMap<>();
        attributes.putIfAbsent("test", 5);
        attributes.putIfAbsent("test1", 52);
        assertTrue(g.addNode(5));
        g.addAttributeToNode(5, attributes);
        assertEquals(1L, g.getNodeCount());
        assertEquals(5, g.getNode(5).getAttributes().get("test"));
        assertEquals(52, g.getNode(5).getAttributes().get("test1"));
        assertEquals(2L, g.getNode(5).getAttributes().size());
    }

    private static void getEdgesCount(Graph<Integer> g) {
        assertTrue(g.addNode(5));
        assertEquals(0L, g.getEdgesCount());
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
    }

    private static void getAllNodes(Graph<Integer> g) {
        Collection<Integer> list = addNodesFrom(g);
        assertArrayEquals(list.toArray(), g.getAllNodes().toArray());
    }

    private static void getAllNodesInfo(Graph<Integer> g) {
        for (int i = 0; i < 100; i++) {
            assertTrue(g.addNode(i, "test", 5));
        }
        assertEquals(100L, g.getNodeCount());
        List<Node<Integer>> allNodesInfo = g.getAllNodesInfo();
        for (int i = 0; i < 100; i++) {
            assertEquals(new Integer(i), allNodesInfo.get(i).getMyNode());
            assertEquals(5, allNodesInfo.get(i).getAttributes().get("test"));
        }
    }

    private static void getNode(Graph<Integer> g) {
        assertTrue(g.addNode(5));
        assertEquals(1L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
        assertTrue(g.getNode(5).getAttributes().isEmpty());
        assertTrue(g.getNode(5).getNeighbours().isEmpty());
        assertNull(g.getNode(5).getNeighbourEdge(10));
    }

    private static void getEdge(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getEdge(5, 10).getNodeFrom());
        assertEquals(new Integer(10), g.getEdge(5, 10).getNodeTo());
        assertTrue(g.getEdge(5, 10).getAttributes().isEmpty());
    }

    private static void getAllEdgesInfo(Graph<Integer> g) {
        for (int i = 0; i < 50; i++) {
            assertTrue(g.addEdge(i, 100 - i - 1, "test", 5));
        }
        assertEquals(100L, g.getNodeCount());
        List<Edge<Integer>> allEdgesInfo = g.getAllEdgesInfo();
        for (int i = 0; i < 50; i++) {
            assertEquals(50L, allEdgesInfo.size());
            assertEquals(allEdgesInfo.get(i).getNodeFrom(), new Integer(100 - allEdgesInfo.get(i).getNodeTo() - 1));
            assertEquals(5, allEdgesInfo.get(i).getAttributes().get("test"));
        }
    }

    private static void hasNode(Graph<Integer> g) {
        addNodesFrom(g);
        assertTrue(g.hasNode(50));
        assertFalse(g.hasNode(-50));
    }

    private static void hasEdge(Graph<Integer> g) {
        addEdgesFrom(g);
        assertTrue(g.hasEdge(5, 94));
        assertFalse(g.hasEdge(0, 50));
    }

    private static void addEdgesFrom(Graph<Integer> g) {
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            list1.add(i);
            list2.add(100 - i - 1);
        }
        g.addEdgesFrom(list1, list2);
        assertEquals(100L, g.getNodeCount());
        assertEquals(100L, g.getNodeCount());
        assertEquals(50L, g.getEdgesCount());
    }

    private static void getAllNodeAttributes(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
        g.generateInDegreeAttributes();
        g.generateWeightedInDegreeAttributes("weight");
        assertEquals(2L, g.getNode(5).getAttributes().size());
        assertEquals(2L, g.getNode(10).getAttributes().size());
        assertEquals(2L, g.getAllNodeAttributes().size());
    }

    private static void getAllEdgeAttributes(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10, "test", 5));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertEquals(5, g.getEdge(10, 5).getAttributes().get("test"));
        assertEquals(1L, g.getAllEdgeAttributes().size());
    }

    private static void getAverageDegree(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10, "test", 5));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertEquals(5, g.getEdge(10, 5).getAttributes().get("test"));
        assertEquals(1.0, g.getAverageDegree(false), 0.0);
    }

    private static void removeAllGeneratedAttributes(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
        g.generateInDegreeAttributes();
        g.generateWeightedInDegreeAttributes("weight");
        assertEquals(2L, g.getNode(5).getAttributes().size());
        assertEquals(2L, g.getNode(10).getAttributes().size());
        g.removeALlGeneratedAttributes();
        assertEquals(0L, g.getNode(5).getAttributes().size());
        assertEquals(0L, g.getNode(10).getAttributes().size());
    }

    private static void getAverageWeightedDegree(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10, "test", 5));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertEquals(5, g.getEdge(10, 5).getAttributes().get("test"));
        assertEquals(1.0, g.getAverageWeightedDegree("weight", false), 0.0);
        assertEquals(5.0, g.getAverageWeightedDegree("test", false), 0.0);
    }

    private static void getDensity(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(1.0, g.getDensity(), 0.0);
    }

    private static void getLinksPerNode(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(0.5, g.getLinksPerNode(), 0.0);
    }

    private static void changeNameOfNodeAttribute(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.getNode(5).addAttribute("label", 45));
        g.changeNameOfNodeAttribute("label", "newLabel");
        assertEquals(45, g.getNode(5).getAttributes().get("newLabel"));
    }

    private static void changeNameOfEdgeAttribute(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.getNode(5).addAttributeToEdge(10, "label", 45));
        g.changeNameOfEdgeAttribute("label", "newLabel");
        assertEquals(45, g.getNode(5).getNeighbourEdge(10).getAttributes().get("newLabel"));
    }

    private static void getInNeighbours(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(1L, g.getInNeighbours(10).size());
        assertEquals(1L, g.getInNeighbours(5).size());
    }

    private static void generateOutDegreeAttributes(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
        g.getAverageDegree(true);
        assertEquals(1L, g.getNode(5).getAttributes().size());
        assertEquals(1L, g.getNode(10).getAttributes().size());
    }

    private static void generateWeightedOutDegreeAttributes(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
        g.getAverageWeightedDegree("weight", true);
        assertEquals(1L, g.getNode(5).getAttributes().size());
        assertEquals(1L, g.getNode(10).getAttributes().size());
    }

    private static void calculateLowestNumberOfHops(Graph<String> graph) {
        assertEquals(419L, graph.getNodeCount());
        assertEquals(2489L, graph.getEdgesCount());
        assertEquals(1188L, Math.round(graph.getAverageDegree(false) * 100.0));
        assertEquals(2L, graph.calculateLowestNumberOfHops().get("105657").get("103970").longValue());
    }

    private static void testGraphMetics(Graph<String> graph, long parameter1, long parameter2) {
        assertEquals(419L, graph.getNodeCount());
        assertEquals(2489L, graph.getEdgesCount());
        assertEquals(1188L, Math.round(graph.getAverageDegree(false) * 100.0));
        assertEquals(1379L, Math.round(graph.getAverageWeightedDegree("weight", false) * 100.0));
        assertEquals(parameter1, parameter2);
    }

    private static void getEccentricityMap(Graph<String> graph) {
        assertEquals(419L, graph.getNodeCount());
        assertEquals(2489L, graph.getEdgesCount());
        assertEquals(1188L, Math.round(graph.getAverageDegree(false) * 100.0));
        assertEquals(1379L, Math.round(graph.getAverageWeightedDegree("weight", false) * 100.0));
        assertEquals(new Integer(11), graph.getEccentricityMap(false).get("200619"));
    }

    private static void checkIfAlgorithmRunsCorrectly(Graph<String> graph, long parameter, Map<String, Double> map,
                                                      double accuracy) {
        assertEquals(419L, graph.getNodeCount());
        assertEquals(2489L, graph.getEdgesCount());
        assertEquals(1188L, Math.round(graph.getAverageDegree(false) * 100.0));
        assertEquals(1379L, Math.round(graph.getAverageWeightedDegree("weight", false) * 100.0));
        assertEquals(parameter, Math.round(map.get("103970") * accuracy));
    }

    private static void getBetweennessCentrality(Graph<String> graph) {
        assertEquals(419L, graph.getNodeCount());
        assertEquals(2489L, graph.getEdgesCount());
        assertEquals(1188L, Math.round(graph.getAverageDegree(false) * 100.0));
        assertEquals(1379L, Math.round(graph.getAverageWeightedDegree("weight", false) * 100.0));
        assertEquals(2704L, Math.round(graph.getBetweennessCentrality(false).get("105657")));
    }

    private static void removeEdge(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertNotNull(g.removeEdge(5, 10));
        assertEquals(0L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertNull(g.removeEdge(10, 5));
    }

    private static void addEdge(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertFalse(g.addEdge(10, 5));
    }

    private static void testAddEdge(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10, "test", 5));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertNotNull(g.getEdge(10, 5));
        assertFalse(g.addEdge(5, 10, "test", 5));
        assertFalse(g.addEdge(5, 10));
    }

    private static void testAddEdge1(Graph<Integer> g) {
        ConcurrentHashMap<String, Object> attributes = new ConcurrentHashMap<>();
        attributes.putIfAbsent("test", 5);
        attributes.putIfAbsent("test2", 52);
        assertTrue(g.addEdge(5, 10, attributes));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertEquals(52, g.getEdge(5, 10).getAttributes().get("test2"));
        assertEquals(2L, g.getEdge(5, 10).getAttributes().size());
        assertNotNull(g.getEdge(10, 5));
        assertFalse(g.addEdge(5, 10, "test", 5));
        assertFalse(g.addEdge(5, 10));
        assertFalse(g.addEdge(5, 10, attributes));
    }

    private static void addAttributeToEdge(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertTrue(g.addAttributeToEdge(5, 10, "test", 5));
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertNotNull(g.getEdge(10, 5));
        assertFalse(g.addEdge(5, 10, "test", 5));
        assertFalse(g.addEdge(5, 10));
    }

    private static void testAddAttributeToEdge(Graph<Integer> g) {
        ConcurrentHashMap<String, Object> attributes = new ConcurrentHashMap<>();
        attributes.putIfAbsent("test", 5);
        attributes.putIfAbsent("test2", 52);
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        g.addAttributeToEdge(5, 10, attributes);
        assertEquals(5, g.getEdge(5, 10).getAttributes().get("test"));
        assertEquals(52, g.getEdge(5, 10).getAttributes().get("test2"));
        assertEquals(2L, g.getEdge(5, 10).getAttributes().size());
        assertNotNull(g.getEdge(10, 5));
        assertFalse(g.addEdge(5, 10, "test", 5));
        assertFalse(g.addEdge(5, 10));
        assertFalse(g.addEdge(5, 10, attributes));
    }

    private static void generateInDegreeAttributes(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
        g.generateInDegreeAttributes();
        assertEquals(1L, g.getNode(5).getAttributes().size());
        assertEquals(1L, g.getNode(10).getAttributes().size());
    }

    private static void generateWeightedInDegreeAttributes(Graph<Integer> g) {
        assertTrue(g.addEdge(5, 10));
        assertEquals(1L, g.getEdgesCount());
        assertEquals(2L, g.getNodeCount());
        assertEquals(new Integer(5), g.getNode(5).getMyNode());
        g.generateWeightedInDegreeAttributes("weight");
        assertEquals(1L, g.getNode(5).getAttributes().size());
        assertEquals(1L, g.getNode(10).getAttributes().size());
    }

    private static void calculateLowestNumberOfHopsPath(Graph<String> graph) {
        assertEquals(419L, graph.getNodeCount());
        assertEquals(2489L, graph.getEdgesCount());
        assertEquals(1188L, Math.round(graph.getAverageDegree(false) * 100.0));
        Map<String, ConcurrentHashMap<String, ArrayList<ArrayList<String>>>> paths = graph.calculateLowestNumberOfHopsPath();
        Map<String, ConcurrentHashMap<String, Integer>> hops = graph.calculateLowestNumberOfHops();
        paths.forEach((key, nodes) -> nodes.forEach((keyNode, pathsNode) ->
                assertEquals(pathsNode.get(0).size() - 1, hops.get(key).get(keyNode).longValue())));
    }

    @Test
    public void removeNode() {
        removeNode(new UndirectedGraphFast<>());
        removeNode(new UndirectedGraph<>());
    }

    private void removeNode(Graph<Integer> g) {
        assertTrue(g.addNode(5));
        assertEquals(1L, g.getNodeCount());
        assertEquals(new Integer(5), g.removeNode(5).getMyNode());
        assertEquals(0L, g.getNodeCount());
        assertNull(g.removeNode(5));
    }

    @Test
    public void addNode() {
        Graph<Integer> g = new UndirectedGraphFast<>();
        assertTrue(g.addNode(5));
        assertEquals(1L, g.getNodeCount());
        g = new UndirectedGraph<>();
        assertTrue(g.addNode(5));
        assertEquals(1L, g.getNodeCount());
    }

    @Test
    public void testAddNode() {
        testAddNode(new UndirectedGraphFast<>());
        testAddNode(new UndirectedGraph<>());
    }

    private void testAddNode(Graph<Integer> g) {
        assertTrue(g.addNode(5, "test", 5));
        assertEquals(5, g.getNode(5).getNodeAttribute("test"));
        assertEquals(1L, g.getNodeCount());
    }

    @Test
    public void testAddNode1() {
        testAddNode1(new UndirectedGraphFast<>());
        testAddNode1(new UndirectedGraph<>());
    }

    @Test
    public void addNodesFrom() {
        addNodesFrom(new UndirectedGraphFast<>());
        addNodesFrom(new UndirectedGraph<>());
    }

    @Test
    public void addAttributeToNode() {
        addAttributeToNode(new UndirectedGraphFast<>());
        addAttributeToNode(new UndirectedGraph<>());
    }

    @Test
    public void testAddAttributeToNode() {
        testAddAttributeToNode(new UndirectedGraphFast<>());
        testAddAttributeToNode(new UndirectedGraph<>());
    }

    @Test
    public void getNodeCount() {
        Graph<Integer> graph = new UndirectedGraphFast<>();
        assertTrue(graph.addNode(5));
        assertEquals(1L, graph.getNodeCount());

        graph = new UndirectedGraph<>();
        assertTrue(graph.addNode(5));
        assertEquals(1L, graph.getNodeCount());
    }

    @Test
    public void getEdgesCount() {
        getEdgesCount(new UndirectedGraphFast<>());
        getEdgesCount(new UndirectedGraph<>());
    }

    @Test
    public void getAllNodes() {
        getAllNodes(new UndirectedGraphFast<>());
        getAllNodes(new UndirectedGraph<>());
    }

    @Test
    public void getAllNodesInfo() {
        getAllNodesInfo(new UndirectedGraphFast<>());
        getAllNodesInfo(new UndirectedGraph<>());
    }

    @Test
    public void getNode() {
        getNode(new UndirectedGraphFast<>());
        getNode(new UndirectedGraph<>());
    }

    @Test
    public void getEdge() {
        getEdge(new UndirectedGraphFast<>());
        getEdge(new UndirectedGraph<>());
    }

    @Test
    public void getAllEdgesInfo() {
        getAllEdgesInfo(new UndirectedGraphFast<>());
        getAllEdgesInfo(new UndirectedGraph<>());
    }

    @Test
    public void hasNode() {
        hasNode(new UndirectedGraphFast<>());
        hasNode(new UndirectedGraph<>());
    }

    @Test
    public void hasEdge() {
        hasEdge(new UndirectedGraphFast<>());
        hasEdge(new UndirectedGraph<>());
    }

    @Test
    public void getAllNodeAttributes() {
        getAllNodeAttributes(new UndirectedGraphFast<>());
        getAllNodeAttributes(new UndirectedGraph<>());
    }

    @Test
    public void getAllEdgeAttributes() {
        getAllEdgeAttributes(new UndirectedGraphFast<>());
        getAllEdgeAttributes(new UndirectedGraph<>());
    }

    @Test
    public void getAverageDegree() {
        getAverageDegree(new UndirectedGraphFast<>());
        getAverageDegree(new UndirectedGraph<>());
    }

    @Test
    public void generateOutDegreeAttributes() {
        generateOutDegreeAttributes(new UndirectedGraphFast<>());
        generateOutDegreeAttributes(new UndirectedGraph<>());
    }

    @Test
    public void generateWeightedOutDegreeAttributes() {
        generateWeightedOutDegreeAttributes(new UndirectedGraphFast<>());
        generateWeightedOutDegreeAttributes(new UndirectedGraph<>());
    }

    @Test
    public void removeALlGeneratedAttributes() {
        removeAllGeneratedAttributes(new UndirectedGraphFast<>());
        removeAllGeneratedAttributes(new UndirectedGraph<>());
    }

    @Test
    public void getAverageWeightedDegree() {
        getAverageWeightedDegree(new UndirectedGraphFast<>());
        getAverageWeightedDegree(new UndirectedGraph<>());
    }

    @Test
    public void getDensity() {
        getDensity(new UndirectedGraphFast<>());
        getDensity(new UndirectedGraph<>());
    }

    @Test
    public void getLinksPerNode() {
        getLinksPerNode(new UndirectedGraphFast<>());
        getLinksPerNode(new UndirectedGraph<>());
    }

    @Test
    public void changeNameOfNodeAttribute() {
        changeNameOfNodeAttribute(new UndirectedGraphFast<>());
        changeNameOfNodeAttribute(new UndirectedGraph<>());
    }

    @Test
    public void changeNameOfEdgeAttribute() {
        changeNameOfEdgeAttribute(new UndirectedGraphFast<>());
        changeNameOfEdgeAttribute(new UndirectedGraph<>());
    }

    @Test
    public void calculateLowestNumberOfHops() {
        calculateLowestNumberOfHops(IOGraph.readGEXFFile("qwe.gexf", true));
        calculateLowestNumberOfHops(IOGraph.readGEXFFile("qwe.gexf", false));
    }

    @Test
    public void getDiameter() {
        Graph<String> graph = IOGraph.readGEXFFile("qwe.gexf", true);
        testGraphMetics(graph, 11L, graph.getDiameter());
        graph = IOGraph.readGEXFFile("qwe.gexf", false);
        testGraphMetics(graph, 11L, graph.getDiameter());
    }

    @Test
    public void getAveragePathLength() {
        Graph<String> graph = IOGraph.readGEXFFile("qwe.gexf", true);
        testGraphMetics(graph, 313L, Math.round(graph.getAveragePathLength() * 100.0));
        graph = IOGraph.readGEXFFile("qwe.gexf", false);
        testGraphMetics(graph, 313L, Math.round(graph.getAveragePathLength() * 100.0));
    }

    @Test
    public void getRadius() {
        Graph<String> graph = IOGraph.readGEXFFile("qwe.gexf", true);
        testGraphMetics(graph, 1L, graph.getRadius());
        graph = IOGraph.readGEXFFile("qwe.gexf", false);
        testGraphMetics(graph, 1L, graph.getRadius());
    }

    @Test
    public void getEccentricityMap() {
        getEccentricityMap(IOGraph.readGEXFFile("qwe.gexf", true));
        getEccentricityMap(IOGraph.readGEXFFile("qwe.gexf", false));
    }

    @Test
    public void getHubsMap() {
        Graph<String> graph = IOGraph.readGEXFFile("qwe.gexf", true);
        checkIfAlgorithmRunsCorrectly(graph, 50, graph.getHubsMap(false, 100, 1.0e-4), 1000.0);
        graph = IOGraph.readGEXFFile("qwe.gexf", false);
        checkIfAlgorithmRunsCorrectly(graph, 50, graph.getHubsMap(false, 100, 1.0e-4), 1000.0);
    }

    @Test
    public void getAuthorityMap() {
        Graph<String> graph = IOGraph.readGEXFFile("qwe.gexf", true);
        checkIfAlgorithmRunsCorrectly(graph, 503, graph.getAuthorityMap(false, 100, 1.0e-4), 10000.0);
        graph = IOGraph.readGEXFFile("qwe.gexf", false);
        checkIfAlgorithmRunsCorrectly(graph, 503, graph.getAuthorityMap(false, 100, 1.0e-4), 10000.0);
    }

    @Test
    public void getWeaklyConnectedComponents() {
        Graph<String> graph = IOGraph.readGEXFFile("qwe.gexf", true);
        testGraphMetics(graph, 18L, graph.getWeaklyConnectedComponents());
        graph = IOGraph.readGEXFFile("qwe.gexf", false);
        testGraphMetics(graph, 18L, graph.getWeaklyConnectedComponents());
    }

    @Test
    public void getEigenvectorCentrality() {
        Graph<String> graph = IOGraph.readGEXFFile("qwe.gexf", true);
        checkIfAlgorithmRunsCorrectly(graph, 3422, graph.getEigenvectorCentrality(false, 9, null), 10000.0);
        graph = IOGraph.readGEXFFile("qwe.gexf", false);
        checkIfAlgorithmRunsCorrectly(graph, 3422, graph.getEigenvectorCentrality(false, 9, null), 10000.0);
    }

    @Test
    public void getBetweennessCentrality() {
        getBetweennessCentrality(IOGraph.readGEXFFile("qwe.gexf", true));
        getBetweennessCentrality(IOGraph.readGEXFFile("qwe.gexf", false));
    }

    @Test
    public void getClosenessCentrality() {
        Graph<String> graph = IOGraph.readGEXFFile("qwe.gexf", true);
        checkIfAlgorithmRunsCorrectly(graph, 38, graph.getClosenessCentrality(false), 100.0);
        graph = IOGraph.readGEXFFile("qwe.gexf", false);
        checkIfAlgorithmRunsCorrectly(graph, 38, graph.getClosenessCentrality(false), 100.0);

    }

    //Algorithm is not stable
    @Test
    public void getCommunityClassesSyncLabelPropagation() {
        assertTrue(true);
    }

    //Algorithm is not stable
    @Test
    public void getCommunityClassesAsyncLabelPropagation() {
        assertTrue(true);
    }

    @Test
    public void getStronglyConnectedComponents() {
        Graph<String> graph = IOGraph.readGEXFFile("qwe.gexf", true);
        testGraphMetics(graph, 419L, graph.getStronglyConnectedComponents());
        graph = IOGraph.readGEXFFile("qwe.gexf", false);
        testGraphMetics(graph, 419L, graph.getStronglyConnectedComponents());
    }

    @Test
    public void removeEdge() {
        removeEdge(new UndirectedGraphFast<>());
        removeEdge(new UndirectedGraph<>());
    }

    @Test
    public void addEdge() {
        addEdge(new UndirectedGraphFast<>());
        addEdge(new UndirectedGraph<>());
    }

    @Test
    public void testAddEdge() {
        testAddEdge(new UndirectedGraphFast<>());
        testAddEdge(new UndirectedGraph<>());
    }

    @Test
    public void testAddEdge1() {
        testAddEdge1(new UndirectedGraphFast<>());
        testAddEdge1(new UndirectedGraph<>());
    }

    @Test
    public void addAttributeToEdge() {
        addAttributeToEdge(new UndirectedGraphFast<>());
        addAttributeToEdge(new UndirectedGraph<>());
    }

    @Test
    public void testAddAttributeToEdge() {
        testAddAttributeToEdge(new UndirectedGraphFast<>());
        testAddAttributeToEdge(new UndirectedGraph<>());
    }

    @Test
    public void addEdgesFrom() {
        addEdgesFrom(new UndirectedGraphFast<>());
        addEdgesFrom(new UndirectedGraph<>());
    }

    @Test
    public void generateInDegreeAttributes() {
        generateInDegreeAttributes(new UndirectedGraphFast<>());
        generateInDegreeAttributes(new UndirectedGraph<>());
    }

    @Test
    public void generateWeightedInDegreeAttributes() {
        generateWeightedInDegreeAttributes(new UndirectedGraphFast<>());
        generateWeightedInDegreeAttributes(new UndirectedGraph<>());
    }

    @Test
    public void getInNeighbours() {
        getInNeighbours(new UndirectedGraphFast<>());
        getInNeighbours(new UndirectedGraph<>());
    }

    @Test
    public void calculateLowestNumberOfHopsPath() {
        calculateLowestNumberOfHopsPath(IOGraph.readGEXFFile("qwe.gexf", true));
        calculateLowestNumberOfHopsPath(IOGraph.readGEXFFile("qwe.gexf", false));
    }
}